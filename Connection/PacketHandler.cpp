/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "PacketHandler.h"
#include <Winsock2.h>
#include "../Decoder/Decoder.h"
#include "../Decoder/MasterDecoder.h"
#include "../Decoder/MapleEncryption.h"
#include "../TitanMS/PgoLogin.h"
#define LOGNIN_HEADER_LEN 4
#define	CHARSVR_HEADER_LEN 6
#define MASTER_HEADER_LEN 6
//Login Handlers
PacketHandlerLogin::PacketHandlerLogin(int socket, AbstractPlayer* player) {
	this->socket = socket;
	bytesInBuffer = 0;
	this->player = player;
	decoder = new Decoder();
	//int l = send(socket, (char*)(decoder->getConnectPacket()), Decoder::CONNECT_LENGTH, 0);
	//if (l < Decoder::CONNECT_LENGTH) {
		//TODO
	//}
}
PacketHandlerLogin::~PacketHandlerLogin(){
	delete decoder;
	delete player;
}


int PacketHandlerLogin::handle (Selector* selector, int socket) {
	if (bytesInBuffer < LOGNIN_HEADER_LEN) {
		// read header
		int l = recv(socket, (char*)(buffer + bytesInBuffer), LOGNIN_HEADER_LEN - bytesInBuffer, 0);
		if (l <= 0) {
			end();
			return 0;
		}
		bytesInBuffer += l;
	}

	if (bytesInBuffer >= LOGNIN_HEADER_LEN) {
		int packetSize = Decoder::getLength(buffer);
		int l = recv(socket, (char*)(buffer + bytesInBuffer), LOGNIN_HEADER_LEN + packetSize - bytesInBuffer, 0);
		if (l <= 0) {
			end();
			return 0;
		}
		bytesInBuffer += l;
		if (bytesInBuffer == packetSize + LOGNIN_HEADER_LEN){
			//decoder->decrypt(buffer + HEADER_LEN, packetSize);
			player->handleRequest(buffer, packetSize);
			bytesInBuffer = 0;
		}
	}
	return 1;

}

void PacketHandlerLogin::sendPacket(unsigned char *buff, int size){
	//unsigned char bufs[BUFFER_LEN];
	//decoder->createHeader((unsigned char*)bufs, (short)(size));
	//decoder->encrypt(buff, size);
	//memcpy_s(bufs+HEADER_LEN, size, buff, size);
	//decoder->next();
	//send(socket, (const char*)bufs, size+HEADER_LEN, 0);
	send(socket, (const char*)buff, size, 0);
}

/////Chanels Handlres


//Chars Handlers
PacketHandlerChars::PacketHandlerChars(int socket, AbstractPlayer* player) {
	this->socket = socket;
	bytesInBuffer = 0;
	this->player = player;
	decoder = new MasterDecoder();
	//int l = send(socket, (char*)(decoder->getConnectPacket()), Decoder::CONNECT_LENGTH, 0);
	//if (l < Decoder::CONNECT_LENGTH) {
		//TODO
	//}
}
PacketHandlerChars::~PacketHandlerChars(){
	delete decoder;
	delete player;
}


int PacketHandlerChars::handle (Selector* selector, int socket) {
	if (bytesInBuffer < CHARSVR_HEADER_LEN) {
		// read header
		int l = recv(socket, (char*)(buffer + bytesInBuffer), CHARSVR_HEADER_LEN - bytesInBuffer, 0);
		if (l <= 0) {
			end();
			return 0;
		}
		bytesInBuffer += l;
	}

	if (bytesInBuffer >= LOGNIN_HEADER_LEN) {
		int packetSize = MasterDecoder::getLength(buffer);
		int l = recv(socket, (char*)(buffer + bytesInBuffer), CHARSVR_HEADER_LEN + packetSize - bytesInBuffer, 0);
		if (l <= 0) {
			end();
			return 0;
		}
		bytesInBuffer += l;
		//if (bytesInBuffer == packetSize + CHARSVR_HEADER_LEN){
			//decoder->decrypt(buffer + HEADER_LEN, packetSize);
			//player->handleRequest(buffer + CHARSVR_HEADER_LEN, packetSize);
		if (bytesInBuffer == packetSize){
			player->handleRequest(buffer, packetSize);
			bytesInBuffer = 0;
		}
	}
	return 1;

}

void PacketHandlerChars::sendPacket(unsigned char *buff, int size){
	//unsigned char bufs[BUFFER_LEN];
	//decoder->createHeader((unsigned char*)bufs, (short)(size));
	//decoder->encrypt(buff, size);
	//memcpy_s(bufs+HEADER_LEN, size, buff, size);
	//decoder->next();
	//send(socket, (const char*)bufs, size+HEADER_LEN, 0);
	send(socket, (const char*)buff, size, 0);
}





//Game Server Handlers
PacketHandlerMaster::PacketHandlerMaster(int socket, AbstractPlayer* player) {
	this->socket = socket;
	bytesInBuffer = 0;
	this->player = player;
	decoder = new MasterDecoder();
	LogInfo tmpinfo;
	tmpinfo = PgoLogin::getInstance()->getPlayerLoginInfo(player->getIP());
	//decoder->getConnectPacket(); 
	send(socket, (char*)(decoder->getConnectPacket(tmpinfo.UserId,Tools::getTime(),player->getIP())), Decoder::CONNECT_LENGTH, 0);
}
PacketHandlerMaster::~PacketHandlerMaster(){
	delete decoder;
	delete player;
}

int PacketHandlerMaster::handle (Selector* selector, int socket) {
	if (bytesInBuffer < MASTER_HEADER_LEN) {
		// read header
		int l = recv(socket, (char*)(buffer + bytesInBuffer), MASTER_HEADER_LEN - bytesInBuffer, 0);
		if (l <= 0) {
			end();
			return 0;
		}
		bytesInBuffer += l;
	}

	if (bytesInBuffer >= MASTER_HEADER_LEN) {
		int packetSize = MasterDecoder::getLength(buffer);
		int l = recv(socket, (char*)(buffer + bytesInBuffer),  packetSize - bytesInBuffer, 0);//MASTER_HEADER_LEN +
		if (l <= 0) {
			end();
			return 0;
		}
		bytesInBuffer += l;
		if (bytesInBuffer == packetSize){
			player->handleRequest(buffer, packetSize);
			bytesInBuffer = 0;
		}
	}
	return 1;
}


void PacketHandlerMaster::sendPacket(unsigned char *buff, int size){
	unsigned char bufs[BUFFER_LEN];
	//decoder->createHeader((unsigned char*)bufs, (short)(size));
	//decoder->encrypt(buff, size);
	memcpy_s(bufs, size, buff, size);
	send(socket, (const char*)bufs, size, 0);
}
///////////////CH
PacketHandlerCH::PacketHandlerCH(int socket, AbstractPlayer *player){
	this->socket = socket;
	bytesInBuffer = 0;
	this->player = player;
	decoder = new MasterDecoder();
	
}
PacketHandlerCH::~PacketHandlerCH(){
	delete decoder;
	delete player;
}
int PacketHandlerCH::handle(Selector* selector, int socket) {

	if (bytesInBuffer < MASTER_HEADER_LEN) {
		// read header
		int l = recvfrom(socket, (char*)(buffer + bytesInBuffer), 10000 - bytesInBuffer, 0,NULL,0);
		if (l <= 0) {
			end();
			return 0;
		}
		bytesInBuffer += l;
	}

	if (bytesInBuffer >= MASTER_HEADER_LEN) {
		int packetSize = MasterDecoder::getLength(buffer);
		if(packetSize < bytesInBuffer){
			int l = recvfrom(socket, (char*)(buffer + bytesInBuffer),  packetSize - bytesInBuffer, 0,NULL,0);//MASTER_HEADER_LEN +
			if (l <= 0) {
				//end();
				bytesInBuffer = 0;
				return 1;
			}
			bytesInBuffer += l;
		}
		if (bytesInBuffer == packetSize){
			player->handleRequest(buffer, packetSize);
			bytesInBuffer = 0;
		}
	}
	return 1;
}
void PacketHandlerCH::sendPacket(unsigned char *buff, int size){
		unsigned char bufs[BUFFER_LEN];
		//decoder->createHeader((unsigned char*)bufs, (short)(size));
		//decoder->encrypt(buff, size);
		memcpy_s(bufs, size, buff, size);
		sendto(socket, (const char*)bufs, size, 0,NULL,0);
}
void PacketHandlerLogin::disconnect(){
	end();
}
void PacketHandlerMaster::disconnect(){
	end();
}
void PacketHandlerChars::disconnect(){
end();
}
void PacketHandlerCH::disconnect(){
end();
}