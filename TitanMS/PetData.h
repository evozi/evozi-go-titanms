/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef PETDATA_H
#define PETDATA_H

#include "DataStruct.h"
#include "TopDataStruct.h"
using namespace std;
class PetCommandData;

class PetData : public DataStruct, public TopDataStruct<PetCommandData> {
private:
	string name;
	char LevelRequer;
	char Type;
	char Equip;
	char HostProtect;
	char AccAttack;
public:
	PetData(int id){
		this->id = id;
		LevelRequer=0;
		Type=0;
		Equip=0;
		name="";
		HostProtect=0;
		AccAttack = 0;
	}
	void setName(string name){
		this->name = name;
	}
	string getName(){
		return name;
	}
	void setLevelRequer(int lv){
		this->LevelRequer = lv;
	}
	void setType(int lv){
		this->Type = lv;
	}
	void setEquip(int lv){
		this->Equip = lv;
	}	
	void setHostProtect(int lv){
		this->HostProtect = lv;
	}
	void setAccAttack(int lv){
		this->AccAttack = lv;
	}
	char getLevelRequer(){
		return LevelRequer;
	}
	char getType(){
		return Type;
	}
	char getEquip(){
		return Equip;
	}
	char getHostProtect(){
		return HostProtect;
	}
	char getAccAttack(){
		return AccAttack;
	}
};

#endif