 /*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef PLAYER_H
#define PLAYER_H

#include "../Connection/AbstractPlayer.h"
#include "../Connection/PacketHandler.h"

#include <string.h>
#include <math.h>
#include <vector>

#include "LifeMapObject.h"

class Trade;
class PlayerNPC;
class PlayerSkills;
class PlayerQuests;
class PlayerInventories;
class PlayerBuffs;
class Map;
class PlayerKeys;
class ShopData;
class Party;
class Channel;
class PacketWriter;
class Pet;
template <class T> class Handler;
class PlayerHandler;

struct SkillMapEnterActiveInfo;

class Player:public AbstractPlayer, public LifeMapObject{
public:
	Player (int port);

	~Player();

	void handleRequest(unsigned char* buf, int len);
	void sendPacket(unsigned char* buf, int len){ packetHandler->sendPacket(buf,len); }
	void disconnect();
	void setName(string name){
		this->name = name;
	}
	string getName(){
		return this->name;
	}
	void setTitle(string title){
		this->NickName = title;
	}
	string getTitle(){
		return this->NickName;
	}
	void setGender(char g){
		this->gender = g;
	}
	char getGender(){
		return this->gender;
	}
	void setSkin(char id, bool send = true);
	char getSkin(){
		return this->skin;
	}
	void setFace(int id, bool send = true);
	int getFace(){
		return this->face;
	}
	void setHair(int id, bool send = true);
	int getHair(){
		return this->hair;
	}
	void setMap(int mapid);
	void setRegMap(int Regid,int Mapid);
	void setMapPos(int mapid, int pos);
	void setMapPortal(int mapid, string& portalname);
	Map* getMap(){
		return this->map;
	}
	void setMappos(char pos){
		this->mappos = pos;
	}
	void FixStats();
	char getMappos(){
		return this->mappos;
	}
	void setNPCShop(ShopData* shop){
		this->npcShop=shop;
	}
	ShopData* getNPCShop(){
		return npcShop;
	}
	void setNPC(PlayerNPC* npc){
		this->npc = npc;
	}
	void setParty(Party* party){
		this->party = party;
	}
	void setChair(int chair){
		this->chair = chair;
	}
	int getChair(){
		return chair;
	}
	void setItemEffect(int itemEffect){
		this->itemEffect = itemEffect;
	}
	int getItemEffect(){
		return itemEffect;
	}
	PlayerNPC* getNPC(){
		return npc;
	}
	void setTrade(Trade* trade){
		this->trade = trade;
	}
	Trade* getTrade(){
		return trade;
	}
	int isGM(){
		return gm;
	}
	PlayerInventories* getInventories(){
		return inv;	
	}
	PlayerSkills* getSkills(){
		return skills;
	}
	PlayerQuests* getQuests(){
		return quests;
	}
	PlayerKeys* getKeys(){
		return keys;
	}
	PlayerBuffs* getBuffs(){
		return buffs;
	}
	void damage();
	void save();
	void setcharID(int id){
		this->charid= id;
	}
	void setUserID(int id){
		this->id = id;
		playerConnect();
	}
	void addMaxHPAP(int a){
		hpap+=a;
	}
	void addMaxMPAP(int a){
		mpap+=a;
	}
	void setMaxHPAP(int a){
		hpap=a;
	}
	void setMaxMPAP(int a){
		mpap=a;
	}
	int getMaxHPAP(){
		return hpap;
	}
	int getMaxMPAP(){
		return mpap;
	}
	void setMesos(int m, bool send = true, bool item = false);
	int getMesos(){
		return mesos;
	}
	void setLevel(int level, bool send = true);
	unsigned char getLevel(){
		return this->level;
	}
	void setJob(short job, bool send = true);
	short getJob(){
		return this->job;
	}
	void setStr(short str, bool send = true);
	short getStr(){
		return this->str;
	}
	void setDex(short dex, bool send = true);
	short getDex(){
		return this->dex;
	}
	void setVit(short vit, bool send = true);
	short getVit(){
		return this->vit;
	}
	void setInt(short intt, bool send = true);
	short getInt(){
		return this->intt;
	}
	void setLuk(short luk, bool send = true);
	short getLuk(){
		return this->luk;
	}
	void setHP(int hp, bool send = true, bool item= false);
	unsigned short getHP(){
		return this->hp;
	}
	void setMP(int mp, bool send = true, bool item= false);
	unsigned short getMP(){
		return this->mp;
	}
	void setMaxHP(int mhp, bool send = true, bool item= false);
	unsigned short getMaxHP(){
		return this->mhp;
	}
	void setBaseMaxHP(int rmhp);
	unsigned short getBaseMaxHP(){
		return this->rmhp;
	}
	void setMaxMP(int mmp, bool send = true, bool item= false);
	unsigned short getMaxMP(){
		return this->mmp;
	}
	void setBaseMaxMP(int rmmp);
	unsigned short getBaseMaxMP(){
		return this->rmmp;
	}
	void setAP(short ap, bool send = true);
	short getAP(){
		return this->ap;
	}
	void setSP(short sp, bool send = true);
	short getSP(){
		return this->sp;
	}
	void setFame(short fame, bool send = true);
	short getFame(){
		return this->fame;
	}
	void setExp(int exp, bool send = true);
	int getExp(){
		return this->exp;
	}
	Channel* getChannel(){
		return channel;
	}
	void addMesos(int m, bool send = true, bool item=false){
		setMesos(mesos + m, send, item);
	}
	void addExp(int e, bool send = true){
		printf("Exp to Add: %d\n",e);
		printf("Exp %d\n",exp);
		setExp(exp + e, send);
	}
	void addLevel(int l, bool send = true){
		setLevel(level + l, send);
	}
	void addJob(short j, bool send = true){
		setJob(job + j, send);
	}
	void addSTR(short s, bool send = true){
		setStr(str + s, send);
	}
	void addDEX(short d, bool send = true){
		setDex(dex + d, send);
	}
	void addINT(short i, bool send = true){
		setInt(intt + i, send);
	}
	void addLUK(short l, bool send = true){
		setLuk(luk+ l, send);
	}
	void addHP(int h, bool send = true, bool item= false){
		setHP(hp + h, send, item);
	}
	void addMP(int m, bool send = true, bool item= false){
		setMP(mp + m, send, item);
	}
	void addMaxHP(int m, bool send = true, bool item= false){
		setMaxHP(mhp + m, send, item);
	}
	void addBaseMaxHP(int r){
		setBaseMaxHP(rmhp + r);
	}
	void addMaxMP(int m, bool send = true, bool item= false){
		setMaxMP(mmp + m, send, item);
	}
	void addBaseMaxMP(int r){
		setBaseMaxMP(rmmp + r);
	}
	void addAP(short a, bool send = true){
		setAP(ap + a, send);
	}
	void addSP(short s, bool send = true){
		setSP(sp + s, send);
	}
	void addFame(short f, bool send = true){
		setFame(fame + f, send);
	}
	Pet* getPet(int id);
	vector <Pet*>* getPets(){
		return &pets;
	}
	void addPet(Pet* pet);
	void removePet(Pet* pet);
	void levelUP();
	void updateStat(int stat, int value, bool item = false, char pet=0);
	Value removeStat(int stat);
	Value addStat(int stat, bool random = false);
	void changeMap(Map* map, int portal = 0);
	void addQuest(short questid);
	void endQuest(short questid);
	void setStyle(int id);
	int getItemAmount(int itemid);
	bool giveItem(int itemid, short amount);
	void giveMesos(int amount);
	static class Update {
	public:
		static const int NOTHING = 0x0;
		static const int SKIN = 0x10;
		static const int FACE = 0x20;
		static const int HAIR = 0x30;
		static const int PET = 0x40;
		static const int LEVEL = 0x50;
		static const int JOB = 0x60;
		static const int STR = 0x1;
		static const int DEX = 0x2;
		static const int INT = 0x3;
		static const int VIT = 0x4;
		static const int LUK = 0x5;
		static const int HP = 0x400;
		static const int MAXHP = 0x800;
		static const int MP = 0x1000;
		static const int MAXMP = 0x2000;
		static const int AP = 0x4000;
		static const int SP = 0x8000;
		static const int EXP = 0x10000;
		static const int FAME = 0x20000;
		static const int MESOS = 0x40000;
	};
	static class Effects {
	public:
		static const int LEVEL_UP = 0x0;
		static const int PET_LEVEL_UP = 0x4;
		static const int JOB_CHANGE = 0x8;
		static const int QUEST_FINISH = 0x9;
		static const int HEAL = 0xA;
	};
	static const int exps[200];
	void send(PacketWriter* packet);
	/// PvP
		
	void setPvP(int pvp){
		this->pvp = pvp;
	}
	int getPvP(){
		return pvp;
	}
	void setPvPMaskMob(int mobid){
		mask = mobid;
	}
	int getPvPMaskMob(){
		return mask;
	}	
	int getWDef();
	int getMDef();

	///

	
	void setVariable(string &name, int val){
		vars[name] = val;
	}
	int getVariable(string &name){
		if(vars.find(name) == vars.end())
			return 0;
		else
			return vars[name];
	}
	void setGlobalVariable(string &name, int val){
		global[name] = val;
	}
	int getGlobalVariable(string &name){
		if(global.find(name) == global.end())
			return 0;
		else
			return global[name];
	}
	void SetPlayerLoginID(int ID){    //ADD BY ME
		 this->PlayerLoginID = ID;
	}
	int GetPlayerLoginID(){             ///ADD BY ME
		return this->PlayerLoginID;
	}
	void deleteGlobalVariable(string &name){
		if(global.find(name) != global.end())
			global.erase(name);
	}
	///////////////////////////////////////
	void setClass(char pclass){
		this->Pclass = pclass;
	}
	char getClass(){
		return this->Pclass;
	}
	void setClassLevel(char pclasslevel){
		this->PclassLevel = pclasslevel;
	}
	char getClassLevel(){
		return this->PclassLevel;
	}
	void setRage(short rage){
		this->Rage = rage;
	}
	short getRage(){
		return this->Rage;
	}
	void setMRage(short mrage){
		this->MRage = mrage;
	}
	short getMRage(){
		return this->MRage;
	}
	void setAtk(short atk){
		this->Atk = atk;
	}
	short getAtk(){
		return this->Atk;
	}
	void setMagicAtk(short magicatk){
		this->MagicAtk = magicatk;
	}
	short getMagicAtk(){
		return this->MagicAtk;
	}
	void setAtkMax(short atkmax){
		this->AtkMax = atkmax;
	}
	short getAtkMax(){
		return this->AtkMax;
	}
	void setMagicAtkMax(short magicatkmax){
		this->MagicAtkMax = magicatkmax;
	}
	short getMagicAtkMax(){
		return this->MagicAtkMax;
	}
	void setDefence(short Defence){
		this->defnce = Defence;
	}
	short getDefence(){
		return this->defnce;
	}
	void setAbiltyPoints(short points){
		this->AbiltyPoints = points;
	}
	short getAbiltyPoints(){
		return this->AbiltyPoints;
	}
	void setSkillPoints(short points){
		this->SkillPoints = points;
	}
	short getSkillPoints(){
		return this->SkillPoints;
	}
	void setPlayerX(short playerX){
		this->PlayerX  = playerX;
	}
	short getPlayerX(){
		return this->PlayerX;
	}
	void setPlayerY(short playerY){
		this->PlayerY  = playerY;
	}
	short getPlayerY(){
		return this->PlayerY;
	}
	void setWep(int wep){
		this->Wep = wep;
	}
	int getWep(){
		return this->Wep;
	}
	void setRegionId(int regionId){
		this->RegionId = regionId;
	}
	int getRegionId(){
		return this->RegionId;
	}
	void setMapId(int mapid){
		this->MapId = mapid;
	}
	int getMapId(){
		return this->MapId;
	}
	void setArmor(int armor){
		this->Armor = armor;
	}
	int getArmor(){
		return this->Armor;
	}
	void setEyes(int eyes){
		this->Eyes = eyes;
	}
	int getEyes(){
		return this->Eyes;
	}
	void setMeditate(bool val){
		this->Meditate = val;
	}
	bool getMeditate(){
		return this->Meditate;
	}
	void setRun(bool val){
		this->Run = val;
	}
	bool getRun(){
		return this->Run;
	}
	void setGSM(bool val){
		this->Gsm = val;
	}
	bool getGSM(){
		return this->Gsm;
	}
	void setLocalIP(string ip){
		this->localip = ip;
	}
	string getLoaclIP(){
		return this->localip;
	}
	void setUDPport(int port){
		this->UDPport = port;
	}
	int getUDPport(){
		return this->UDPport;
	}
	void setDie(int type){
		this->Die = type;
	}
	int getDie(){
		return this->Die;
	}
int Getcash(int num)
	{
		switch (num){
		case 1: return this->Cash1;break;
		case 2: return this->Cash2;break;
		case 3: return this->Cash3;break;
		case 4: return this->Cash4;break;
		case 5: return this->Cash5;break;
		}
		return 0;
	}
private:
	//things i add
	char Pclass;
	char PclassLevel;
	short Rage;
	short MRage;
	short PlayerX;
	short PlayerY;
	int Wep;
	int Armor;
	int Eyes;
	int Cash1;
	int Cash2;
	int Cash3;
	int Cash4;
	int Cash5;
	short RegionId;
	short MapId;
	bool Gsm;
	string localip;
	int UDPport;
	int Die;
	///////////////////////////////
	bool isconnect;
	string name;
	string NickName;
	char gender;
	char skin; //No Need
	int face;
	int hair;
	Map* map;
	char mappos;
	int chair;
	int itemEffect;
	int gm;
	int mesos;
	int hpap;
	int mpap;
	int mask;
	int pvp;
	int inserver;
	Channel* channel;
	std::vector <Pet*> pets;
	void playerConnect();


	PlayerInventories* inv;
	PlayerSkills* skills;
	PlayerQuests* quests;
	PlayerNPC* npc;
	PlayerKeys* keys;
	ShopData* npcShop;
	Party* party;
	PlayerBuffs* buffs;
	Handler <PlayerHandler>* handler;
	Trade* trade;

	unsigned char level;
	bool Run;
	bool Meditate;

	char job;
	short str;
	short dex;
	short vit;
	short intt;
	short luk;
	short defnce;
	short AtkMax;
	short Atk;
	short MagicAtkMax;
	short MagicAtk;
	short AbiltyPoints;
	short SkillPoints;
	unsigned short hp;
	unsigned short mhp;
	unsigned short rmhp;
	unsigned short mp;
	unsigned short mmp;
	unsigned short rmmp;
	short ap;
	short sp;
	int exp;
	short fame;
	
	int PlayerLoginID;  //ADD BY ME

	hash_map <string, int> global;
	hash_map <string, int> vars;
};

class PlayerFactory:public AbstractPlayerFactory {
public:
	AbstractPlayer* createPlayer(int port) {
		return new Player(port);
	}
};

#endif