/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "DataProvider.h"
#include "Damage.h"
#include "PacketReader.h"

Damage::Damage(PacketReader& packet, bool range){
	packet.readInt();
	PlayerID =			packet.readShort();
	MobID =				packet.readShort();
	SamthingWithMob =	packet.readShort();
	Val1 =				packet.readShort();
	AtkDmg =		packet.readInt();
	MobX =				packet.readShort();
	Val2 =				packet.readShort();
	SkillUse =		packet.readInt();
	Val3 =            packet.read();
	Val4 =	          packet.read();
	Val5 =				packet.readShort();
	PlayerHitC =	packet.readInt();
	Val7 =		      packet.read();
	Val8 =            packet.read();
	HitPosX =			packet.readShort();
	HitPosY =			 packet.readShort();
	Val9 =				packet.readShort();

	info = 0x1;//packet.read();
	mobCount = 1;//info/0x10;
	attackCount = 1;// info%0x10;
	skill = 0;//packet.readInt();
	//packet.read();
	charge = 0;
	/*
	if (skill == 2121001 || skill == 2221001 || skill == 2321001)
		charge = packet.readInt();
		*/
	stance = 9;//packet.read();
	//packet.read();
	speed = 5;//packet.read();

	//packet.readInt();

	// Mesos Explosion - TODO ( or after stance?)
	/*
	mesoexplosion = (skill == 4211006);
	if(mesoexplosion)
		return;

	if(skill == 5201002)
		packet.readInt();

	if(range){
		itemslot = packet.readInt();
		packet.read();
	}
	else itemslot = 0;
	*/
	itemslot = 0;
int min =abs(HitPosX - MobX);
MinHitVal = MobX;

if(abs(HitPosX -(MobX+100) < min))
{
	min = abs(HitPosX -(MobX+100));
	MinHitVal = MobX+100;
}
if(abs(HitPosX -(MobX-100) < min))
{
	min = abs(HitPosX -(MobX-100));
	MinHitVal = MobX-100;
}



	for(int i=0; i<mobCount; i++){
		int mobid = MobID;//packet.readInt();
		//packet.read(14);
		mobs.push_back(mobid);
		vector<int> damage;
		for(int j=0; j<attackCount; j++){
			damage.push_back(AtkDmg);//
		}
		damages[mobid] = damage;
		//packet.readInt();
	}
}