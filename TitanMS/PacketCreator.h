/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef PACKETCREATOR_H
#define PACKETCREATOR_H

#include <vector>


#include "PacketWriter.h"

class Drop;
class Player;
class Players;
class MapObject;
class ObjectMoving;
class Values;
class World;
class Damage;
class Character;
class Item;
class NPC;
class Mob;
class PlayerKeys;
class Quest;
struct Position;
class ShopData;
class Skill;
class Effect;
class Pet;
class Reactor;
class Trade;

class Inventory;
using namespace std;


class PacketCreator{
private:

	static const short PING = 0x11;

	// Remote Control
	static const short REMOTE_LOGIN = 0x00;
	static const short COMMAND_REPLAY = 0x01;

	// Login
	static const int   LOGIN_HEADER_START = 0x55AA;
	static const int   LOGIN_HEADER_END = 0xAA55;
	static const int   WORLD_HEADER_START = 0x0105;
	static const short LOGIN_PROCESS = 0x00;
	static const short WORLD_STATUS = 0x03;
	static const short PIN = 0x06;
	static const short SHOW_WORLDS = 0x0A;
	static const short SHOW_CHARACTERS = 0x0B;
	static const short CHANNEL_INFO = 0x0C;
	static const short CHECK_NAME = 0x0D;
	static const short ADD_CHARACTER = 0x0E;
	static const short REMOVE_CHARACTER = 0x0F;
	static const short LOG_BACK = 0x16;
	// Drops
	static const short SHOW_DROP = 0xCB;
	static const short REMOVE_DROP = 0xCC;
	static const short SHOW_GAIN = 0x24;
	// Inventory
	static const short INVENTORY_CHANGE = 0x1A;
	static const short USE_PET = 0x7E;
	static const short PET_MOVE = 0x80;
	static const short PET_TEXT = 0x81;
	static const short PET_COMMAND_REPLAY = 0x84; 
	// Stats
	static const short STATS_UPDATE = 0x1C;
	static const short ADD_SKILL = 0x21;
	// Maps
	static const short CHANGE_MAP = 0x5B;
	static const short SHOW_PLAYER = 0x77;
	static const short REMOVE_PLAYER = 0x78;
	static const short MAP_EFFECT = 0x67;
	static const short MAP_WEATHER = 0x68;
	// Mobs
	static const short CONTROL_MOB = 0xAF;
	static const short SPAWN_MOB = 0x38;
	static const short Regenrate_Mob = 0x3F;
	static const short MOVE_MOB = 0xB0;
	static const short MOVE_MOB_RESPONSE = 0xB1;
	static const short SHOW_MOB_HP = 0xBB;
	static const short REMOVE_MOB = 0xAE;
	static const short DAMAGE_REGULAR = 0x8D;
	static const short DAMAGE_RANGED = 0x8E;
	static const short DAMAGE_MAGIC = 0x8F;
	static const short DAMAGE_MOB = 0x90; // to find
	static const short MOB_SKILL = 0x91; // to find
	// NPCs
	static const short SHOW_NPC = 0xC0;
	static const short NPC_TALK = 0xEA;
	static const short SHOW_SHOP = 0xEB;
	static const short BUY_RESONSE = 0xEC;
	// Player
	static const short Char_HP_SP = 0x51;
	static const short UPDATE_LOOK = 0x97;
	static const short SCROLL_EFFECT = 0x7D;
	static const short ITEM_EFFECT = 0x31;
	static const short SHOW_CHAIR = 0x96;
	static const short CANCEL_CHAIR = 0x9F;
	static const short SHOW_PLAYER_EFFECT = 0x98;
	static const short SHOW_PLAYER_BUFF = 0x1D;
	static const short SHOW_BUFF = 0x99;
	static const short CANCEL_PLAYER_BUFF = 0x1E;
	static const short CANCEL_BUFF = 0x9A;
	static const short MAKE_APPLE = 0x62;
	static const short SHOW_KEYS = 0x104;
	static const short DAMAGE_PLAYER = 0x93;
	static const short PLAYER_MOVE = 0x8C;
	static const short CHAT_MASSAGE = 0x79;
	static const short SHOW_MASSAGE = 0x41;
	static const short FACE_EEXPRESSION = 0x94;
	static const short PLAYER_INFO = 0x3A;
	static const short FIND_PLAYER = 0x64;
	static const short CHANGE_CHANNEL = 0x03;
	// Quests
	static const short ITEM_GAIN_CHAT = 0xA0;
	static const short QUEST_UPDATE = 0xA5;
	static const short COMPLETE_QUEST = 0x2E;
	// Reactors
	static const short UPDATE_REACTOR = 0xD4;
	static const short SHOW_REACTOR = 0xD6;
	static const short DESTROY_REACTOR = 0xD7;
	// Transportation
	static const short TRANSPORTATION = 0xF2;
	// Party
	static const short PARTY_HP = 0x96; // to find

	PacketWriter pw;
public:
	PacketCreator(){
		pw = PacketWriter();
	}
	//FromFile
	PacketWriter* FromFile(char* FilePath,char* Title,bool charserver = false,bool addhead = true);

	PacketWriter* ping();
	// Remote Control
	PacketWriter* loginReplay(bool success);
	PacketWriter* commandReplay(string replay);
	// Login
	PacketWriter* nPacketLOGIN_ACK(string p1,string p2,string p3);
	PacketWriter* loginError(short errorid);
	PacketWriter* loginProcess(char type);
	PacketWriter* loginConnect(char gender, string username, bool firstlogin = false);
	PacketWriter* processOk();
	PacketWriter* showWorld(World* world);
	PacketWriter* endWorlds();
	PacketWriter* showChannels();
	PacketWriter* SelectChRespone(string ip,int TCPP,int UDPP);
	PacketWriter* showCharacters(vector <Character*>& chars);
	void showCharacter(Character* character);
	PacketWriter* addCharacter(Character* character);
	PacketWriter* checkName(string name, bool ok);
	PacketWriter* removeCharacter(int characterid);
	PacketWriter* connectChannel(int charid, short port);
	PacketWriter* logBack();
	// Drops
	PacketWriter* dropObjects(vector<Drop*> drops,  MapObject* dropper);
	PacketWriter* dropObject(Drop* drop, char mode,  MapObject* dropper);
	PacketWriter* showDrop(Drop* drop);
	PacketWriter* gainDrop(int itemid, int amount, bool mesos = false);
	PacketWriter* gainMesos(int mesos);
	PacketWriter* lootError();
	PacketWriter* removeDrop(int dropid,int PlayerID,int ItemID,char type = 0);
	PacketWriter* explodeDrop(int dropid);
	PacketWriter* lootDrop(int dropid, int playerid);
	// Inventory
	PacketWriter* emptyInventoryAction();
	PacketWriter* INVEN_UPDATE(Inventory* inv,int invuse);
	PacketWriter* INVEN_EQUIP(Inventory* inv);
	PacketWriter* INVEN_EQUIP1(Inventory* inv);
	PacketWriter* INVEN_EQUIP2(Inventory* inv);
	PacketWriter* INVIN_SPEND3(Inventory* inv);
	PacketWriter* INVEN_OTHER4(Inventory* inv);
	PacketWriter* INVEN_PET5(Inventory* inv);
	void itemInfo(Item* item, bool showPosition = true, bool showZeroPosition = false);
	PacketWriter* moveItem(char inv, short slot1, short slot2, char equip = 0);
	PacketWriter* updateSlot(Item* item, bool drop = false);
	PacketWriter* newItem(Item* item, bool drop);
	PacketWriter* removeItem(char inv, int slot, bool drop);
	PacketWriter* moveItemMerge(char inv, short slot1, short slot2, short amount);
	PacketWriter* moveItemMergeTwo(char inv, short slot1, short amount1, short slot2, short amount2);
	PacketWriter* scrollItem(Item* scroll, Item* equip, bool destroyed);
	PacketWriter* showPet(int playerid, Pet* pet);
	PacketWriter* removePet(int playerid, int petSlot);
	PacketWriter* movePet(int playerid, int petSlot, ObjectMoving& moving, Position& pos);
	PacketWriter* updatePet(Pet* pet, bool drop = false);
	PacketWriter* petCommandReplay(int playerid, int petSlot, int id, bool success);
	PacketWriter* showPetText(int playerid, int petSlot, string text, int act);
	// Player
	PacketWriter* MSG_FROM_SERVER(string Msg);
	PacketWriter* Player_DEAD_ACK(Player* player);
	PacketWriter* Player_PVP_END(int PlayerWin);
	PacketWriter* Player_PVP_START(int playerid1,int playerid2);
	PacketWriter* Player_136();
	PacketWriter* Player_PVP_REQ(int playerid);
	PacketWriter* Player_CLICK_CHAR_INFO(Player* player);
	PacketWriter* Player_CLICK_INFO(int playerid);
	PacketWriter* Player_SET_AVATAR(Player* player);
	PacketWriter* Player_INVEN_MONEY(Player* player,int GoldBefore);
	PacketWriter* Player_ENTER_MAP_START(Player* player);
	PacketWriter* Player_CAN_WARP_ACK(int canWarp,short Region,short Map,short px,short py);
	PacketWriter* Player_WARP_ACK(Player* player);
	PacketWriter* Player_SKILL_ALL(Player* player);
	PacketWriter* Player_CHAR_LEVELUP(Player* player);
	PacketWriter* Player_CHAR_LVEXP(Player* player);
	PacketWriter* Player_CHAR_STATUP_ACK(Player* player);
	PacketWriter* Player_CHAR_ALL(Player* player);
	PacketWriter* Player_GSM_SKILL();
	PacketWriter* Player_GSM_CONVERT(Player* player,unsigned char State);//Byme
	PacketWriter* Player_HP_SP(Player* player);
	PacketWriter* updatePlayer(Player* player);
	PacketWriter* useScroll(int playerid, bool success, bool cursed = false);
	PacketWriter* useItemEffect(int playerid, int itemid);
	PacketWriter* useChair(int playerid, int chairid);
	PacketWriter* cancelChair();
	PacketWriter* showEffect(int playerid, char effect, int what = 0, bool buff = false);
	PacketWriter* showPlayerEffect(char effect, int what = 0, bool buff = true);
	PacketWriter* showBuffEffect(int playerid, char effect, int source);
	PacketWriter* showPlayerBuffEffect(char effect, int source);
	PacketWriter* showPlayerBuff(Values* values, int buffid, int time);
	PacketWriter* showBuff(int playerid, Values* values);
	PacketWriter* cancelPlayerBuff(Values* values);
	PacketWriter* cancelBuff(int playerid, Values* values);
	void playerInfo(Player* player);
	void playerShow(Player* player, bool smega = false);
	PacketWriter* makeApple();
	PacketWriter* connectionData(Player* player);
	PacketWriter* showKeys(PlayerKeys* keys);
	PacketWriter* showMoving(int playerid, ObjectMoving& moving);
	PacketWriter* faceExpression(int playerid, int face);
	PacketWriter* showChatMassage(int playerid, string msg,string PlayerName ,bool macro, bool gm = false);
	PacketWriter* damagePlayer(int playerid, int skill, int dmg, int obj);
	PacketWriter* showMassage(string msg, char type, int channel = 0, bool server = false);
	PacketWriter* showInfo(Player* player);
	PacketWriter* findPlayerReplay(string name, bool success = true);
	PacketWriter* findPlayerMap(string name, int map, int channel);
	PacketWriter* findPlayerChannel(string name, int map, int channel);
	PacketWriter* whisperPlayer(string name, string msg, int channel);
	PacketWriter* changeChannel(char channelid, short port);
	// Stats
	PacketWriter* gainEXP(int exp, bool chat, bool yellow=false);
	PacketWriter* enableAction();
	PacketWriter* updateStats(Values& stats, bool item=false, char pets = 0);
	// Map
	PacketWriter* showPlayers(vector <Player*>,Player* player);
	PacketWriter* showPlayer(Player* player);
	PacketWriter* removePlayer(int playerid);
	PacketWriter* changeMap(Player* player);
	PacketWriter* changeSound(string sound);
	PacketWriter* showEffect(string effect);
	PacketWriter* playSound(string sound);
	PacketWriter* mapChange(char mode, string name);
	// Mobs
	PacketWriter* controlMob(Mob* mob, bool agrs = false, bool spawn = false);
	PacketWriter* endControlMob(int mobid);
	PacketWriter* AttackMob(Mob* mob,float Speed,int Damge,int PlayerID,short HitX,short HitY);//Bymy
	PacketWriter* RegenrMob(Mob* mob);//By Me
	PacketWriter* showMob(Mob* mob, bool spawn);
	PacketWriter* moveMob(int mobid, Position& position, ObjectMoving& moving, char skill, int skillid);
	PacketWriter* moveMobResponse(Mob* mob, int type, bool agrs);
	PacketWriter* showHP(int mobid, char per);
	PacketWriter* showBossHP(int mobid, int hp, int maxhp, char per, int color, int bgcolor);
	PacketWriter* killMob(int mobid, bool animation = true);
	void damage(int playerid, Damage& damage, int itemid = 0);
	PacketWriter* damageMob(int playerid, Damage& dmg);
	PacketWriter* damageMob(int mobid, int damage);
	PacketWriter* damageMobMagic(int playerid, Damage& dmg);
	PacketWriter* damageMobRanged(int playerid, Damage& dmg, int itemid = 0);
	// NPCs
	PacketWriter* PSHOP_BUYACK(int Respone);
	PacketWriter* PSHOP_SELLSTARTACK(int playerID,string shopname);
	PacketWriter* PSHOP_SELLINFO(ShopData* data); //TODO

	PacketWriter* PSHOP_SELLEND(int playerID);
	PacketWriter* PSHOP_OPENACK(int slots);
	PacketWriter* showNPC(NPC* npc);
	PacketWriter* bought();
	PacketWriter* showNPCShop(ShopData* data);
	void npcPacket(int npcid, string text, char type);
	PacketWriter* sendSimple(int npcid, string text);
	PacketWriter* sendYesNo(int npcid, string text);
	PacketWriter* sendBackNext(int npcid, string text, bool back, bool next);
	PacketWriter* sendAcceptDecline(int npcid, string text);
	PacketWriter* sendGetText(int npcid, string text);
	PacketWriter* sendGetNumber(int npcid, string text, int def, int min, int max);
	PacketWriter* sendStyle(int npcid, string text, int styles[], char size);
	// Party
	/*
	PacketWriter* createParty(Player* player);
	PacketWriter* inviteParty(Player* player, Player* in);
	PacketWriter* partyError(Player* player, int error);
	PacketWriter* partyReplay(Player* player, char* to);
	*/
	//	Quests
	PacketWriter* LVLMSG();
	PacketWriter* QuestUpdate(short qid,unsigned char stateA,unsigned char stateB,int mobcount);
	PacketWriter* QuestAll(vector <Quest*> ProgarsQ,vector <Quest*> DoneQ);
	PacketWriter* updateQuest(short questid, int npcid, int nextquest=0);
	PacketWriter* updateQuestInfo(Quest* quest, bool forfeit = false);
	PacketWriter* questDone(int questid);
	PacketWriter* itemGainChat(int itemid, int amount);
	PacketWriter* mesosGainChat(int amount);
	// Skills
	PacketWriter* SkllAddLevel(int Inv,int Pos,int Level,short AbyltyPoints);
	PacketWriter* updateSkill(Skill* skill);
	PacketWriter* updateSkill(vector <Skill*>& skills);
	// Reactors
	PacketWriter* showReactor(Reactor* reactor);
	PacketWriter* updateReactor(Reactor* reactor, short stance);
	PacketWriter* destroyReactor(Reactor* reactor);

	// Transportation
	PacketWriter* showTrade(Player* right, Player* left = NULL);
	PacketWriter* inviteTrade(Player* player, int inviteid);
	PacketWriter* joinTrade(Player* player);
	PacketWriter* tradeError(char s);
	PacketWriter* showTradeChat(string msg, bool by);
	PacketWriter* tradeMesos(int mesos, char side);
	PacketWriter* tradeItem(Item* item, char side);
	PacketWriter* tradeConfirm();

	// PvP
	PacketWriter* showPvPMovingForPlayer(Player* player, Position& start);
	PacketWriter* showPvPMoving(Player* player, Position& pos, ObjectMoving& obj);
	PacketWriter* startPvPMobForPlayer(Player* player);
	PacketWriter* startPvPMob(Player* player);
	PacketWriter* startPvPMobEffect(Player* player, int wdef, int mdef);
	PacketWriter* stopPvP(Player* player);
	PacketWriter* stopPvPForPlayer(Player* player);
	PacketWriter* updatePvPHP(Player* player);
	// MyCanges
	PacketWriter* CharHPSPINIT(Player* player);
	PacketWriter* FWDiscauntFaction(Player* player);
	PacketWriter* QUEST_ALL(Player* player);
	PacketWriter* INVEN_ALL(Player* player);
	PacketWriter* SKILL_ALL(Player* player);
	PacketWriter* QUICKSLOTALL(Player* player);
	PacketWriter* STORE_INFO_1(Player* player);
	PacketWriter* STORE_INFO_2(Player* player);
	PacketWriter* STORE_INFO_3(Player* player);
	PacketWriter* STORE_INFO_4(Player* player);
	PacketWriter* STORE_MONEYINFO(Player* player);
	PacketWriter* Enter_Map_Start(Player* player);
	PacketWriter* INVEN_CASH(Player* player);

};

#endif
