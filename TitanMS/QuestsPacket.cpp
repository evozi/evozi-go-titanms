 /*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "PacketCreator.h"
#include "PacketWriter.h"
#include "Quest.h"

PacketWriter* PacketCreator::updateQuest(short questid, int npcid, int nextquest){
	pw.writeShort(QUEST_UPDATE);

	pw.write(8);
	pw.writeShort(questid);
	pw.writeInt(npcid);
	pw.writeInt(nextquest);

	return &pw;
}
PacketWriter* PacketCreator::updateQuestInfo(Quest* quest, bool forfeit){
	pw.writeShort(SHOW_GAIN);

	pw.write(1);
	pw.writeShort(quest->getID());
	pw.write(forfeit ? 0 : (quest->isCompleted() ? 2 : 1));
	if(!forfeit && quest->isCompleted()){
		pw.writeLong(quest->getCompleteTime());
	}
	else{
		if(!forfeit) pw.writeString(quest->getKilledMobs());
		else pw.writeString("");
		pw.writeLong(0);
	}

	return &pw;
}
PacketWriter* PacketCreator::questDone(int questid){
	pw.writeShort(COMPLETE_QUEST);

	pw.writeShort(questid);

	return &pw;
}
PacketWriter* PacketCreator::itemGainChat(int itemid, int amount){
	pw.writeShort(ITEM_GAIN_CHAT);
	
	pw.write(3);
	pw.write(1); // TODO: items num
	pw.writeInt(itemid);
	pw.writeInt(amount);

	return &pw;

}
PacketWriter* PacketCreator::mesosGainChat(int amount){
	pw.writeShort(SHOW_GAIN);
	
	pw.write(5);
	pw.writeInt(amount);

	return &pw;
}
PacketWriter* PacketCreator::LVLMSG(){
	pw.writeBytes("0501");
	pw.writeBytes("8400");
	pw.writeBytes("A600");
	pw.writeBytes("2F02");
	pw.writeBytes("00000000");


	pw.writeBytes("7E790000");
	pw.writeBytes("C0C7C7E0C0BB20BFCFB7E1C7CFBFB4BD");
	pw.writeBytes("C0B4CFB4D92E0000000000000000000000000000000000000000000000000000");
	pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
	pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
	pw.writeBytes("00000000000000000000000000000000000000000000000000000000E8035234");
	pw.writeBytes("623F0000AD52");
return &pw;
}
PacketWriter* PacketCreator::QuestUpdate(short qid,unsigned char stateA,unsigned char stateB,int mobcount){
	pw.writeBytes("0501");
	pw.writeBytes("8000");
	pw.writeBytes("3200");
	pw.writeBytes("B701");
	pw.writeBytes("00000000");

	pw.writeInt(mobcount);
	pw.writeInt(0);
	pw.writeInt(0);
	pw.writeShort(qid);
	pw.write(stateA);
	pw.write(stateB);
	pw.writeInt(0);

	pw.writeShort(qid);
	pw.write(stateA);
	pw.write(stateB);
	pw.writeInt(0);
	pw.writeBytes("E803BC3D394500004F55");
return &pw;
}
PacketWriter* PacketCreator::QuestAll(std::vector<Quest*> ProgarsQ, std::vector<Quest*> DoneQ){
	pw.writeBytes("0501");
	pw.writeBytes("7900");
	pw.writeBytes("E404");
	pw.writeBytes("6206");
	pw.writeBytes("00000000");

	pw.writeInt(0);
	pw.writeInt(0);
	pw.writeInt(0);

int doneq = 0;
	for(int i = 0 ;i < ProgarsQ.size();i++){
			Quest* q = ProgarsQ[i];

			pw.writeShort(q->getID());
			pw.writeBytes("01");
			pw.writeBytes("00000000000000000000000000");
			doneq++;
	}

	for(int i = doneq ; i < 14 ; i++){
			pw.writeShort(0);
			pw.writeBytes("00");
			pw.writeBytes("00000000000000000000000000");
	}

	pw.writeInt(0);

unsigned char tmp[999];
memset(tmp,0x20,999);
	for(int i = 0; i < ProgarsQ.size();i++){
			tmp[DoneQ[i]->getID()-1] = DoneQ[i]->getState();
			//	pw.write(DoneQ[i]->getState());
	}
	pw.write(tmp,999);
	pw.write(0x0);
	return &pw;
}