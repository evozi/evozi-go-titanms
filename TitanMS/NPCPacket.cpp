 /*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "PacketCreator.h"
#include "PlayerNPC.h"
#include "PacketWriter.h"
#include "PlayerInventories.h"
#include "NPC.h"
#include "ShopData.h"
#include "ShopItemData.h"
#include "DataProvider.h"
#include "Tools.h"

using namespace Tools;
PacketWriter* PacketCreator::PSHOP_BUYACK(int Respone){
	pw.writeBytes("0501");//start
	pw.writeBytes("D600");//Head
	pw.writeBytes("2600");//Size
	pw.writeBytes("0102");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(Respone);

	pw.writeBytes("05A218000100000000704000E803A7F5CADB0000C7B5");
	return &pw;
}
PacketWriter* PacketCreator::PSHOP_SELLSTARTACK(int playerID, std::string shopname){
	pw.writeBytes("0501");//start
	pw.writeBytes("D000");//Head
	pw.writeBytes("5200");//Size
	pw.writeBytes("2702");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(playerID);
	pw.writeInt(0);
	pw.writeString(shopname,0x16);
	pw.writeString("",0x16);
	
	pw.writeBytes("0000000000704000E80373DA17BB0000D2E2");
	return &pw;
}
PacketWriter* PacketCreator::PSHOP_SELLINFO(ShopData* data){
	pw.writeBytes("0501");//start
	pw.writeBytes("D200");//Head
	pw.writeBytes("A800");//Size
	pw.writeBytes("7F02");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(data->getID());
	pw.writeInt(data->getGold());
	pw.writeInt(0); //??
	
	vector <ShopItemData*>* items = data->getData();

	for(int i = 0; i < 12;i++){
		ShopItemData* item = (*items)[i];
		if(item->getCount() > 0){
			pw.writeShort(item->getInvUse());
			pw.writeShort(item->getSlotUse());
			pw.writeInt(item->getCount());
			pw.writeInt(item->getPrice());
		}else
		{
			pw.writeBytes("FFFFFFFF");
			pw.writeInt(0);
			pw.writeInt(0);
		}
		
	}
	
	
	return &pw;
}

PacketWriter* PacketCreator::PSHOP_SELLEND(int playerID){
	pw.writeBytes("0501");//start
	pw.writeBytes("D100");//Head
	pw.writeBytes("2600");//Size
	pw.writeBytes("FC01");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(playerID);
	pw.writeBytes("05A21800");
	pw.writeInt(playerID);

	pw.writeBytes("00704000E8039ED326B5000040E3");
	return &pw;
}
PacketWriter* PacketCreator::PSHOP_OPENACK(int slots){
	pw.writeBytes("0501");//start
	pw.writeBytes("CE00");//Head
	pw.writeBytes("2600");//Size
	pw.writeBytes("F901");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(slots);
	pw.writeBytes("05A21800");
	pw.writeInt(slots);
	pw.writeBytes("00104000E8031C061DEA000078D7");
	return &pw;
}
PacketWriter* PacketCreator::showNPC(NPC* npc){
	pw.writeShort(SHOW_NPC);

	pw.writeInt(npc->getID());
	pw.writeInt(npc->getNPCID());
	pw.writeShort(npc->getPosition().x);
	pw.writeShort(npc->getPosition().y);
	pw.write(!npc->getFlip());
	pw.writeShort(npc->getFoothold());
	pw.writeShort(npc->getRX0());
	pw.writeShort(npc->getRX1());
	pw.write(1);

	return &pw;
}
PacketWriter* PacketCreator::bought(){
	pw.writeShort(BUY_RESONSE);

	pw.write(0);

	return &pw;	
}
PacketWriter* PacketCreator::showNPCShop(ShopData* data){
	
	pw.writeBytes("0501");//start
	pw.writeBytes("D400");//Head
	pw.writeBytes("F803");//Size
	pw.writeBytes("D105");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(data->getID());
	pw.writeString(data->getShopName(),0x16);
	pw.writeString("",0x12);
	vector <ShopItemData*>* items = data->getData();
//int remain = 12 - (int)items->size();
	for(int i=0; i< 12; i++){
		ShopItemData* item = (*items)[i];
		if(item->getCount() > 0){
				pw.writeInt(item->getID());
				pw.writeShort(0);
				pw.writeShort(item->getCount());
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(item->getLock());
				pw.writeInt(item->getPrice());
				pw.writeInt(0);
				pw.writeInt(item->getEndTime());
				pw.writeInt(0);
				pw.writeInt(0);
				pw.writeBytes("0000000000000000000000000000000000000000");//20 bytes
				pw.writeShort(0);
				pw.writeShort(0);
				pw.writeInt(0);
				pw.writeInt(0);
				pw.writeInt(0);
				pw.writeInt(0);
		}else
		{
				pw.writeInt(0);
				pw.writeShort(0);
				pw.writeShort(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.write(0);
				pw.writeInt(0);
				pw.writeInt(0);
				pw.writeInt(0);
				pw.writeInt(0);
				pw.writeInt(0);
				pw.writeBytes("0000000000000000000000000000000000000000");//20 bytes
				pw.writeShort(0);
				pw.writeShort(0);
				pw.writeInt(0);
				pw.writeInt(0);
				pw.writeInt(0);
				pw.writeInt(0);

		}
	}
	/*
	pw.writeShort(SHOW_SHOP);

	pw.writeInt(data->getNPC());
	pw.writeShort(data->getCount());
	vector <ShopItemData*>* items = data->getData();
	for(int i=0; i<(int)items->size(); i++){
		ShopItemData* item = (*items)[i];
		pw.writeInt(item->getID());
		pw.writeInt(item->getPrice());
		if(IS_STAR(item->getID())){
			pw.writeShort(0);
			pw.writeInt(0);
			pw.writeShort((short)doubleAsLong(DataProvider::getInstance()->getItemUnitPrice(item->getID())));
		}
		else{
			pw.writeShort(1);
		}
		pw.writeShort(DataProvider::getInstance()->getItemMaxPerSlot(item->getID()));
	}
	*/
	return &pw;

}
void PacketCreator::npcPacket(int npcid, string text, char type){
	pw.writeShort(NPC_TALK);
	
	pw.write(4);
	pw.writeInt(npcid);
	pw.write(type);
	pw.writeString(text);
}
PacketWriter* PacketCreator::sendSimple(int npcid, string text){
	npcPacket(npcid, text, PlayerNPC::SIMPLE);
	return &pw;
}
PacketWriter* PacketCreator::sendYesNo(int npcid, string text){
	npcPacket(npcid, text, PlayerNPC::YES_NO);
	return &pw;
}
PacketWriter* PacketCreator::sendBackNext(int npcid, string text, bool back, bool next){
	npcPacket(npcid, text, PlayerNPC::BACK_NEXT);
	pw.write(back);
	pw.write(next);
	return &pw;
}
PacketWriter* PacketCreator::sendAcceptDecline(int npcid, string text){
	npcPacket(npcid, text, PlayerNPC::ACCEPT_DECLINE);
	return &pw;
}
PacketWriter* PacketCreator::sendGetText(int npcid, string text){
	npcPacket(npcid, text, PlayerNPC::GET_TEXT);
	pw.writeInt(0);
	pw.writeInt(0);
	return &pw;
}
PacketWriter* PacketCreator::sendGetNumber(int npcid, string text, int def, int min, int max){
	npcPacket(npcid, text, PlayerNPC::GET_NUMBER);
	pw.writeInt(def);
	pw.writeInt(min);
	pw.writeInt(max);
	pw.writeInt(0);
	return &pw;
}
PacketWriter* PacketCreator::sendStyle(int npcid, string text, int styles[], char size){
	npcPacket(npcid, text, PlayerNPC::STYLE);
	pw.write(size);
	for(int i=0; i<size; i++)
		pw.writeInt(styles[i]);
	return &pw;
}