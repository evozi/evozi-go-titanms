 /*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "PacketCreator.h"
#include "Worlds.h"
#include "World.h"
#include "Channels.h"
#include "Channel.h"
#include "Character.h"
#include "CharacterEquip.h"
#include "PacketWriter.h"
#include <sstream>
#include <hash_map>
using namespace stdext;

PacketWriter* PacketCreator::nPacketLOGIN_ACK(string p1,string p2,string p3){
	pw.writeShort(LOGIN_HEADER_START);
	pw.writeBytes("2501");
	pw.writeBytes("31");
	pw.write(0);
	pw.write(0);

	pw.writeString(p1);
	pw.writeString(p2);
	pw.writeString(p3);
	pw.writeBytes("55AA");
	pw.putsize();
return &pw;
}
PacketWriter* PacketCreator::loginError(short errorid){
	printf("PacketCreator::loginError\n");
	//Packet packet = Packet();
	//packet.addHeader(0x55AA);
	//packet.addBytes("050031");   
	//packet.addShort(errorid);
	//packet.addBytes("000055AA");
	//pw.writeShort(LOGIN_PROCESS);
	pw.writeShort(LOGIN_HEADER_START);
	pw.writeBytes("050031");
	pw.writeShort(errorid);
	pw.writeShort(0);
	pw.writeShort(LOGIN_HEADER_END);
	return &pw;
}
PacketWriter* PacketCreator::loginProcess(char type){
	printf("PacketCreator::loginProcess\n");
	pw.writeShort(PIN);

	pw.write(type);
	return &pw;
}
PacketWriter* PacketCreator::loginConnect(char gender, string username, bool firstlogin){
	printf("PacketCreator::loginConnect\n");
	/*
		AA 55 05 00 31 00 00 E8 03 55 AA
	*/

	//pw.writeShort(LOGIN_PROCESS);
	pw.writeShort(LOGIN_HEADER_START);
	pw.writeBytes("050031");
	pw.writeShort(0);
	pw.writeShort(1000);
	pw.writeShort(LOGIN_HEADER_END);
	return &pw;
}
PacketWriter* PacketCreator::processOk(){
	printf("PacketCreator::processOk\n");
	pw.writeShort(CHECK_NAME);

	pw.write(0);
	return &pw;
}
PacketWriter* PacketCreator::showWorld(World* world){
	printf("PacketCreator::showWorld\n");
	pw.writeShort(LOGIN_HEADER_START);

	pw.writeBytes("0000");
	pw.writeBytes("33");
	pw.writeBytes("00010203040506D0CFCFCFCFCF");
	pw.writeInt(1);// Number Of Worlds
	pw.writeShort(0);//??
	pw.writeInt(18);//Ch For This World
	//Create 1 Ch
	Channel* ch = world->getChannels()->getChannel(0);

	pw.writeShort(1);
	pw.writeShort(1);//Ch Num
	pw.writeString(Worlds::getInstance()->getStringIP()); //Iplen and Ip string
	pw.writeInt(17101); //Tcp Port
	pw.writeInt(ch->getPlayersCount());
	pw.writeInt(100); //Max Players For Ch
	pw.writeInt(12); //Type icon
	pw.writeInt(0);//??
	pw.write(1);//State
	pw.writeInt(15199);//Udp Port
	
	for(int i = 1; i < 18 ; i++){

		pw.writeShort(i);
		pw.writeShort(i);//Ch Num
		pw.writeString(Worlds::getInstance()->getStringIP()); //Iplen and Ip string
		pw.writeInt(17101); //Tcp Port
		pw.writeInt(0);
		pw.writeInt(100); //Max Players For Ch
		pw.writeInt(12); //Type icon
		pw.writeInt(0);//??
		pw.write(0);//State
		pw.writeInt(15199);//Udp Port
	}
pw.writeShort(LOGIN_HEADER_END);
pw.putsize();
	/*
	pw.writeShort(SHOW_WORLDS);

	pw.write(world->getID());
	pw.writeString(Worlds::getName(world->getID()));
	pw.write(0); //Type 2-new
	pw.writeShort(0);
	pw.write(100);
	pw.write(0);
	pw.write(100);
	pw.writeShort(0);
	pw.write(world->getChannels()->getChannelsCount());
	for(int i=0; i<world->getChannels()->getChannelsCount(); i++){
		stringstream out;
		out << i+1;
		string name = Worlds::getName(world->getID()) + "-" + out.str();
		pw.writeString(name);
		pw.writeInt(0x0); // Pop - TODO
		pw.write(world->getID());
		pw.writeShort(i);
	}
	pw.writeShort(0);
	*/
	return &pw;
}
PacketWriter* PacketCreator::endWorlds(){
	printf("PacketCreator::endWorlds\n");
	pw.writeShort(SHOW_WORLDS);

	pw.write(-1);
	return &pw;
}
PacketWriter* PacketCreator::showChannels(){
	printf("PacketCreator::showChannels\n");
	pw.writeShort(WORLD_STATUS);

	pw.writeShort(0);
	return &pw;
}
PacketWriter* PacketCreator::SelectChRespone(string ip,int TCPP,int UDPP){
	printf("PacketCreator::SelectChRespone\n");
	pw.writeBytes("AA55");
	pw.writeBytes("1900");                                        //Packet Size
	pw.writeBytes("3500");                                        //Packet Id
	pw.writeString(ip);							             	  //Ch IP
	pw.writeBytes("FD3A0000");                                          //Port
	pw.writeBytes("5F3B0000");                                          //Udp
	pw.writeBytes("55AA");
	pw.putsize();
	return &pw;
}
PacketWriter* PacketCreator::showCharacters(vector <Character*>& chars){
	printf("PacketCreator::showCharacters\n");
	pw.writeShort(WORLD_HEADER_START);
	pw.writeBytes("0900");
	pw.writeBytes("9001");
	pw.writeBytes("9E02");
	pw.writeBytes("00000000");

	pw.writeInt(chars.size()); //Players Counter
	int Poss[4]= {-1,-1,-1,-1};
	for(int i=0; i<(int)chars.size();i++)
				Poss[chars[i]->getPos()-1] = i;

	for(int i = 0; i <4 ;i++){
					if(Poss[i] != -1)
						showCharacter(chars[Poss[i]]);
					else
					{
							pw.writeBytes("0000000000000000000000000000000000000000"); //Name
							pw.writeBytes("0000000000000000000000000000000000000000"); //Title
							pw.writeBytes("00"); //Jender
							pw.writeBytes("00");  //Level
							pw.writeBytes("00");    //Job
							pw.writeBytes("00"); //??
							pw.writeBytes("00"); //??
							pw.writeBytes("00");				  //Byte ??
							pw.writeBytes("0000");		 //Short Region
							pw.writeBytes("0000");		//Short Map
							pw.writeBytes("0000");		//Short X
							pw.writeBytes("0000");		//Short Y
							pw.writeBytes("0000");		 //Short ??
							pw.writeInt(0);	//WEP
							pw.writeInt(0);	//DRESS
							pw.writeInt(0);	//INT
							pw.writeInt(0);	//INT
							pw.writeInt(0);	//INT
							pw.writeInt(0);	//EYE
							pw.writeInt(0);	//INT
							pw.writeInt(0);	//INT
							pw.writeInt(0);	//HAIR
							pw.writeInt(0);	//INT
					}
			}
	return &pw;
}

void PacketCreator::showCharacter(Character* character){
	printf("PacketCreator::showCharacter\n");

	//pw.writeInt(character->getID());
	pw.writeString(character->getName(), 20); //Name
	pw.writeString(character->getNickName(), 20); //Title
	pw.write(character->getGender()); //Jender
	pw.write(character->getLevel());  //Level
	pw.write(character->getJob());    //Job
	pw.write(character->getClass());  //??
	pw.write(character->getClassLevel()); //??
	pw.writeBytes("00");				  //Byte ??
	pw.writeBytes("0100");		 //Short Region
	pw.writeBytes("0100");		//Short Map
	pw.writeBytes("0000");		//Short X
	pw.writeBytes("0000");		//Short Y
	pw.writeBytes("0000");//Short ??

	pw.writeInt(character->getEquipid(0));	//WEP
	pw.writeInt(character->getEquipid(1));	//DRESS
	pw.writeInt(0);	//INT
	pw.writeInt(0);	//INT
	pw.writeInt(0);	//INT
	pw.writeInt(0);	//EYE
	pw.writeInt(0);	//INT
	pw.writeInt(0);	//INT
	pw.writeInt(character->getEquipid(7));	//HAIR
	pw.writeInt(character->getEquipid(8));	//INT
}
PacketWriter* PacketCreator::addCharacter(Character* character){
	printf("PacketCreator::addCharacter\n");
	pw.writeBytes("0501");
	pw.writeBytes("0B00");
	pw.writeBytes("1000");
	pw.writeBytes("2001");
	pw.writeBytes("00000000");
	pw.write(1);
	pw.write(character->getPos()); //POS
	//showCharacter(character);
	pw.writeBytes("6A45");
	return &pw;
}
PacketWriter* PacketCreator::checkName(string name, bool ok){
	if(ok)
		ok = false;
	else
		ok = true;
	printf("PacketCreator::checkName\n");
	pw.writeBytes("0501");
	pw.writeBytes("0D001000220100000000");
	pw.write(ok);
	pw.writeBytes("D60508");
	return &pw;
}
PacketWriter* PacketCreator::removeCharacter(int characterid){
	printf("PacketCreator::removeCharacter\n");
	pw.writeBytes("0501");
	pw.writeBytes("0F00");
	pw.writeBytes("1000");
	pw.writeBytes("2401");
	pw.writeBytes("00000000");
	pw.write(characterid);
	pw.writeBytes("5A2A45");//MayBeTime
	pw.write(0);
	return &pw;
}
PacketWriter* PacketCreator::connectChannel(int charid, short port){
	printf("PacketCreator::connectChannel\n");
	pw.writeShort(CHANNEL_INFO);

	pw.writeShort(0);

	IP* ip = Worlds::getInstance()->getIP();
//Selected Server IP

 pw.write(ip->p1); // Put your IP here
 pw.write(ip->p2);
 pw.write(ip->p3);
 pw.write(ip->p4);

	pw.writeShort(port);
	pw.writeInt(charid);
	pw.writeInt(0);
	pw.write(0);
	return &pw;
}
PacketWriter* PacketCreator::logBack(){
	printf("PacketCreator::logBack\n");
	pw.writeShort(LOG_BACK);

	pw.write(1);
	return &pw;
}