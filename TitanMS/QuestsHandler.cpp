/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "DataProvider.h"
#include "PlayerHandler.h"
#include "PacketReader.h"
#include "PacketWriter.h"
#include "PacketCreator.h"
#include "Player.h"
#include "PlayerQuests.h"
void PlayerHandler::questHandle(PacketReader& packet){

}
void PlayerHandler::questGive(PacketReader &packet){
	packet.readInt();

	short Questid = packet.readShort();
	short Var = packet.readShort();

	this->player->getQuests()->giveQuest(Questid,0);
}
void PlayerHandler::questDrop(PacketReader &packet){
	packet.readInt();

	short Questid = packet.readShort();
	short Var = packet.readShort();

	this->player->getQuests()->removeQuest(Questid);
}
void PlayerHandler::questUpdate(PacketReader &packet){
	packet.readInt();

	short Qid = packet.readShort();
	short var = packet.readShort();
	int MobID = packet.readInt();

	player->getQuests()->UpdateQuest(Qid,MobID);

}
void PlayerHandler::questDone(PacketReader &packet){
	packet.readInt();
	short questid = packet.readShort();
	
	player->getQuests()->completeQuest(questid);
}