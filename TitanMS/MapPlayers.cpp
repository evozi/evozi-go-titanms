/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "DataProvider.h"
#include "Player.h"
#include "Players.h"
#include "MapPlayers.h"
#include "PacketCreator.h"
#include "Map.h"
#include "PacketWriter.h"
void MapPlayers::add(Player* player){
	player->send(PacketCreator().Player_WARP_ACK(player));
	map->send(PacketCreator().Player_WARP_ACK(player));
	if(players.size() > 0)
	{
		PacketWriter pw;
		int Start = 0x105;
		int Header =0x44;
		int Size = (playersv.size()*0x104)+0x10;//header + playerscount
		pw.writeShort(Start);
	pw.writeShort(Header);
	pw.writeShort(Size);
	pw.writeShort(Start + Header + Size);
	pw.writeBytes("00000000");

	pw.writeInt(playersv.size());
	player->send(&pw);
		//player->send(PacketCreator().InitHeader(0x105,0x44,(playersv.size()*0x104)+0xc));
		for(int i=0; i<(int)playersv.size(); i++){
			player->send(PacketCreator().showPlayer(playersv[i]));//USER_CREATE
		}
	}


	players[player->getID()] = player;
	playersv.push_back(player);
	
	/*
	for(int i=0; i<(int)playersv.size(); i++){
		if(playersv[i] != player)
		{
			player->send(PacketCreator().Player_WARP_ACK(playersv[i]));
			player->send(PacketCreator().Player_SET_AVATAR(playersv[i]));
		}
	}
	*/
	

}
void MapPlayers::remove(Player* player){
	if(players.find(player->getID()) != players.end()){
		players.erase(player->getID());
	}
	for(int i=0; i<(int)playersv.size(); i++){
		if(playersv[i] == player){
			playersv.erase(playersv.begin()+i);
			break;
		}
	}
	//Need Send->(1F) LEAVE_WARP_ACK
	map->send(PacketCreator().removePlayer(player->getID()), player);
}