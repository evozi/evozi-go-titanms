 /*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "PacketCreator.h"
#include "Channel.h"
#include "PlayerBuffs.h"
#include "Effect.h"
#include "Map.h"
#include "Pet.h"
#include "Player.h"
#include "PlayerInventories.h"
#include "Inventory.h"
#include "ShopData.h"
PacketWriter* PacketCreator::showPlayers(std::vector<Player*> p, Player *player){
	pw.writeBytes("0501");
	pw.writeBytes("4400");
	pw.writeBytes("F8FD");
	pw.writeBytes("41FF");
	pw.writeBytes("00000000");
	pw.writeInt(p.size() - 1);

	for(int i=0; i<(int)p.size(); i++){
		if(p[i] != player){

			Player* pl = p[i];
			pw.writeInt(pl->getID());
			pw.writeString(pl->getName(),16);
			pw.writeString(pl->getTitle(),11);
			pw.writeShort(pl->getPlayerX());
			pw.writeInt(pl->getPlayerY());


		}
			
	}

return &pw;
}
PacketWriter* PacketCreator::showPlayer(Player* player){
	pw.writeInt(player->getID());
	pw.writeString(player->getName(),16);
	pw.writeString(player->getTitle(),10);

	pw.writeShort(player->getPlayerX());
	pw.writeShort(player->getPlayerY());
	pw.write(player->getGender());
	pw.write(player->getLevel());
	pw.write(player->getJob());
	pw.write(-1);//??1
	pw.write(-1);//??2
	pw.write(0);//??3
	pw.write(0);//??4 HidePlayer
	pw.write(0);//??5 ReflectorSkill
	if(player->getNPCShop() != NULL)
		pw.write(1);//??6 PlayerNPC
	else
		pw.write(0);
	pw.write(0);//??7
	pw.write(0);//??8
	pw.write(1);//??9
	pw.write(1);//??10
	pw.write(1);//??11
	pw.write(1);//??12
	pw.write(1);//??13
	pw.write(1);//??14
	pw.write(1);//??15
	
//Hair
	if(player->getInventories()->getInventory(0)->getItemBySlot(7) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(7)->getID());
	else
		pw.writeInt(0);
//Face
	if(player->getInventories()->getInventory(0)->getItemBySlot(9) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(9)->getID());
	else
		pw.writeInt(0);
//Face 2
	if(player->getInventories()->getInventory(0)->getItemBySlot(12) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(12)->getID());
	else
		pw.writeInt(0);
//Hat
	if(player->getInventories()->getInventory(0)->getItemBySlot(6) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(6)->getID());
	else
		pw.writeInt(0);
//Eye
	if(player->getInventories()->getInventory(0)->getItemBySlot(8) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(8)->getID());
	else
		pw.writeInt(0);
//Dress
	if(player->getInventories()->getInventory(0)->getItemBySlot(1) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(1)->getID());
	else
		pw.writeInt(0);
//Dress 2
	if(player->getInventories()->getInventory(0)->getItemBySlot(11) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(11)->getID());
	else
		pw.writeInt(0);//??
//WEP
	if(player->getInventories()->getInventory(0)->getItemBySlot(0) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(0)->getID());
	else
		pw.writeInt(0);
//Mantle
	if(player->getInventories()->getInventory(0)->getItemBySlot(4) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(4)->getID());
	else
		pw.writeInt(0);
//PET   10
	if(player->getInventories()->getInventory(0)->getItemBySlot(10) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(10)->getID());
	else
		pw.writeInt(0);
//HairAcc  14
	if(player->getInventories()->getInventory(0)->getItemBySlot(14) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(14)->getID());
	else
		pw.writeInt(0);
//Toy  15
	if(player->getInventories()->getInventory(0)->getItemBySlot(15) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(15)->getID());
	else
		pw.writeInt(0);

	Pet* p = (Pet*)player->getInventories()->getInventory(0)->getItemBySlot(10);
if(p != NULL){
	pw.writeString(p->getName(),0x10);//PetName
	pw.write(p->getLevel());//PetLevel
	pw.writeBytes("000000");
	pw.writeInt(p->getHP());//HP
	pw.writeInt(p->getMaxMP());
	pw.writeInt(p->getExp());
}else
{
	pw.writeBytes("00000000000000000000000000000000");//PetName
	pw.writeBytes("00");//PetLevel
	pw.writeBytes("000000");//??
	pw.writeBytes("00000000");//PetHP
	pw.writeBytes("00000000");//??
	pw.writeBytes("00000000");//??
}
Pet* t = (Pet*)player->getInventories()->getInventory(0)->getItemBySlot(15);
if(t != NULL){
	pw.writeString(t->getName(),0x10);//PetName
	pw.write(t->getLevel());//PetLevel
	pw.writeBytes("000000");
	pw.writeInt(t->getHP());//HP
	pw.writeInt(t->getMaxMP());

}else{
	pw.writeBytes("00000000000000000000000000000000");//PetName
	pw.writeBytes("00");//PetLevel
	pw.writeBytes("000000");//??
	pw.writeBytes("00000000");//PetHP
	pw.writeBytes("00000000");//??
}

	pw.writeBytes("0000");//Wep Glow ++
	pw.writeBytes("0000");//??

	IP ip = Tools::stringToIP(player->getIP());
	pw.write(ip.p1);
	pw.write(ip.p2);
	pw.write(ip.p3);
	pw.write(ip.p4);

	IP iploc = Tools::stringToIP(player->getLoaclIP());
	pw.write(iploc.p1);
	pw.write(iploc.p2);
	pw.write(iploc.p3);
	pw.write(iploc.p4);

	pw.writeBytes("1F40");
	//shop Name
	if(player->getNPCShop() != NULL){
		ShopData* shop = player->getNPCShop();
		pw.writeString(shop->getShopName(),0x16);
		pw.writeString("",0x14);

	}else{
	pw.writeBytes("000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
	}
	pw.writeInt(0);
	pw.writeInt(0);
	pw.writeInt(0);
	
	pw.writeBytes("0000000000000000000000000000000000000000");

	pw.write(0);//??
	pw.write(0);//Like Warp On Player Effect
	pw.write(0);//??
	pw.write(0);//Buubles From Char Effect
	pw.write(0);//Buubles From Char Effect
	pw.write(0);//??
	pw.writeShort(0);//??
	pw.writeShort(player->getID());// Map Number
	pw.writeBytes("FF00");
	printf("UserCreate Size %x",pw.getLength());
	return &pw;
}
PacketWriter* PacketCreator::removePlayer(int playerid){
	pw.writeBytes("0501");
	pw.writeBytes("1F00");
	pw.writeBytes("2600");
	pw.writeBytes("4A01");
	pw.writeBytes("00000000");

	pw.writeInt(playerid);
	pw.writeBytes("05A218007B04000000104000E8033F6BA74200009523");
	return &pw;
}
PacketWriter* PacketCreator::changeMap(Player* player){
	pw.writeShort(CHANGE_MAP);

	pw.writeInt(player->getChannel()->getID());
	pw.writeInt(0);
	pw.writeInt(player->getMap()->getID());
	pw.write(player->getMappos());
	pw.writeShort(player->getHP());
	pw.write(0);
	pw.writeInt(-1);
	pw.writeShort(-1);
	pw.write(-1);
	pw.write(1);

	return &pw;
}
PacketWriter* PacketCreator::changeSound(string sound){
	return mapChange(6, sound);
}
PacketWriter* PacketCreator::showEffect(string effect){
	return mapChange(3, effect);
}
PacketWriter* PacketCreator::playSound(string sound){
	return mapChange(4, sound);
}
PacketWriter* PacketCreator::mapChange(char mode, string name){
	pw.writeShort(MAP_EFFECT);

	pw.write(mode);
	pw.writeString(name);

	return &pw;
}