/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef SHOPDATA_H
#define SHOPDATA_H

#include "TopDataStruct.h"
class ShopItemData;

class ShopData : public TopDataStruct<ShopItemData>, public DataStruct {
private:
	int npc;
	int Gold;
	string ShopName;
public:
	ShopData(int id){
		this->id = id;
		npc=0;
		Gold = 0;
	}
	void setGold(int amount){
		Gold += amount;
	}
	int getGold(){
		return Gold;
	}
	void setShopTitle(string name){
		this->ShopName = name;
	}
	string getShopName(){
		return ShopName;
	}
	void setNPC(int npc){
		this->npc = npc;
	}
	int getNPC(){
		return npc;
	}
};


#endif