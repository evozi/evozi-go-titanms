/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef PLAYERHANDLER_H
#define PLAYERHANDLER_H

class PacketReader;
class Player;
class PlayerHandler;
#include "Handler.h"

class PlayerHandler : public Handler<PlayerHandler> {
private:

	static const short CONNECT =0x18;
	static const short PING = 0x100;
	static const short TEMP1D = 0x179;
	// NPCs
	static const short HANDLE_NPC = 0x36;
	static const short NPC_CHAT = 0x38;
	static const short USE_SHOP = 0x39;
	// Channels
	static const short CHANGE_CHANNEL = 0x24;
	// Player
	static const short PSHOP_BUY_REQ = 0xD5;
	static const short PSHOP_SELL_END = 0xD1;
	static const short PSHOP_INFO_REQ = 0xD3;
	static const short OPEN_SHOP_REQ = 0xCF;
	static const short START_SHOP_REQ = 0xCD;
	static const short PVP_ACK = 0xA5;
	static const short PVP_REQ = 0xA4;
	static const short TAKE_DAMAGE = 0x46;
	static const short PLAYER_DIE = 0x47;
	static const short PLAYER_MOVE = 0x26;
	static const short CLICK_INFO = 0x0214;
	static const short CHAR_INFO = 0x0235;
	static const short WARP_REQ = 0x85;
	static const short CHANGE_MAP = 0x1D;
	static const short CHANGE_MAP_SPECIAL = 0x5C;
	static const short PLAYER_INFO = 0x59;
	static const short FACE_EXP = 0x30;
	static const short RECOVERY = 0x51;
	static const short TRANSPORTATION = 0x6E;
	static const short CHAR_GSM_ONOFF = 0x25F;

	// Mobs
	static const short DAMAGE_MOB = 0x45;
	static const short DAMAGE_MOB_RANGED = 0x2A;
	static const short DAMAGE_MOB_MAGIC = 0x2B;
	static const short CONTROL_MOB = 0x9A;
	static const short CONTROL_MOB_SKILL = 0xA0; // to find
	static const short HIT_BY_MOB = 0x9E; // to find
	// Skills
	static const short ADD_SKILL_LEVEL = 0x74;
	static const short ADD_SKILL = 0x52;
	static const short USE_SKILL = 0x76;
	static const short STOP_SKILL = 0x54;
	// Chat
	static const short USE_CHAT = 0x17;
	static const short CHAT_COMMAND = 0x6B;
	// Inventory
	static const short UNLOCK_ITEM = 0x136;
	static const short BUY_CASH_ITEM = 0xE7;
	static const short BUY_NPC_ITEM = 0x22;
	static const short SELL_NPC_ITEM = 0x23;
	static const short CANCEL_CHAIR = 0x27;
	static const short USE_CHAIR = 0x28;
	static const short USE_ITEM_EFFECT = 0x31;
	static const short AUTO_ARRANGEMENT = 0x40;
	static const short MOVE_ITEM = 0x6C;
	static const short USE_ITEM = 0x6F;
	static const short CANCEL_ITEM_BUFF = 0x44;
	static const short USE_SUMMON_BUG = 0xFF;
	static const short USE_CASH_ITEM = 0x49;
	static const short USE_RETURN_SCROLL = 0x4E;
	static const short USE_SCROLL = 0x4F;
	// Pet
	static const short USE_PET = 0x5A;
	static const short MOVE_PET = 0x89; 
	static const short PET_COMMAND_TEXT = 0x8A;
	static const short PET_COMMAND = 0x8B;
	// Stats
	static const short ADD_STAT = 0x5F;
	// Drops
	static const short DROP_MESOS = 0x56;
	static const short LOOT_DROP = 0x4C;
	// Quests
	static const short DONE_QUEST = 0x7C;
	static const short UPDATE_QUEST = 0x7F;
	static const short GIVE_QUEST = 0x7A;
	static const short DROP_QUEST = 0x7B;
	static const short QUEST_HANDLE = 0x62;
	// Keys
	static const short CHANGE_KEY = 0x99;
	// Reactors
	static const short HIT_REACTOR = 0xAB;

	/*
	static const short CONNECT = 0x14;
	static const short PING = 0xA;
	// NPCs
	static const short NPC_CHAT = 0x21;
	static const short HANDLE_NPC = 0x23;
	static const short USE_SHOP = 0x22;
	// Channels
	static const short CHANGE_CHANNEL = 0x27;
	// Player
	static const short TAKE_DAMAGE = 0x2A;
	static const short PLAYER_MOVE = 0x35;
	static const short CHANGE_MAP = 0x2F;
	static const short PLAYER_INFO = 0x44;
	static const short FACE_EXP = 0x5C;
	static const short RECOVERY = 0x67;
	static const short TRANSPORTATION = 0x3E;
	// Mobs
	static const short DAMAGE_MOB_MAGIC = 0x2E;
	static const short DAMAGE_MOB = 0x59;
	static const short DAMAGE_MOB_RANGED = 0x36;
	static const short CONTROL_MOB = 0x9D;
	static const short CONTROL_MOB_SKILL = 0xA0;
	static const short HIT_BY_MOB = 0x9E;
	// Skills
	static const short ADD_SKILL = 0x4D;
	static const short STOP_SKILL = 0x4E;
	static const short USE_SKILL = 0x51;
	// Chat
	static const short USE_CHAT = 0x2C;
	static const short CHAT_COMMAND = 0x58;
	// Inventory
	static const short MOVE_ITEM = 0x62;
	static const short USE_CHAIR = 0x2D;
	static const short CANCEL_CHAIR = 0x2B;
	static const short USE_ITEM_EFFECT = 0x5D;
	static const short USE_CASH_ITEM = 0x53;
	static const short USE_ITEM = 0x63;
	static const short USE_SCROLL = 0x65;
	static const short USE_RETURN_SCROLL = 0x64;
	static const short USE_SUMMON_BUG = 0x4B;
	static const short CANCEL_ITEM_BUFF = 0x49;
	// Pet
	static const short USE_PET = 0x45;
	static const short MOVE_PET = 0x84;
	static const short PET_COMMAND = 0x80; 
	static const short PET_COMMAND_TEXT = 0x82; 
	// Stats
	static const short ADD_STAT = 0x66;
	// Drops
	static const short DROP_MESOS = 0x68;
	static const short LOOT_DROP = 0x89;
	// Quests
	static const short QUEST_HANDLE = 0x6B;
	// Keys
	static const short CHANGE_KEY = 0x75;
	// Reactors
	static const short HIT_REACTOR = 0x8C; */

	Player* player;
public:
	PlayerHandler(Player* player){
		this->player = player;
	}
	static void loadPackets();

	void connectionRequestHandle(PacketReader& packet);
	void Temp(PacketReader& packet);
	void pingHandle(PacketReader& packet);
	// NPCs
	void PSHOP_BUYREQ(PacketReader& packet);
	void PSHOP_INFOREQ(PacketReader& packet);
	void PSHOP_OPENSTARTREQ(PacketReader& packet);
	void NPCHandle(PacketReader& packet);
	void NPCChatHandle(PacketReader& packet);
	// TODO: controlled npcs
	void BUY_CASH(PacketReader& packet);
	void NPC_SELL(PacketReader& packet);
	void useShopHandle(PacketReader& packet);
	// Channels - TODO
	void changeChannelHandle(PacketReader& packet);
	// Player
	void PSHOP_SELLEND(PacketReader& packet);
	void PSHOP_OPENREQ(PacketReader& packet);
	void playerPvpAck(PacketReader& packet);
	void playerPvpReq(PacketReader& packet);
	void playerClickInfo(PacketReader& packet);
	void playerCharInfo(PacketReader& packet);
	void playerDieHandle(PacketReader& packet);
	void playerTakeDamageHandle(PacketReader& packet);
	void playerToGSM(PacketReader& packet);
	void playerMovingHandle(PacketReader& packet);
	void cangeMapRewquest(PacketReader& packet);
	void changeMapHandle(PacketReader& packet);
	void changeMapSpecialHandle(PacketReader& packet);
	void getPlayerInformationHandle(PacketReader& packet);
	void useFaceExpressionHandle(PacketReader& packet);
	void recoveryHealthHandle(PacketReader& packet);
	void itemsTransportationHandle(PacketReader& packet);
	// Mobs
	void damageMobMagicHandle(PacketReader& packet);
	void damageMobHandle(PacketReader& packet);
	void damageMobRangedHandle(PacketReader& packet);
	void controlMobHandle(PacketReader& packet);
	void controlMobSkillHandle(PacketReader& packet); //?
	void hitByMobHandle(PacketReader& packet);
	// Skills
	void updtaeSkillLevel(PacketReader& packet);
	void addSkillHandle(PacketReader& packet);
	void stopSkillHandle(PacketReader& packet);
	void useSkillHandle(PacketReader& packet);
	// Chat
	void chatCommandHandle(PacketReader& packet);
	void useChatHandle(PacketReader& packet);
	// Inventory
	void cancelChairHandle(PacketReader& packet);
	void useChairHandle(PacketReader& packet);
	void useItemEffectHandle(PacketReader& packet);
	void unlockItemHandle(PacketReader& packet);
	void useCashItemHandle(PacketReader& packet);
	void SellItemHandle(PacketReader& packet);
	void moveItemHandle(PacketReader& packet);
	void useItemHandle(PacketReader& packet);
	void useReturnScrollHandle(PacketReader& packet);
	void useScrollHandle(PacketReader& packet);
	void useSummonBugHandle(PacketReader& packet);
	void cancelItemBuffHandle(PacketReader& packet);
	void autoArrangementHandle(PacketReader& packet);
	// Pet
	void usePetHandle(PacketReader& packet);
	void movePetHandle(PacketReader& packet);
	void petCommandHandle(PacketReader& packet);
	void petCommandTextHandle(PacketReader& packet);
	//Stats
	void addStatHandle(PacketReader& packet);
	// Drops
	void dropMesosHandle(PacketReader& packet);
	void lootDropHandle(PacketReader& packet);
	// Quests
	void questDone(PacketReader& packet);
	void questUpdate(PacketReader& packet);
	void questGive(PacketReader& packet);
	void questDrop(PacketReader& packet);
	void questHandle(PacketReader& packet);
	// Keys
	void changeKeyHandle(PacketReader& packet);
	// Reactor
	void hitReactorHandle(PacketReader& packet);
};

#endif