/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "DataProvider.h"
#include "Channel.h"
#include "Maps.h"
#include "Players.h"
#include "Player.h"
#include "PacketCreator.h"
#include "Transportations.h"
#include "Map.h"
#include "Worlds.h"
#include "PacketReader.h"
class PacketReader;

void Channel::handleRequest(unsigned char *buf,int len){
	 short header;
		//header = buf[2] + buf[3]*0x100;
		//printf("CHANNEL HANDLER [%x]\n",header);

		PacketReader pp =  PacketReader(buf, len, 0);
		pp.readInt();
		int PlayerID = pp.readInt();
		printf("UDP Header: %d\n",pp.getHeader());
		printf("PlayerId : %d\n",PlayerID);
		if(pp.getHeader() == 221){
			float x = pp.readFloat();
			float y = pp.readFloat();
			//printf("PlayerXY : %d,%d\n",(int)x,(int)y);
			Player* p = players->getPlayerByID(PlayerID);
			p->setPlayerX((int)x);
			p->setPlayerY((int)y);
		}
	
	
}
Channel::Channel(World* world, int id, int port){
	this->id = id;
	maps = new Maps(this);
	players = new Players();
	trans = new Transportations();
	notice = Worlds::getInstance()->getDefaultNotice();
	this->port = port;
}
Channel::~Channel(){

}
int Channel::getPlayersCount(){
		return players->getPlayersCount();
}
void Channel::addPlayer(Player* player){
	players->add(player);
//player->getMap()->addPlayer(player);
	//player->send(PacketCreator().showMassage(notice, 4, 0, true));
	
}
Map* Channel::getMap(int id){
	return maps->getMap(id);
}