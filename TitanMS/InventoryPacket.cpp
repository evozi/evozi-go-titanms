 /*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "PacketCreator.h"
#include "PacketWriter.h"
#include "PlayerInventories.h"
#include "DataProvider.h"
#include "Item.h"
#include "Pet.h"
#include "ObjectMoving.h"
#include "Equip.h"
#include "Tools.h"

#include "Inventory.h"
using namespace Tools;

PacketWriter* PacketCreator::emptyInventoryAction(){
	pw.writeShort(INVENTORY_CHANGE);

	pw.write(1);
	pw.write(0);
	return &pw;
}
PacketWriter* PacketCreator::INVEN_EQUIP(Inventory* inv){
	pw.writeBytes("0501");//start
	pw.writeBytes("6500");//Head
	pw.writeBytes("0604");//Size
	pw.writeBytes("7005");//Crc
	pw.writeBytes("00000000");//PacketCounter

	for(int i = 0;i < 19;i++){//IDS
		Equip* item = (Equip*)inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeInt(item->getID());
		else
			pw.writeInt(0);
	}
	for(int i = 0;i < 19;i++){//Fusions Used
		pw.write(0);
	}

	for(int i = 0;i < 19;i++){//Item ++
		pw.writeBytes("00000000000000000000");
	}

	pw.write(0); //??

	//SealValue
	for(int i = 0;i < 19;i++){
		Equip* item = (Equip*)inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeShort(item->GetSealVal());
		else
			pw.writeShort(0);
	}

	//Small Icon On Item
	for(int i = 0;i < 19;i++){
		Equip* item = (Equip*)inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeShort(item->getIcon());
		else
			pw.writeShort(0);
	}

	for(int i = 0;i < 19;i++){//??
		pw.writeBytes("0000000000000000000000000000000000000000");
	}
Pet* p = (Pet*)inv->getItemBySlot(10);
if(p != NULL)
{
	pw.writeString(p->getName(),20); //PetName
	pw.write(p->getLevel()); //PetLevel
	pw.write(0); //??
	pw.writeInt(p->getHP()); //PetHP
	pw.writeInt(p->getMP()); //LifhGauge
	pw.writeInt(p->getExp()); //EXP
	pw.writeInt(p->getPetDeco()); //PetDeco
	pw.writeInt(-1); //??
	pw.writeInt(-1); //PetENDdate
	pw.write(0); //??
}else
{
	pw.writeBytes("0000000000000000000000000000000000000000"); //PetName
	pw.write(0); //PetLevel
	pw.write(0); //??
	pw.writeInt(0); //PetHP
	pw.writeInt(0); //??
	pw.writeInt(0); //??
	pw.writeInt(-1); //??
	pw.writeInt(-1); //??
	pw.writeInt(-1); //PetENDdate
	pw.write(0); //??
}
	pw.writeBytes("0000000000000000000000000000000000000000"); //ToyName
	pw.write(0); //PetLevel
	pw.writeShort(0); //??
	pw.writeInt(0); //??
	pw.writeInt(0); //ToyHP
	pw.writeInt(0); //??

	pw.writeInt(-1); //ToyEndDate
	pw.writeInt(-1); //WepEndDate
	pw.writeInt(-1); //outfitEndDate
	pw.writeInt(-1); //HatEndDate
	pw.writeInt(-1); //?? EndDate

	for(int i = 0;i < 19;i++){//??
		pw.writeInt(0);
	}

	for(int i = 0;i < 19;i++){//??
		pw.writeInt(0);
	}
	pw.writeBytes("000000000000000000104000E803663CB1360000C1CD"); //Junk
	printf("SPEND3 Psize : %X\n",pw.getLength());
	return &pw;
}
PacketWriter* PacketCreator::INVEN_EQUIP1(Inventory* inv){
	pw.writeBytes("0501");//start
	pw.writeBytes("6600");//Head
	pw.writeBytes("E209");//Size
	pw.writeBytes("4D0B");//Crc
	pw.writeBytes("00000000");//PacketCounter


	//int (ID) *48
	for(int i = 0;i < 48;i++){
		Equip* item = (Equip*)inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeInt(item->getID());
		else
			pw.writeInt(0);
	}
	//Fusion
	for(int i = 0;i < 48;i++){
		pw.write(0);
	}
	//Item ++ 
	for(int i = 0;i < 48;i++){
		pw.writeBytes("00000000000000000000");
	}
	//?? 
	for(int i = 0;i < 48;i++){
		pw.writeShort(0);
	}
	//(LOCK)
	for(int i = 0;i < 48;i++){
		Equip* item = (Equip*)inv->getItemBySlot(i);
		if(item != NULL)
			pw.write(item->getLocked());
		else
			pw.write(0);
	}
	//End Date
	for(int i = 0;i < 48;i++){
		Equip* item = (Equip*)inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeInt(item->getEndDate());
		else
			pw.writeInt(0);
	}
	//Small Icon On Item
	for(int i = 0;i < 48;i++){
		Equip* item = (Equip*)inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeShort(item->getIcon());
		else
			pw.writeShort(0);
	}
	//??
	for(int i = 0;i < 48;i++){
		pw.writeBytes("0000000000000000000000000000000000000000");
	}
	//Extra Display
	for(int i = 0;i < 48;i++){
		pw.writeInt(0);
	}
	//??
	for(int i = 0;i < 48;i++){
		pw.writeInt(0);
	}
	pw.writeBytes("010000000000000000104000E8039536F0410000342C"); //Junk
	printf("EQUIP1 Psize : %X\n",pw.getLength());
	return &pw;
}
PacketWriter* PacketCreator::INVEN_EQUIP2(Inventory* inv){
	pw.writeBytes("0501");//start
	pw.writeBytes("6700");//Head
	pw.writeBytes("2206");//Size
	pw.writeBytes("8E07");//Crc
	pw.writeBytes("00000000");//PacketCounter

	//int (ID) *48
	for(int i = 0;i < 48;i++){//IDS
		Equip* item = (Equip*)inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeInt(item->getID());
		else
			pw.writeInt(0);
	}
	//BYTE (??) *48
	for(int i = 0;i < 48;i++){//Fusions Used
		pw.write(0);
	}
	//10Byte (ITEM ++) *48
	for(int i = 0;i < 48;i++){//Item ++
		pw.writeBytes("00000000000000000000");
	}
	//short (SheldsVal) *48
	for(int i = 0;i < 48;i++){
		Equip* item = (Equip*)inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeShort(item->GetSealVal());
		else
			pw.writeShort(0);
	}
	//byte (LOCK) *48
	for(int i = 0;i < 48;i++){//Lock
		Equip* item = (Equip*)inv->getItemBySlot(i);
		if(item != NULL)
			pw.write(item->getLocked());
		else
			pw.write(0);
	}
	//short (ItemIcon) *48
	for(int i = 0;i < 48;i++){
		Equip* item = (Equip*)inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeShort(item->getIcon());
		else
			pw.writeShort(0);
	}
	//int (??-1) *48
	for(int i = 0;i < 48;i++){
		Equip* item = (Equip*)inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeInt(item->getEndDate());
		else
			pw.writeInt(0);
	}
	//int (??) *48
	for(int i = 0;i < 48;i++){
		pw.writeInt(0);
	}
	//int (??) *48
	for(int i = 0;i < 48;i++){
		pw.writeInt(0);
	}
	
	pw.writeBytes("000000000000000000704000E803B7D9E7BB00002AD5");
	printf("EQUIP2 Psize : %X\n",pw.getLength());
	return &pw;
}
PacketWriter* PacketCreator::INVIN_SPEND3(Inventory* inv){
	pw.writeBytes("0501");//start
	pw.writeBytes("6800");//Head
	pw.writeBytes("4003");//Size//pw.writeBytes("5603");//Size
	pw.writeBytes("AD04");//Size//pw.writeBytes("C304");//Crc
	pw.writeBytes("00000000");//PacketCounter
	
	for(int i = 0;i < 48;i++){//IDS
		Item* item = inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeInt(item->getID());
		else
			pw.writeInt(0);
	}
	for(int i = 0;i < 48;i++){//Amount
		Item* item = inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeShort(item->getAmount());
		else
			pw.writeShort(0);
	}
	for(int i = 0;i < 48;i++){//??
		pw.writeInt(0);
	}
	for(int i = 0;i < 48;i++){//Lock
		Item* item = inv->getItemBySlot(i);
		if(item != NULL)
			pw.write(item->getLocked());
		else
			pw.write(0);
	}
	//END DATE
	for(int i = 0;i < 48;i++){
		Item* item = inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeInt(item->getEndDate());
		else
			pw.writeInt(0);
	}

	pw.writeBytes("03FF"); //??
	
	for(int i = 0;i < 48;i++){//??
		pw.writeShort(0);
	}

	pw.writeBytes("FFFF"); //??
	//pw.writeBytes("000000000000FFFF00704000E803B7D9E7BB0000C5A0"); //junk.
	printf("SPEND3 Psize : %X\n",pw.getLength());
	return &pw;
}
PacketWriter* PacketCreator::INVEN_OTHER4(Inventory* inv){
	pw.writeBytes("0501");//start
	pw.writeBytes("6900");//Head
	pw.writeBytes("9202");//Size
	pw.writeBytes("0004");//Crc
	pw.writeBytes("00000000");//PacketCounter

	for(int i = 0;i < 48;i++){//IDS
		Item* item = inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeInt(item->getID());
		else
			pw.writeInt(0);
	}
	for(int i = 0;i < 48;i++){//Amount
		Item* item = inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeShort(item->getAmount());
		else
			pw.writeShort(0);
	}
	for(int i = 0;i < 48;i++){//Lock
		Item* item = inv->getItemBySlot(i);
		if(item != NULL)
			pw.write(item->getLocked());
		else
			pw.write(0);
	}
	for(int i = 0;i < 48;i++){//END DATE
		pw.writeInt(-1);
	}
	for(int i = 0;i < 48;i++){//Small Icon On Item
		pw.writeShort(0);
	}
	pw.writeBytes("000000000000000000704000E803B7D9E7BB000042F4");
	printf("OTHER Psize : %X\n",pw.getLength());
	return &pw;
}
PacketWriter* PacketCreator::INVEN_PET5(Inventory* inv){
	pw.writeBytes("0501");//start
	pw.writeBytes("6A00 ");//Head
	pw.writeBytes("420A");//Size
	pw.writeBytes("B10B");//Crc
	pw.writeBytes("00000000");//PacketCounter
	
	//INT (ID) *48
	for(int i = 0;i < 48;i++){
		Item* item = inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeInt(item->getID());
		else
			pw.writeInt(0);
	}
	
	//string20 (Name) *48
	for(int i = 0;i < 48;i++){
		Pet* p = (Pet*)inv->getItemBySlot(i);
		if(p != NULL)
			if(IS_PET(p->getItemID()) || IS_TOY(p->getItemID()))
				pw.writeString(p->getName(),20);
			else
				pw.writeBytes("0000000000000000000000000000000000000000");
		else
			pw.writeBytes("0000000000000000000000000000000000000000");
	}
	//Byte (Level) *48
	for(int i = 0;i < 48;i++){
		Pet* p = (Pet*)inv->getItemBySlot(i);
		if(p != NULL)
			if(IS_PET(p->getItemID()) || IS_TOY(p->getItemID()))
				pw.write(p->getLevel());
			else
				pw.writeBytes("00");
		else
			pw.writeBytes("00");
	}
	//Int (HP) *48
	for(int i = 0;i < 48;i++){
		Pet* p = (Pet*)inv->getItemBySlot(i);
		if(p != NULL)
			if(IS_PET(p->getItemID()) || IS_TOY(p->getItemID()))
				pw.writeInt(p->getHP());
			else
				pw.writeInt(0);
		else
			pw.writeInt(0);
	}
	//int (EXP) *48
	for(int i = 0;i < 48;i++){
		Pet* p = (Pet*)inv->getItemBySlot(i);
		if(p != NULL)
			if(IS_PET(p->getItemID()) || IS_TOY(p->getItemID()))
				pw.writeInt(p->getExp());
			else
				pw.writeInt(0);
		else
			pw.writeInt(0);
	}
	//int (??) *48
	for(int i = 0;i < 48;i++){
		Pet* p = (Pet*)inv->getItemBySlot(i);
		if(p != NULL)
			if(IS_PET(p->getItemID()) || IS_TOY(p->getItemID()))
				pw.writeInt(p->getMP());
			else
				pw.writeInt(0);
		else
			pw.writeInt(0);
	}
	//int (EndDate) *48
	for(int i = 0;i < 48;i++){
		pw.writeInt(-1);
	}
	//short (ItemCount-if no pet) *48
	for(int i = 0;i < 48;i++){//Amount
		Item* item = inv->getItemBySlot(i);
		if(item != NULL)
			pw.writeShort(item->getAmount());
		else
			pw.writeShort(0);
	}
	//int (??) *48
	for(int i = 0;i < 48;i++){
		pw.writeInt(0);
	}
	//Byte (LOCK) *48
	for(int i = 0;i < 48;i++){//Lock
		Item* item = inv->getItemBySlot(i);
		if(item != NULL)
			pw.write(item->getLocked());
		else
			pw.write(0);
	}
	//int (??) *48
	for(int i = 0;i < 48;i++){
		pw.writeInt(-1);
	}
	//short (SmallIcon) *48
	for(int i = 0;i < 48;i++){
		pw.writeShort(0);
	}

	pw.writeBytes("000000000000000000704000E803B7D9E7BB0000EBB9");
	printf("PET5 size : %X\n",pw.getLength());
	return &pw;

}
PacketWriter* PacketCreator::INVEN_UPDATE(Inventory* inv,int invuse){
	if(invuse==0)
		return INVEN_EQUIP(inv);
	if(invuse==1)
		return INVEN_EQUIP1(inv);
	if(invuse==2)
		return INVEN_EQUIP2(inv);
	if(invuse==3)
		return INVIN_SPEND3(inv);
	if(invuse==4)
		return INVEN_OTHER4(inv);
	if(invuse==5)
		return INVEN_PET5(inv);
}
void PacketCreator::itemInfo(Item* item, bool showPosition, bool showZeroPosition){
	char slot = item->getSlot();
	bool cash = false;
	if(showPosition){
		if(slot < 0){ // EQUIPED
			if(slot < -100){
				cash = true;
				pw.write(0);
				pw.write(-(slot+100));
			}
			else{
				pw.write(-slot);
			}
		}
		else{
			pw.write(slot);
		}
	}
	else if(showZeroPosition){
		pw.write(0);		
	}
	pw.write(item->getType() == EQUIP ? 1 : (IS_PET(item->getID()) ? 3 : 2)); // 1 for equips and 2 for items 3 for pets
	pw.writeInt(item->getID());
	if(IS_PET(item->getID())){
		pw.write(1); 
		pw.writeLong(((MapObject*)((Pet*)item))->getID()); 
		pw.write(0);
		pw.writeBytes("8005"); // Cash?

	}/*
	else if(DataProvider::getInstance()->getItemCash(item->getID())){
		pw.write(1); 
		pw.writeLong(getTimeByDays(90)); // Time?
		pw.writeBytes("80206F"); // Cash?
	}*/
	else{
		pw.writeShort(0);
		pw.writeBytes("8005");
	}

	pw.writeBytes("BB46E617");
	pw.write(2);

	if(item->getType() == EQUIP){
		Equip* equip = (Equip*)item;
		pw.write(equip->getSlots());
		pw.write(equip->getScrolls());
		pw.writeShort(equip->getStr());
		pw.writeShort(equip->getDex());
		pw.writeShort(equip->getInt());
		pw.writeShort(equip->getLuk());
		pw.writeShort(equip->getHP());
		pw.writeShort(equip->getMP());
		pw.writeShort(equip->getWAtk());
		pw.writeShort(equip->getMAtk());
		pw.writeShort(equip->getWDef());
		pw.writeShort(equip->getMDef());
		pw.writeShort(equip->getAcc());
		pw.writeShort(equip->getAvo());
		pw.writeShort(equip->getHand());
		pw.writeShort(equip->getJump());
		pw.writeShort(equip->getSpeed());
		pw.writeString(equip->getOwner());
		pw.write(equip->getLocked());
		if(!cash){
			pw.write(0);
			pw.writeLong(0);	
		}
	}
	else if(IS_PET(item->getID())){
		Pet* pet = (Pet*)item;
		pw.writeString(pet->getName(), 13);
		pw.write(pet->getLevel());
		pw.writeShort(pet->getCloseness());
		pw.write(pet->getFullness());
		pw.writeLong(pet->getTime()); 
		pw.writeInt(1); // Or 0?
	}
	else{
		pw.writeShort(item->getAmount());
		pw.writeString(""); // owner, but items don't have owner
		pw.writeShort(item->getLocked());
		if(IS_STAR(item->getID())){
			pw.writeInt(2);
			pw.writeShort(0x54);
			pw.write(0);
			pw.write(0x34);
		}
	}
}

PacketWriter* PacketCreator::moveItem(char inv, short slot1, short slot2, char equip){
	pw.writeShort(INVENTORY_CHANGE);

	pw.write(1);
	pw.write(1);
	pw.write(2);
	pw.write(inv);
	pw.writeShort(slot1);
	pw.writeShort(slot2);
	if(equip)
		pw.write(equip);
	return &pw;
}
PacketWriter* PacketCreator::updatePet(Pet* pet, bool drop){
	pw.writeShort(INVENTORY_CHANGE);

	pw.write(drop);
	pw.write(2);
	pw.write(3);
	pw.write(pet->getType());
	pw.writeShort(pet->getSlot());
	pw.write(0);
	pw.write(pet->getType());
	pw.writeShort(pet->getSlot());
	itemInfo(pet, false);

	return &pw;
}
PacketWriter* PacketCreator::updateSlot(Item* item, bool drop){
	pw.writeShort(INVENTORY_CHANGE);

	pw.write(drop);
	pw.write(1);
	pw.write(1);
	pw.write(item->getType());
	pw.writeShort(item->getSlot());
	pw.writeShort(item->getAmount());
	return &pw;
}/*
PacketWriter* PacketCreator::updateItem(Item* item, bool drop){
	pw.writeShort(INVENTORY_CHANGE);

	pw.write(drop);
	pw.write(1);
	pw.write(2);
	pw.write(item->getType());
	pw.writeShort(item->getSlot());
	itemInfo(item, true);
	return &pw;
}*/
PacketWriter* PacketCreator::newItem(Item* item, bool drop){
	pw.writeShort(INVENTORY_CHANGE);

	pw.write(drop);
	pw.write(1);
	pw.write(0);
	pw.write(item->getType());
	pw.writeShort(item->getSlot());
	itemInfo(item, false);
	return &pw;
}
PacketWriter* PacketCreator::removeItem(char inv, int slot, bool drop){
	pw.writeShort(INVENTORY_CHANGE);

	pw.write(drop);
	pw.write(1);
	pw.write(3);
	pw.write(inv);
	pw.writeShort(slot);
	if(slot < 0)
		pw.write(1);
	return &pw;
}
PacketWriter* PacketCreator::moveItemMerge(char inv, short slot1, short slot2, short amount){
	pw.writeShort(INVENTORY_CHANGE);

	pw.write(1);
	pw.write(2);
	pw.write(3);
	pw.write(inv);
	pw.writeShort(slot1);
	pw.write(1);
	pw.write(inv);
	pw.writeShort(slot2);
	pw.writeShort(amount);
	return &pw;
}
PacketWriter* PacketCreator::moveItemMergeTwo(char inv, short slot1, short amount1, short slot2, short amount2){
	pw.writeShort(INVENTORY_CHANGE);

	pw.write(1);
	pw.write(2);
	pw.write(1);
	pw.write(inv);
	pw.writeShort(slot1);
	pw.writeShort(amount1);
	pw.write(1);
	pw.write(inv);
	pw.writeShort(slot2);
	pw.writeShort(amount2);
	return &pw;
}
PacketWriter* PacketCreator::scrollItem(Item* scroll, Item* equip, bool destroyed){
	pw.writeShort(INVENTORY_CHANGE);

	pw.write(1);
	pw.write((destroyed) ? 2 : 3);
	pw.write((scroll->getAmount() > 1) ? 1 : 3);
	pw.write(USE);
	pw.writeShort(scroll->getSlot());
	if(scroll->getAmount() > 1)
		pw.writeShort(scroll->getAmount()-1);
	pw.write(3);
	if(!destroyed){
		pw.write(EQUIP);
		pw.writeShort(equip->getSlot());
		pw.write(0);
	}
	pw.write(EQUIP);
	pw.writeShort(equip->getSlot());
	if(!destroyed)
		itemInfo(equip, false);
	pw.write(1);

	return &pw;
}

PacketWriter* PacketCreator::showPet(int playerid, Pet* pet){
	pw.writeShort(USE_PET);
	// 7E 00 C4 E6 1F 00 00 01 01 67 4B 4C 00 09 00 50 6F 72 63 75 70 69 6E 65 3B FC 33 00 00 00 00 00 0F F3 5E 00 00 EE 00 00 00
	pw.writeInt(playerid);
	pw.write(pet->getPetSlot());
	pw.write(1); /// pet count?
	pw.write(1); // 

	pw.writeInt(pet->getItemID());
	pw.writeString(pet->getName());
	pw.writeInt(pet->getObjectID());
	pw.writeInt(0);
	pw.writeShort(pet->getPosition().x);
	pw.writeShort(pet->getPosition().y);
	pw.write(pet->getStance());
	pw.writeInt(pet->getFoothold()); 

	return &pw;
}
PacketWriter* PacketCreator::removePet(int playerid, int petSlot){
	pw.writeShort(USE_PET);

	pw.writeInt(playerid);
	pw.write(petSlot);
	pw.write(0);
	pw.write(0);

	return &pw;
}

PacketWriter* PacketCreator::petCommandReplay(int playerid, int petSlot, int id, bool success){
	pw.writeShort(PET_COMMAND_REPLAY);

	pw.writeInt(playerid);
	pw.writeShort(petSlot);
	pw.write(id);
	pw.writeShort(success);

	return &pw;

}

PacketWriter* PacketCreator::movePet(int playerid, int petSlot, ObjectMoving& moving, Position& pos){
	pw.writeShort(PET_MOVE);
	
	pw.writeInt(playerid);
	pw.write(petSlot);
	pw.writeShort(pos.x);
	pw.writeShort(pos.y);
	pw.write(moving.getPacket()->getBytes(), moving.getPacket()->getLength());

	return &pw;

}

PacketWriter* PacketCreator::showPetText(int playerid, int petSlot, string text, int act){
	pw.writeShort(PET_TEXT);
	//81 00 C1 57 43 00 00 00 0F 0F 00 49 20 66 65 65 6C 20 73 70 65 63 69 61 6C 21 00
	pw.writeInt(playerid);
	pw.writeShort(petSlot);
	pw.write(act);
	pw.writeString(text);
	pw.write(0);

	return &pw;

}
