/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef SHOPITEMDATA_H
#define SHOPITEMDATA_H

#include "DataStruct.h"

class ShopItemData : public DataStruct {
private:
	int price;
	int count;
	bool lock;
	int EndTime;
	short inv;
	short slot;
	int ShopIndex;
public:
	ShopItemData(){
		id=-1;
		price=1;
		count=1;
		ShopIndex=0;
	}

	void setPrice(int price){
		this->price = price;
	}
	void setCount(int amount){
		this->count = amount;
	}
	int getCount(){
		return count;
	}
	void setInvUse(short invUse){
		this->inv = invUse;
	}
	void setSlotUse(short SlotUse){
		this->slot = SlotUse;
	}
	short getInvUse(){
		return inv;
	}
	short getSlotUse(){
		return slot;
	}
	int getPrice(){
		return price;
	}

	void setLock(bool locked){
			this->lock = locked;
	}
	bool getLock(){
		return lock;
	}
	void setEndTime(int time){
		this->EndTime = time;
	}
	int getEndTime(){
		return EndTime;
	}

};

#endif