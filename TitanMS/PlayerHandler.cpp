/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "PgoLogin.h"
#include "DataProvider.h"
#include "PlayerHandler.h"
#include "PacketReader.h" 
#include "PacketCreator.h" 
#include "PacketWriter.h" 
#include "Player.h"
#include "PlayerBuffs.h"
#include "MapMobs.h"
#include "Map.h"
#include "Effect.h"
#include "PvP.h"
#include "MobsData.h"
#include "Mob.h"
#include "Maps.h"
#include "Item.h"
#include "Pet.h"
#include "Equip.h"
#include "ObjectMoving.h"
#include "Channel.h"
#include "MapPortalData.h"
#include "PlayerInventories.h"
#include "PacketHandlingError.h"
#include "Handler.h"
#include "Inventory.h"
#include "MapPlayers.h"
#include "AngelScriptEngine.h"
#include "Trade.h"
#include "Transportations.h"
#include <iostream>
#include "Tools.h"
#include "MapPVP.h"
using namespace Tools;

hash_map <short, PlayerHandler::handlerf> Handler<PlayerHandler>::handlers;

void PlayerHandler::loadPackets(){
	handlers[PING] = &PlayerHandler::pingHandle;
	handlers[CONNECT] = &PlayerHandler::connectionRequestHandle;
	handlers[TEMP1D] =&PlayerHandler::Temp;
	handlers[NPC_CHAT] = &PlayerHandler::NPCChatHandle;
	handlers[CHAR_GSM_ONOFF] = &PlayerHandler::playerToGSM;//DONE
	handlers[HANDLE_NPC] = &PlayerHandler::NPCHandle;
	handlers[USE_SHOP] = &PlayerHandler::useShopHandle;
	handlers[CHANGE_CHANNEL] = &PlayerHandler::changeChannelHandle;
	handlers[PLAYER_DIE] = &PlayerHandler::playerDieHandle; //PlayerDie
	handlers[TAKE_DAMAGE] = &PlayerHandler::playerTakeDamageHandle; //Player Get Damge
	handlers[PLAYER_MOVE] = &PlayerHandler::playerMovingHandle;
	//Warping
	handlers[WARP_REQ] = &PlayerHandler::cangeMapRewquest;//CAN I WARP
	handlers[CHANGE_MAP] = &PlayerHandler::changeMapHandle;//WarpAck
	//
	handlers[PLAYER_INFO] = &PlayerHandler::getPlayerInformationHandle;
	handlers[FACE_EXP] = &PlayerHandler::useFaceExpressionHandle;
	handlers[RECOVERY] = &PlayerHandler::recoveryHealthHandle;
	handlers[DAMAGE_MOB_MAGIC] = &PlayerHandler::damageMobMagicHandle;
	handlers[DAMAGE_MOB] = &PlayerHandler::damageMobHandle;
	handlers[DAMAGE_MOB_RANGED] = &PlayerHandler::damageMobRangedHandle;
	handlers[CONTROL_MOB] = &PlayerHandler::controlMobHandle;
	handlers[CONTROL_MOB_SKILL] = &PlayerHandler::controlMobSkillHandle;
	handlers[ADD_SKILL] = &PlayerHandler::addSkillHandle;
	handlers[STOP_SKILL] = &PlayerHandler::stopSkillHandle;
	handlers[ADD_SKILL_LEVEL] = &PlayerHandler::updtaeSkillLevel;
	handlers[USE_SKILL] = &PlayerHandler::useSkillHandle;
	handlers[USE_CHAT] = &PlayerHandler::useChatHandle;
	handlers[CHAT_COMMAND] = &PlayerHandler::chatCommandHandle;
	handlers[UNLOCK_ITEM] = &PlayerHandler::unlockItemHandle;//Need Insert Unlock Item (136)
	handlers[BUY_CASH_ITEM] = &PlayerHandler::BUY_CASH; //Buy
	handlers[BUY_NPC_ITEM] = &PlayerHandler::NPC_SELL; //Buy
	handlers[SELL_NPC_ITEM] = &PlayerHandler::SellItemHandle;//Sell
	handlers[MOVE_ITEM] = &PlayerHandler::moveItemHandle;//Move Item
	handlers[CHAR_INFO] = &PlayerHandler::playerCharInfo;//ClickOnPlayer Info
	handlers[CLICK_INFO] = &PlayerHandler::playerClickInfo;//ClickOnPlayer
	handlers[MOVE_ITEM] = &PlayerHandler::moveItemHandle;//Move Item
	handlers[USE_CHAIR] = &PlayerHandler::useChairHandle;
	handlers[CANCEL_CHAIR] = &PlayerHandler::cancelChairHandle;
	handlers[USE_ITEM_EFFECT] = &PlayerHandler::useItemEffectHandle;
	handlers[USE_CASH_ITEM] = &PlayerHandler::useCashItemHandle;
	handlers[USE_ITEM] = &PlayerHandler::useItemHandle;
	handlers[USE_SCROLL] = &PlayerHandler::useScrollHandle;
	handlers[USE_RETURN_SCROLL] = &PlayerHandler::useReturnScrollHandle;
	handlers[USE_SUMMON_BUG] = &PlayerHandler::useSummonBugHandle;
	handlers[CANCEL_ITEM_BUFF] = &PlayerHandler::cancelItemBuffHandle;
	handlers[AUTO_ARRANGEMENT] = &PlayerHandler::autoArrangementHandle;
	handlers[ADD_STAT] = &PlayerHandler::addStatHandle;
	handlers[DROP_MESOS] = &PlayerHandler::dropMesosHandle;
	//Pshop
	//handlers[PSHOP_BUY_REQ] = &PlayerHandler::PSHOP_BUYREQ;
	handlers[OPEN_SHOP_REQ] = &PlayerHandler::PSHOP_OPENSTARTREQ;
	handlers[PSHOP_INFO_REQ] = &PlayerHandler::PSHOP_INFOREQ;
	handlers[START_SHOP_REQ] = &PlayerHandler::PSHOP_OPENREQ;
	handlers[PSHOP_SELL_END] = &PlayerHandler::PSHOP_SELLEND;
	//pvp
	handlers[PVP_REQ] = &PlayerHandler::playerPvpReq;
	handlers[PVP_ACK] = &PlayerHandler::playerPvpAck;
	//
	handlers[LOOT_DROP] = &PlayerHandler::lootDropHandle; ///DONE
	handlers[DONE_QUEST] = &PlayerHandler::questDone;;
	handlers[UPDATE_QUEST] = &PlayerHandler::questUpdate;;
	handlers[GIVE_QUEST] = &PlayerHandler::questGive;
	handlers[DROP_QUEST] = &PlayerHandler::questDrop;
	handlers[QUEST_HANDLE] = &PlayerHandler::questHandle;
	handlers[CHANGE_KEY] = &PlayerHandler::changeKeyHandle;
	handlers[USE_PET] = &PlayerHandler::usePetHandle;
	handlers[MOVE_PET] = &PlayerHandler::movePetHandle;
	handlers[PET_COMMAND] = &PlayerHandler::petCommandHandle;
	handlers[PET_COMMAND_TEXT] = &PlayerHandler::petCommandTextHandle;
	handlers[HIT_REACTOR] = &PlayerHandler::hitReactorHandle;
	handlers[HIT_BY_MOB] = &PlayerHandler::hitByMobHandle;
	handlers[TRANSPORTATION] = &PlayerHandler::itemsTransportationHandle;
	handlers[CHANGE_MAP_SPECIAL] = &PlayerHandler::changeMapSpecialHandle;
}

void PlayerHandler::pingHandle(PacketReader& packet){
	printf("PlayerHandler::pingHandle");
	printf("\n");
}
void PlayerHandler::Temp(PacketReader& packet){
printf("PlayerHandler::Temp");
packet.readInt();
char Slot = packet.read();
string MSG = packet.readString(0x3c);
int var1 = packet.readInt();
char Inv = packet.read();
char Type = packet.read();
PacketWriter pw;

	pw.writeBytes("0501");//start
	pw.writeBytes("FB00");//Head
	pw.writeBytes("0E01");//Size
	pw.writeBytes("0E03");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.write(1);//chanel
	pw.writeString(player->getName(),20);
	pw.writeString(MSG,0x41);

	pw.writeShort(1);//Type (Sell/No Sell)
	
	//WEP
	if(player->getInventories()->getInventory(0)->getItemBySlot(0) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(0)->getID());
	else
		pw.writeInt(0);

	//Dress
	if(player->getInventories()->getInventory(0)->getItemBySlot(1) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(1)->getID());
	else
		pw.writeInt(0);

	//Dress 2
	if(player->getInventories()->getInventory(0)->getItemBySlot(11) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(11)->getID());
	else
		pw.writeInt(0);//??

	//Mantle
	if(player->getInventories()->getInventory(0)->getItemBySlot(4) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(4)->getID());
	else
		pw.writeInt(0);

	//Hair
	if(player->getInventories()->getInventory(0)->getItemBySlot(7) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(7)->getID());
	else
		pw.writeInt(0);

	//Eye
	if(player->getInventories()->getInventory(0)->getItemBySlot(8) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(8)->getID());
	else
		pw.writeInt(0);

	//Face
	if(player->getInventories()->getInventory(0)->getItemBySlot(9) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(9)->getID());
	else
		pw.writeInt(0);

	//Face 2
	if(player->getInventories()->getInventory(0)->getItemBySlot(12) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(12)->getID());
	else
		pw.writeInt(0);

	//Hat
	if(player->getInventories()->getInventory(0)->getItemBySlot(6) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(6)->getID());
	else
		pw.writeInt(0);

	//HairAcc
	if(player->getInventories()->getInventory(0)->getItemBySlot(14) != 0)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(14)->getID());
	else
		pw.writeInt(0);

	pw.writeInt(1);//player->getGender());
	pw.writeInt(0);//sellItem
	pw.writeBytes("01");
	pw.writeBytes("00");
	pw.writeBytes("06");
	pw.writeBytes("00");
	pw.writeBytes("00");
	pw.writeBytes("00");
	pw.writeBytes("00");
	pw.writeBytes("00");
	pw.writeBytes("00");
	pw.writeBytes("00");
	pw.writeBytes("00");

	pw.writeBytes("00");//NotRead
	pw.writeBytes("0000000000000000000000000000000000000000");//++

	pw.writeBytes("0000000000000000");//??
	pw.writeBytes("0000");//Frame
	pw.writeBytes("0000");
	pw.writeInt(-1);
	pw.writeInt(0);
	pw.writeBytes("01");
	pw.writeBytes("00");
	pw.writeBytes("0000");

	pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000000d0508f676120003000000f67612000300000000704000e803b7d9e7bb00001d97");
if(Type == -1){
	player->getChannel()->getPlayers()->send(&pw);
}
if(Type ==0x3){

}
/*
	printf("\n");

	player->send(PacketCreator().Player_WARP_ACK(player));
	//player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\WARP_ACK.txt","EnterPlayer"));
	player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\ITEM_ALLCREATE.txt","ITEM_ALLCREATE"));
	player->getMap()->addPlayer(player);
	//player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\MON_ALLCREATE 1.txt","MON_ALLCREATE 1"));
	player->send(PacketCreator().Player_HP_SP(player));
	//player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\CHAR_HPSP.txt","CHAR_HPSP"));
	player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\SPRIT2BODY_SENDUSE.txt","SPRIT2BODY_SENDUSE"));
	player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\SEANCE_SKILL_CHANGE.txt","SEANCE_SKILL_CHANGE"));
	player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\STAGE_CONDITION.txt","STAGE_CONDITION"));
	player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\SetAvatar.txt","SetAvatar"));
*/
}
void PlayerHandler::connectionRequestHandle(PacketReader& packet){
	printf("PlayerHandler::connectionRequestHandle");
	printf("\n");
	packet.readString(256);
	packet.readInt();
	unsigned char crid;
	crid = packet.read();
	char ip[20];
	IP PlayerLocal;
	PlayerLocal.p1 = packet.read();
	PlayerLocal.p2 = packet.read();
	PlayerLocal.p3 = packet.read();
	PlayerLocal.p4 = packet.read();
	sprintf_s(ip,20,"%d.%d.%d.%d",PlayerLocal.p1,PlayerLocal.p2,PlayerLocal.p3,PlayerLocal.p4);

	LogInfo tmpinfo;
	tmpinfo = PgoLogin::getInstance()->getPlayerLoginInfo(player->getIP());
	if(tmpinfo.find){
	player->setLocalIP(ip);
	player->SetPlayerLoginID(tmpinfo.UserId);
	player->setcharID(tmpinfo.UserPIds[crid]);
	player->setUserID(tmpinfo.UserId);
	}else
	{
		player->disconnect();
	}
	
	
	//player->send(PacketCreator().connectionData());
}
void PlayerHandler::playerToGSM(PacketReader& packet){

packet.readInt();
unsigned char State = packet.read();//0-disable 1  cange 2 on
if(State == 0x2)
{
	player->setGSM(true);
	player->getMap()->send(PacketCreator().Player_GSM_CONVERT(player,State));
	player->send(PacketCreator().Player_GSM_SKILL());
}
else if(State == 0x1)
{
	player->getMap()->send(PacketCreator().Player_GSM_CONVERT(player,State));
}else
{
	player->setGSM(false);
	player->getMap()->send(PacketCreator().Player_GSM_CONVERT(player,State));	
	player->send(PacketCreator().SKILL_ALL(player));
}	
	
	

}
void PlayerHandler::playerCharInfo(PacketReader& packet){
	packet.readInt();
	int PlayerId = packet.readInt();
	player->send(PacketCreator().Player_CLICK_CHAR_INFO(player));
	printf("playerCharInfo: %d",PlayerId);
}
void PlayerHandler::PSHOP_SELLEND(PacketReader &packet){
	packet.readInt();

	int close = packet.readInt();
	ShopData* shop = player->getNPCShop();
	delete shop;
	player->setNPCShop(NULL);
	player->getMap()->send(PacketCreator().PSHOP_SELLEND(player->getID()));
}
void PlayerHandler::PSHOP_OPENREQ(PacketReader &packet){
		packet.readInt();

		player->send(PacketCreator().PSHOP_OPENACK(6));
}
void PlayerHandler::playerPvpAck(PacketReader& packet){
	packet.readInt();

	int AskerP = packet.readInt();//TheCrearor
	int replay = packet.readInt();
	
	if(player->getMap()->getPvP(AskerP) != NULL)
	{		
			if(replay == 1){
				player->getMap()->getPvP(AskerP)->startPvP();
			}else
			{
				player->getMap()->getPvP(AskerP)->getAsker()->send(PacketCreator().MSG_FROM_SERVER("Player Deny your Request"));
				player->getMap()->getPvPs()->removePvP(player->getMap()->getPvP(AskerP));
			}
	}
}
void PlayerHandler::playerPvpReq(PacketReader& packet){
	packet.readInt();

	int PlayerID = packet.readInt();//The Player To Be Invite

	Player* toPlayer = player->getMap()->getPlayer(PlayerID);

	if(toPlayer == NULL)
		return;

	//Look if we in Pk allrady
	if(player->getPvP() > 0){
		player->send(PacketCreator().MSG_FROM_SERVER("LOL you Allrady In PK..."));
		return;
	}
	if(toPlayer->getPvP() > 0)
	{
		toPlayer->send(PacketCreator().MSG_FROM_SERVER("The Player Allrady in PK"));
		return;
	}
	
	PvP* pvp = new  PvP(player->getMap(),player,toPlayer,player->getID());
	player->getMap()->getPvPs()->addPvP(pvp);
	toPlayer->send(PacketCreator().Player_PVP_REQ(player->getID()));

}
void PlayerHandler::playerClickInfo(PacketReader& packet){
	packet.readInt();
	int PlayerId = packet.readInt();

	player->send(PacketCreator().Player_CLICK_INFO(PlayerId));
	printf("playerClickInfo: %d",PlayerId);
}
void PlayerHandler::playerDieHandle(PacketReader& packet){
	packet.readInt();

if(player->getPvP() > 0)
	{
		player->getMap()->getPvPs()->getPvPByID(player->getPvP())->endPvP(true);
		/*
		int p1 = player->getMap()->getPvP()->getAsker();
		int p2 = player->getMap()->getPvP()->getToplayer();
		Player* pvp1;

	if(player->getID() == p1) // i am the asker
		pvp1 = player->getMap()->getPlayer(p2);
	if(player->getID() == p2)
		pvp1 = player->getMap()->getPlayer(p1);

		player->send(PacketCreator().Player_PVP_END(pvp1->getID()));
		pvp1->send(PacketCreator().Player_PVP_END(pvp1->getID()));
*/
	}
player->setDie(1);
player->send(PacketCreator().Player_DEAD_ACK(player));

}
void PlayerHandler::playerTakeDamageHandle(PacketReader& packet){
	printf("PlayerHandler::playerTakeDamageHandle--> BEEN HIT");
	printf("\n");
	packet.readInt();

	int damage = packet.readInt();
	short var1 = packet.readShort();
	short MobNum = packet.readShort();
	int AttackerID = packet.readInt();
	int SkillUse = packet.readInt();

	
	if(damage < 0)
		return;
	if(MobNum != -1){//Player Been Hit By Mob
		player->addHP(-damage);
	
	}
	if(AttackerID != -1){//Player Been Hit By Player
		Player* attak = player->getMap()->getPlayer(AttackerID);
			player->getMap()->getPvPs()->getPvPByID(player->getPvP())->hitPlayer(attak, player, damage);
				
	}
/*
	if(player->getPvP() && player->getMap()->getPvP() != NULL)
		player->getMap()->getPvP()->hitPlayer(NULL, player, damage);
	if(damagetype != -2){ // Damage from monster...
		mobid = packet.readInt();
		if(MobsData::getInstance()->getDataByID(mobid) == NULL) return;
		objid = packet.readInt();
	}	

	if(damage > 0){ 
		if(damagetype == -1 && player->getBuffs()->isBuffActive(Effect::POWER_GUARD)){ // only regular body attack from mobs..
			Mob* mob = player->getMap()->getMob(objid);
			if(mob != NULL && !mob->isBoss()){ // Not on bosses
				int mobdamage = (int) (damage* (player->getBuffs()->getBuffValue(Effect::POWER_GUARD)) / 100.0);
				if(mob->getMaxHP()/10 < mobdamage)
					mobdamage = mob->getMaxHP()/10;
				player->getMap()->getMobs()->damage(player, mob, mobdamage);
				damage -= mobdamage;
				player->send(PacketCreator().damageMob(objid, mobdamage));
				
			}
		}
		if(player->getBuffs()->isBuffActive(Effect::MAGIC_GUARD)){
			int mpdamage = (int)((damage * player->getBuffs()->getBuffValue(Effect::MAGIC_GUARD)) / 100.0);
			int hpdamage = damage - mpdamage;
			if (mpdamage >= player->getMP()) {
	            player->addHP(-(hpdamage + (mpdamage - player->getMP())));
				player->setMP(0);
			}
			else {
	            player->addMP(-mpdamage);
				player->addHP(-hpdamage);
			}
		}	
		else if(player->getBuffs()->isBuffActive(Effect::MESO_GUARD)){
			damage = damage / 2 + (damage % 2);
			int mesos = (int)((damage * player->getBuffs()->getBuffValue(Effect::MESO_GUARD)) / 100.0);
			if(player->getMesos() < mesos){
				player->addMesos(-player->getMesos());
				player->getBuffs()->cancelBuff(4211005);
			}
			else{
				player->addMesos(-mesos);
			}
			player->addHP(-damage);
		}
		else{
			player->addHP(-damage);
		}
	}
	player->getMap()->send(PacketCreator().damagePlayer(player->getID(), damagetype, damage, mobid), player);
*/
}
void PlayerHandler::playerMovingHandle(PacketReader& packet){
	printf("PlayerHandler::playerMovingHandle(");
	printf("\n");
	Position pos = player->getPosition();
	packet.read(5);
	ObjectMoving moving = ObjectMoving(packet, player);
	player->getMap()->send(PacketCreator().showMoving(player->getID(), moving), player);
//if(player->getPvP() && player->getMap()->getPvP() != NULL)
//		player->getMap()->getPvP()->movePlayer(player, pos, moving);
}
void PlayerHandler::cangeMapRewquest(PacketReader& packet){
	packet.readInt(); //Counter
	short RegionTO = packet.readShort();
	short MapTo = packet.readShort();
	short PlayerX = packet.readShort();
	short PlayerY = packet.readShort();
	printf("Cange Map request\n");
	printf("Move Region: %d\n",RegionTO);
	printf("Move Map: %d\n",MapTo);
	printf("CureentX: %d\n",PlayerX);
	printf("CureentY: %d\n\n",PlayerY);
	int MapIDmp = ToMAPID (RegionTO,MapTo);
	Map* map = player->getChannel()->getMaps()->getMap(MapIDmp);
	if(player->getDie() > 0)
	{
		player->setHP(player->getMaxHP(),false);
		player->setMP(player->getMaxMP(),false);
	}
	if(map != NULL)
		player->send(PacketCreator().Player_CAN_WARP_ACK(1,RegionTO,MapTo,PlayerX,PlayerY));
	else
		printf("No Sach Map",PlayerY);

}
void PlayerHandler::changeMapHandle(PacketReader& packet){
	printf("PlayerHandler::changeMapHandle");
	packet.readInt();//Counter

	int PlayerID= packet.readInt();
	int RegionID= packet.readShort();
	int MapID = packet.readShort();
	int PX = packet.readShort();
	int PY = packet.readShort();
	int MapIDmp = ToMAPID (RegionID,MapID);
	
	if(MapIDmp == 17701){
		player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i11c.txt","11C",false,false));
					player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i11d.txt","11d",false,false));
					player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i11e.txt","11e",false,false));
					player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i11f.txt","11f",false,false));
					player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i120.txt","120",false,false));
					player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i122.txt","122",false,false));
					player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i123.txt","123",false,false));
					player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i124.txt","124",false,false));
					player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i125.txt","125",false,false));
					player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i126.txt","126",false,false));
					player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i127.txt","127",false,false));
					player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i128.txt","128",false,false));
					player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i129.txt","129",false,false));
					player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\i12a.txt","12A",false,false));

	}
	
	if(MapIDmp == player->getMap()->getID() && MapIDmp != 17701 && player->getDie() == 0)
	{
		//player->send(PacketCreator().Player_WARP_ACK(player));
		//player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\WARP_ACK.txt","EnterPlayer"));
		player->getMap()->addPlayer(player);
		//player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\MON_ALLCREATE 1.txt","MON_ALLCREATE 1"));
	}else
	{
			player->setDie(0);
			player->setPlayerX(PX);
			player->setPlayerY(PY);
			Map* map = player->getChannel()->getMaps()->getMap(MapIDmp);
			player->setRegionId(map->GetRegionID());
			player->setMapId(map->GetMapID());
			player->changeMap(map);//Oso AddPlayer
	}

		player->send(PacketCreator().Player_HP_SP(player));
		//player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\CHAR_HPSP.txt","CHAR_HPSP"));
		player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\SPRIT2BODY_SENDUSE.txt","SPRIT2BODY_SENDUSE"));
		player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\SEANCE_SKILL_CHANGE.txt","SEANCE_SKILL_CHANGE"));
		player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\STAGE_CONDITION.txt","STAGE_CONDITION"));
		player->getMap()->send(PacketCreator().Player_SET_AVATAR(player));
		
	if(MapIDmp == 17701){
		player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\f3.txt","f3"));
		player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\f2.txt","f2"));
		player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\24b.txt","24b"));
		//player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\11f.txt","11f"));
		//player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\120.txt","120"));
		//player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\123.txt","123"));
		//player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\Game Server\\124.txt","124"));
	}
	
	
	/*
	int type = packet.readInt();
	if(type == 0){
		if(player->getHP() > 0) return;
		//TODO: player->getBuffs()->cancelAll() (crash);
		player->setHP(50, false);
		Map* map = player->getChannel()->getMaps()->getMap(player->getMap()->getReturnMap());
		player->changeMap(map);
	}
	else{
		string toname = packet.readString();
		MapPortalData* portal = player->getMap()->getPortal(toname);
		if(portal != NULL && portal->getScript() != ""){
			AngelScriptEngine::handlePortal(player, portal);
			player->send(PacketCreator().enableAction());
		}
		else if(portal != NULL && portal->getToMapID() != 999999999){
			Map* map = player->getChannel()->getMaps()->getMap(portal->getToMapID());
			MapPortalData* tportal = map->getPortal(portal->getToPortal());
			player->changeMap(map, (tportal != NULL) ? tportal->getID() : 0);
		}
		else{
			player->send(PacketCreator().enableAction());
		}
	}
*/
}
void PlayerHandler::getPlayerInformationHandle(PacketReader& packet){
	printf("PlayerHandler::getPlayerInformationHandle");
	printf("\n");
	packet.readInt();
	int playerid = packet.readInt();
	Player* to = player->getMap()->getPlayers()->getPlayerByID(playerid);
	if(to == NULL)
		to = player;
	player->send(PacketCreator().showInfo(to));
}
void PlayerHandler::useFaceExpressionHandle(PacketReader& packet){
	printf("PlayerHandler::useFaceExpressionHandle");
	printf("\n");
	int face = packet.readInt();
	if(face >= 8){
		if(player->getInventories()->getInventory(CASH)->getItemByID(5160000 + face - 8) == NULL)
			return;
	}
	player->getMap()->send(PacketCreator().faceExpression(player->getID(), face), player);
}
void PlayerHandler::recoveryHealthHandle(PacketReader& packet){
	printf("PlayerHandler::recoveryHealthHandle(");
	printf("\n");
	packet.read(4);

	short hp = packet.readShort();
	short mp = packet.readShort();
	if(hp > 150 || mp > 300){ // too much >_>, checks for passive skills and chairs would be nice
		return;
	}
	player->addHP(hp);
	player->addMP(mp);
}
void PlayerHandler::changeChannelHandle(PacketReader& packet){
	printf("PlayerHandler::changeChannelHandle");
	printf("\n");
		// TODO
}
void PlayerHandler::itemsTransportationHandle(PacketReader& packet){
	printf("PlayerHandler::itemsTransportationHandle");
	printf("\n");
	char type = packet.read();
	if(type == Transportations::OPEN){
		char create = packet.read();
		if(create == Transportations::TYPE_TRADE){ 
			if(player->getTrade() == NULL){
				Trade* trade = new Trade(player);
				trade->open();
			}
			else{
				player->send(PacketCreator().showMassage("You are already in a trade", 5));
			}
		}
		else if(create == Transportations::TYPE_SHOP){ 

		}
	}
	else if(type == Transportations::INVITE){ 
		Trade* trade = player->getTrade();
		if(trade != NULL && trade->getInviteID() == 0){
			int invitedid = packet.readInt();
			Player* invited = player->getMap()->getPlayer(invitedid);
			if(!trade->invite(invited)){
				trade->close();
				player->setTrade(NULL);
				delete trade;
			}
		}
	}
	else if(type == Transportations::DECLINE){ 
		int tradeid = packet.readInt();
		Trade* trade = player->getChannel()->getTransportations()->getTradeByInvite(tradeid);
		if(trade != NULL && trade->getInvited() == player){
			trade->decline();
			player->getChannel()->getTransportations()->closeTrade(trade);
		}
	}
	else if(type == Transportations::JOIN){ 
		int id = packet.readInt();
		Trade* trade = player->getChannel()->getTransportations()->getTradeByInvite(id);
		if(trade != NULL){ // trade
			if(trade->getInvited() == player){
				if(trade->getPlayer()->getMap() != player->getMap())
					player->send(PacketCreator().tradeError(9));
				else
					trade->accept();
			}
			
		}
		else if(false){ // shop
		}
		else{
			player->send(PacketCreator().showMassage("The other player has already closed the trade", 5));
		}
	}
	else if(type == Transportations::CHAT){ 
		if(player->getTrade() != NULL){
			player->getTrade()->chat(packet.readString());
		}
	}
	else if(type == Transportations::CLOSE){ 
		Trade* trade = player->getTrade();
		if(trade != NULL){
			player->getChannel()->getTransportations()->closeTrade(trade);
		}
	}
	else if(type == Transportations::OPEN_SHOP){
	}
	else if(type == Transportations::TRADE_ADD_ITEM){
		Trade* trade = player->getTrade();
		if(trade != NULL){
			if(trade->isConfirmed()) return;
			char inv = packet.read();
			short islot = packet.readShort();
			Item* item = player->getInventories()->getItemBySlot(inv, islot);
			if(item == NULL)
				return;
			short amount = packet.readShort();
			char slot = packet.read();
			if(IS_STAR(item->getID()))
				amount = item->getAmount();
			if((amount >= 0 && amount <= item->getAmount())){
				Item* tItem = NULL;
				if(IS_EQUIP(item->getID())){
					tItem = new Equip(*(Equip*)item);				
				}
				else if(IS_PET(item->getID())){
					tItem = new Pet(*(Pet*)item);
				}
				else{
					tItem = new Item(*item);
					tItem->setAmount(amount);
				}
				player->getInventories()->removeItemBySlot(inv, islot, amount);
				trade->addItem(tItem, slot);
			}
		}
	}
	else if(type == Transportations::ADD_MESOS){
		int mesos = packet.readInt();
		if(mesos <= 0 || mesos > player->getMesos())
			return;
		Trade* trade = player->getTrade();
		if(trade != NULL && !trade->isConfirmed()){
			trade->addMesos(mesos);
		}
	}
	else if(type == Transportations::CONFIRM){
		Trade* trade = player->getTrade();
		if(trade != NULL && !trade->isConfirmed() && trade->getTrader() != NULL){
			trade->confirm();
			if(trade->getTrader()->isConfirmed()){
				trade->complete();
				player->setTrade(NULL);
				trade->getTrader()->getPlayer()->setTrade(NULL);
				delete trade->getTrader();
				delete trade;
			}
		}
	}
	else if(type == Transportations::SHOP_ADD_ITEM){
	}
	else if(type == Transportations::BUY){
	}
	else if(type == Transportations::REMOVE_ITEM){ 
	}
}
void PlayerHandler::changeMapSpecialHandle(PacketReader& packet){
	printf("PlayerHandler::changeMapSpecialHandle(");
	printf("\n");
	packet.read();
	
	string toname = packet.readString();
	MapPortalData* portal = player->getMap()->getPortal(toname);
	if(portal == NULL)
		return;
	AngelScriptEngine::handlePortal(player, portal);
	player->send(PacketCreator().enableAction());
}