/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef PET_H
#define PET_H

#include "Item.h"
#include "LifeMapObject.h"

class Player;
class Timer;
class PetCommandData;
class Pet : public Item, public LifeMapObject {
private:
	static int ids;
	static int levels[30];
	__int64 time;
	string name;
	char level;
	short closeness;
	char fullness;
	int lastTime;
	int petSlot;
	Timer* timer;
	int HP;
	int MaxHP;
	int MP;
	int MaxMP;
	int PetDeco;
	int Exp;
	int MaxExp;
public:
	Pet(int id);
	Pet(const Pet &pet): LifeMapObject(), Item(pet), time(pet.time), name(pet.name), level(pet.level), closeness(pet.closeness), fullness(pet.fullness), lastTime(0), petSlot(-1), timer(NULL) { Item::Item(pet); }
	~Pet();
	void setTime(__int64 time){
		this->time = time;
	}
	__int64 getTime(){
		return time;
	}
	void setName(string name){
		this->name = name;
	}
	void setMaxExp(int exp){
		this->MaxExp = exp;
	}
	int getMaxExp(){
		return MaxExp;
	}
	void setExp(int exp){
		this->Exp = exp;
	}
	int getExp(){
		return Exp;
	}
	void setPetDeco(int deco){
		this->PetDeco = deco;
	}
	int getPetDeco(){
		return PetDeco;
	}
	void setMaxMP(int mp){
		this->MP = mp;
	}
	int getMaxMP(){
		return MaxMP;
	}
	void setMP(int mp){
		this->MP = mp;
	}
	int getMP(){
		return MP;
	}
	void setMaxHP(int hp){
		this->HP = hp;
	}
	int getMaxHP(){
		return MaxHP;
	}
	void setHP(int hp){
		this->HP = hp;
	}
	int getHP(){
		return HP;
	}
	string getName(){
		return name;
	}
	void setLevel(char level){
		if(level > 30)
			level = 30;
		if(level < 1)
			level = 1;
		this->level = level;
	}
	char getLevel(){
		return level;
	}
	void setCloseness(short closeness, Player* player = NULL);
	short getCloseness(){
		return closeness;
	}
	void setFullness(char fullness){
		this->fullness = fullness;
	}
	char getFullness(){
		return fullness;
	}
	void addLevel(char level){
		setLevel(this->level + level);
	}
	void addFullness(char fullness){
		setFullness(this->fullness + fullness);
	}
	void addCloseness(char closeness, Player* player = NULL){
		setCloseness(this->closeness + closeness, player);
	}
	int getObjectID(){
		return ((MapObject*)this)->getID();
	}
	int getItemID(){
		return ((Item*)this)->getID();
	}
	void setLastTime(int t){
		lastTime = t;
	}
	int getLastTime(){
		return lastTime;
	}
	Timer** getTimer(){
		return &timer;
	}
	void setPetSlot(int slot){
		petSlot = slot;
	}
	int getPetSlot(){
		return petSlot;
	}
	void startTimer(Player* Player);
	void stopTimer();
};

#endif