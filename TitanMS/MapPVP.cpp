#include "MapPVP.h"


void MapPVP::addPvP(PvP* pvp){
	pvps[pvp->getID()] = pvp;	
}

void MapPVP::removePvP(PvP *pvp){
	pvps.erase(pvp->getID());
	delete pvp;
}
PvP* MapPVP::getPvPByID(int id){
	if(pvps.find(id) != pvps.end())
		return pvps[id];
	return NULL;
}