/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef QUEST_H
#define QUEST_H

#include <hash_map>
#include <string>
using namespace std;
using namespace stdext;

struct KilledMob {
	int id;
	int count;
};

class Quest {
private: 
	int id;
	bool complete;
	char State;
	__int64 time;
	vector <KilledMob> mobs;
public:
	Quest(int id, bool complete = false, __int64 time = 0){
		this->id = id;
		this->complete = complete;
		this->time = time;
	}
	void addKilledMobsInfo(vector <int>& ids){
		for(int i=0; i<(int)ids.size(); i++){
			KilledMob mob;
			mob.id = ids[i];
			mob.count =0;
			mobs.push_back(mob);
		}
	}
	void addKilledMob(int id, int count=1){
		for(int i=0; i<(int)mobs.size(); i++){
			if(mobs[i].id == id){
				mobs[i].count += count;
				break;
			}
		}
	}
	string getKilledMobs(){
		string ret;
		for(int i=0; i<(int)mobs.size(); i++){
			char temp[4];
			temp[0] = mobs[i].count/100+'0';
			temp[1] = mobs[i].count/10%10+'0';
			temp[2] = mobs[i].count%10+'0';
			temp[3] = '\0';
			ret += string(temp);
		}
		return ret;
	}
	void addKmob(int id){
		int find = -1;
		for(int i=0; i<(int)mobs.size(); i++){//Try Look Same Mob
			if(mobs[i].id == id){
				find = i;
				break;
			}
		}
		if(find != -1){
			mobs[find].count += 1;
		}else
		{
			KilledMob mob;
			mob.id = id;
			mob.count =1;
			mobs.push_back(mob);
		}

	}
	int GetKillMobs(int id){
		for(int i=0; i<(int)mobs.size(); i++){
			if(mobs[i].id == id)
				return mobs[i].count;
		}
		return 0;
	}
	void setID(int id){
		this->id = id;
	}
	int getID(){
		return id;
	}
	char getState(){
		return State;
	}
	void setState(char state){
		this->State = state;
	}
	void setIsCompleted(bool complete){
		this->complete = complete;
	}
	bool isCompleted(){
		return complete;
	}
	void setCompleteTime(__int64 time){
		this->time = time;
	}
	__int64 getCompleteTime(){
		return time;
	}	
};

#endif