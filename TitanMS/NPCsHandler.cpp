/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "DataProvider.h"
#include "PlayerHandler.h"
#include "PacketReader.h"
#include "PacketCreator.h"
#include "Player.h"
#include "Map.h"
#include "NPC.h"
#include "PlayerNPC.h"
#include "Item.h"
#include "DataProvider.h"
#include "Inventory.h"
#include "ShopData.h"
#include "ShopItemData.h"
#include "AngelScriptEngine.h"
#include "PlayerInventories.h"

void PlayerHandler::PSHOP_BUYREQ(PacketReader& packet){
	packet.readInt();

	int shopID = packet.readInt();
	int ItemID = packet.readInt();
	short pos = packet.readShort();
	short count = packet.readShort();
	
	if(player->getMap()->getPlayer(shopID)->getNPCShop() != NULL){
			ShopData* shop = player->getMap()->getPlayer(shopID)->getNPCShop();
			ShopItemData* item = shop->getDataByID(ItemID);
			
			if(item == NULL)
				return;
			if(item->getCount() < count)
				return;
			if(player->getMesos() < item->getPrice()*count)
				return;

			player->send(PacketCreator().PSHOP_BUYACK(1));//Ok For Buy

			Player* Seller = player->getMap()->getPlayer(shopID);
			Item* selcted = Seller->getInventories()->getInventory(item->getInvUse())->getItemBySlot(item->getSlotUse());
			if(count < item->getCount()){//Buy only Few
				Seller->getInventories()->getInventory(item->getInvUse())->removeItemBySlot(item->getSlotUse(),count);
				
				item->setCount(item->getCount() - count); //SetShopCount
			
			}else	//Buy All
			{

			}
			
			//player->send(PacketCreator().showNPCShop(shop));
		}

}
void PlayerHandler::PSHOP_INFOREQ(PacketReader &packet){
		packet.readInt();

		int pshopid = packet.readInt();

		if(player->getMap()->getPlayer(pshopid)->getNPCShop() != NULL){
			ShopData* shop = player->getMap()->getPlayer(pshopid)->getNPCShop();
			//vector <ShopItemData*>* items = shop->getData();
			player->send(PacketCreator().showNPCShop(shop));
		}
}
void PlayerHandler::PSHOP_OPENSTARTREQ(PacketReader &packet){
	packet.readInt();

	string ShopName = packet.readString(0x16);
	string var = packet.readString(0x12);

	ShopData* shop = new ShopData(player->getID());
	shop->setShopTitle(ShopName);
	for(int i =0 ;i < 12;i++){
		short inv = packet.readShort();
		short slot = packet.readShort();
		int count = packet.readInt();
		int price = packet.readInt();
		if(inv != -1 && slot != -1 && count != 0 && price != 0){
			int itemid = player->getInventories()->getInventory(inv)->getItemBySlot(slot)->getID();
			bool locked = player->getInventories()->getInventory(inv)->getItemBySlot(slot)->getLocked();
			int EndD = player->getInventories()->getInventory(inv)->getItemBySlot(slot)->getEndDate();
			ShopItemData* item = new ShopItemData();
				item->setID(itemid);
				item->setCount(count);
				item->setPrice(price);
				item->setInvUse(inv);
				item->setSlotUse(slot);
				item->setEndTime(EndD);
				item->setLock(locked);
				shop->add(item);
		}else
		{
			ShopItemData* item = new ShopItemData();
			item->setID(-1);
			item->setInvUse(-1);
			item->setCount(-1);
			shop->add(item);

		}
	}
	player->setNPCShop(shop);
	player->getMap()->send(PacketCreator().PSHOP_SELLSTARTACK(player->getID(),ShopName));
}

void PlayerHandler::NPCHandle(PacketReader& packet){
	if(player->getNPC() != NULL || player->getNPCShop() != NULL)
		return;
	int npcid = player->getMap()->getNPC(packet.readInt())->getNPCID();
	packet.readShort(); // x
	packet.readShort(); // y
	ShopData* shop = DataProvider::getInstance()->getShopData(npcid);
	
	if(shop != NULL){
		player->setNPCShop(shop);
		player->send(PacketCreator().showNPCShop(shop));
		return;
	}
	PlayerNPC* npc = new PlayerNPC(npcid, player);

	if(!AngelScriptEngine::handleNPC(npc)) npc->end();

	if(npc->isEnd())
		delete npc;
}
void PlayerHandler::NPCChatHandle(PacketReader& packet){

	PlayerNPC* npc = player->getNPC();
	if(npc == NULL)
		return;
	char type = packet.read();
	if(type == PlayerNPC::BACK_NEXT){
		char what = packet.read();
		if(what == 0)
			npc->setState(npc->getState()-1);
		else if(what == 1)
			npc->setState(npc->getState()+1);
		else if(what == -1)
			npc->end();
	}
	else if(type == PlayerNPC::YES_NO || type == PlayerNPC::ACCEPT_DECLINE){
		npc->setState(npc->getState()+1);
		char what = packet.read();
		if(what == 0)
			npc->setSelected(0);
		else if(what == 1)
			npc->setSelected(1);
		else if(what == -1)
			npc->end();
	}
	else if(type == PlayerNPC::GET_TEXT){
		npc->setState(npc->getState()+1);
		if(packet.read() != 0){
			npc->setGetText(packet.readString());
		}
		else
			npc->end();
	}
	else if(type == PlayerNPC::GET_NUMBER){
		npc->setState(npc->getState()+1);
		if(packet.read() == 1)
			npc->setGetNumber(packet.readInt());
		else
			npc->end();
	}
	else if(type == PlayerNPC::SIMPLE){
		npc->setState(npc->getState()+1);
		if(packet.read() == 0)
			npc->end();
		else
			npc->setSelected(packet.read());
	}
	else if(type == PlayerNPC::STYLE){
		npc->setState(npc->getState()+1);
		if(packet.read() == 1)
			npc->setSelected(packet.readShort());
		else 
			npc->end();
	}
	else
		npc->end();
	if(npc->isEnd()){
		delete npc;
		return;
	}
	if(!AngelScriptEngine::handleNPC(npc)) npc->end();
	if(npc->isEnd()){
		delete npc;
		return;
	}
}
void PlayerHandler::BUY_CASH(PacketReader& packet){
packet.readInt();//Counter

	int itemid = packet.readInt();
	int Amount = 1;


	int inv = player->getInventories()->getINVbyITEM(itemid);
	if (inv == -1)
		return ;
	if(IS_HAIR(itemid) || IS_EYE(itemid))
		player->getInventories()->giveItem(itemid, Amount,true);
	else
		player->getInventories()->giveItem(itemid, Amount,false);
player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(inv),inv));
}
void PlayerHandler::NPC_SELL(PacketReader& packet){
	packet.readInt();//Counter
	int NpcID = packet.readInt();
	int ItemID = packet.readInt();
	int Amount = packet.readInt();

	ShopData* shop = DataProvider::getInstance()->getShopData(NpcID);
	if(shop == NULL)
		return;
	ShopItemData* item = shop->getDataByID(ItemID);
	if(item == NULL)
		return;

	int price = item->getPrice();
	
	if(player->getMesos() < price*Amount)
			return;
	if(player->getInventories()->giveItem(ItemID, Amount))
	{
			player->addMesos(-Amount*price);
			int inv = player->getInventories()->getINVbyITEM(ItemID);
			player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(inv),inv));
	}
}
void PlayerHandler::useShopHandle(PacketReader& packet){
	ShopData* shop = player->getNPCShop();
	if(shop == NULL)
		return;
	char action = packet.read();
	if(action == 0){ // Buy
		packet.readShort();
		int itemid = packet.readInt();
		short amount = packet.readShort();
		ShopItemData* item = shop->getDataByID(itemid);
		if(item == NULL)
			return;
		int price = item->getPrice();
		if(player->getMesos() < price*amount)
			return;
		if(amount <= 0 || (INVENTORY(itemid) == EQUIP && amount > 1))
			return;
		if(player->getInventories()->giveItem(itemid, amount))
			player->addMesos(-amount*price);
		player->send(PacketCreator().bought());
	}
	else if(action == 1){ // Sell
		short slot = packet.readShort();
		int itemid = packet.readInt();
		short amount = packet.readShort();
		if(amount < 1) amount = 1;
		Item* item = player->getInventories()->getItemBySlot(INVENTORY(itemid), slot);
		if(item == NULL)
			return;
		if(item->getID() != itemid || item->getAmount() < amount)
			return;
		player->getInventories()->getInventory(INVENTORY(itemid))->removeItem(item, false, true);
		player->addMesos(amount*DataProvider::getInstance()->getItemPrice(itemid));
	}
	else if(action == 2){ // recharge
		// TODO
	}
	else if(action == 3){ // Close
		player->setNPCShop(NULL);
	}
}