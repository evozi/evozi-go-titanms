/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef PVP_H
#define PVP_H

class Map;
class Player;
class ObjectMoving;
class Timer;
struct Position;

class PvP {
private:
	Map* map;
	int ID;
	Player* Creator;
	Player* Toinv;
	int Winner;
	Timer* pvptimer;
public:
	PvP(Map* map,Player* Asker,Player* Toplayer,int id);
	void startPvP();
	void endPvP(bool die = false);
	int getID(){
		return ID;
	}
	int getWinner(){
		return Winner;
	}
	Timer* getTimer(){
		return this->pvptimer;
	}
	Player* getAsker(){
		return Creator;
	}
	Player* getToplayer(){
		return Toinv;
	}
	void hitPlayer(Player* hitter, Player* player, int damage);
	void movePlayer(Player* player, Position& pos, ObjectMoving& moving);
	void showPvP(Player* player);
};

#endif