/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "PgoLogin.h"
#include "DataProvider.h"
#include "PlayerLoginHandler.h"
#include "PlayerLogin.h"
#include "PacketReader.h"
#include "PacketCreator.h"
#include "MySQLM.h"
#include "Character.h"
#include "Worlds.h"
#include "World.h"
#include "Channels.h"
#include "Channel.h"
#include "Tools.h"

using namespace Tools;

hash_map <short, PlayerLoginHandler::handlerf> Handler<PlayerLoginHandler>::handlers;

void PlayerLoginHandler::loadPackets(){
	handlers[PING] = &PlayerLoginHandler::pingHandle;
	handlers[LOGIN_REQUEST] = &PlayerLoginHandler::loginRequestHandle;
	handlers[SHOW_WORLD] = &PlayerLoginHandler::showWorldRequestHandle;////N_Server List (S-0x33)
	handlers[WORLD_SELECT] = &PlayerLoginHandler::worldSelectHandle;//Off line
	handlers[CHANNEL_SELECT] = &PlayerLoginHandler::channelSelectHandle;//Chanel Select (S-0x00) (R-0x34)
	handlers[SHOW_CHARACTERS] = &PlayerLoginHandler::showCharactersHandle;
	handlers[CHARACTER_SELECT] = &PlayerLoginHandler::characterSelectHandle;//sfow chrs (chr server)
	handlers[HANDLE_LOGIN] = &PlayerLoginHandler::loginHandle;
	handlers[WORLD_BACK] = &PlayerLoginHandler::showWorldRequestHandle;
	handlers[CHARACTER_SELECT] = &PlayerLoginHandler::characterSelectHandle;
	handlers[NAME_CHECK] = &PlayerLoginHandler::nameSelectHandle;
	handlers[CREATE_CHARACTER] = &PlayerLoginHandler::createCharacterHandle;
	handlers[DELETE_CHARACTER] = &PlayerLoginHandler::deleteCharacterHandle;
	handlers[LOGIN_BACK] = &PlayerLoginHandler::loginBackHandle;
}

void PlayerLoginHandler::pingHandle(PacketReader& packet){
	printf("PlayerLoginHandler::pingHandle\n");
}
void PlayerLoginHandler::showCharactersHandle(PacketReader& packet){
	printf("PlayerLoginHandler::showCharactersHandle\n");
string temp,User,Pass;
packet.readInt();
temp = packet.readStringNoSise2();
packet.readString(1);
temp = packet.readStringNoSise2();
packet.readString(1);
User = packet.readStringNoSise2();
packet.readString(1);
temp = packet.readStringNoSise2();
packet.readString(1);
Pass = packet.readStringNoSise2();
int s = player->checkLogin(User, Pass);
vector <Character* > toshow;
	


if(s == 1){
		player->setUserid(MySQL::getInstance()->getUserID((char*)User.c_str()));
		player->setIDs(MySQL::getInstance()->getCharactersIDs(player->getUserid()));
		player->setGender(MySQL::getInstance()->getInt("users", player->getUserid(), "gender"));
		player->loadCharacters();
		player->setServer(0);
	for(int i=0; i<(int)player->getCharacters()->size(); i++)
	if((*player->getCharacters())[i]->getWorld() == player->getServer())
			toshow.push_back((*player->getCharacters())[i]);

			player->send(PacketCreator().showCharacters(toshow));
		
}
//packet.show();
}
void PlayerLoginHandler::loginRequestHandle(PacketReader& packet){
	printf("PlayerLoginHandler::loginRequestHandle(\n");
	string username, password;
	username = packet.readString();
	password = packet.readString();
	int s = player->checkLogin(username, password);
	if(s == 1){
		player->setUserid(MySQL::getInstance()->getUserID((char*)username.c_str()));
		player->setIDs(MySQL::getInstance()->getCharactersIDs(player->getUserid()));
		player->setGender(MySQL::getInstance()->getInt("users", player->getUserid(), "gender"));
		//for(int i=0; i<(int)player->getIDs()->size(); i++){
		if(Worlds::getInstance()->isPlayerConnected(player->getUserid())){
				s=-2;
				
			}
		//}
	}
	if(s == 1){
		printf("%s logged in.\n", (char*)username.c_str());
		//player->setPin(MySQL::getInstance()->getInt("users", player->getUserid(), "pin"));
		//int pin = player->getPin();
		//if(pin == -1)
		//	player->setStatus(1); // New PIN
		//else
		//	player->setStatus(2); // Ask for PIN
		player->setGender(MySQL::getInstance()->getInt("users", player->getUserid(), "gender"));
		//player->send(PacketCreator().loginConnect(player->getGender(), username, (player->getStatus() == 1)));
		player->send(PacketCreator().nPacketLOGIN_ACK(password,"STCT1B20101215014606","24C58F0ED3F871EF42406BB0B35235D52C271488A40601F3C75C1637BBB60145C9D34C6D01DF0A9BC8081F8324EBDFF3907D1DE2A59B6A066AF8D64B69E787B5A343F071CD26838F7378359A379CE7E7EFE06FA01361275FBB047EE8E35E703507A507693DDF3E51"));
		//player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\LoginServer\\nPacketLOGIN_ACK.txt","nPacketLOGIN_ACK - 0x31",true));
		player->loadCharacters();
	}
	else{
		short error = 0;
		switch(s){
			case -3: error=0x0;break; //User in Game
			case -2: error=0x0E;break;//Password Mot ok
			case -1: error=0x0D;break;//User Name Not Ok
			case 0: error=4;break;
		}
		player->send(PacketCreator().loginError(error));
	}
	
	
}
void PlayerLoginHandler::channelSelectHandle(PacketReader& packet){
	printf("PlayerLoginHandler::channelSelectHandle\n");
	packet.read();
	packet.read();
	player->setChannel(packet.read());
	//vector <Character* > toshow;
	//for(int i=0; i<(int)player->getCharacters()->size(); i++)
	//	if((*player->getCharacters())[i]->getWorld() == player->getServer())
	//		toshow.push_back((*player->getCharacters())[i]);
	//player->send(PacketCreator().showCharacters(toshow));
	 
	player->send(PacketCreator().SelectChRespone(Worlds::getInstance()->getStringIP(),17101,15199));
	//player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\LoginServer\\CharServerLoginRes.txt","CharServerLoginRes - 0x35",true));
	LogInfo tmpinfo;
	player->SetPlayerIdForLogin(tmpinfo.UserPIds);
	tmpinfo.UserId = player->getUserid();
	PgoLogin::getInstance()->setPlayerLoginId(player->getIP(),tmpinfo);
}
void PlayerLoginHandler::worldSelectHandle(PacketReader& packet){
	printf("PlayerLoginHandler::worldSelectHandle\n");
	player->setServer(packet.read());
	player->send(PacketCreator().showChannels());
}
void PlayerLoginHandler::loginHandle(PacketReader& packet){
	printf("PlayerLoginHandler::loginHandle\n");
	int status = player->getStatus();
	if(status == 1)
		player->send(PacketCreator().loginProcess(0x01));
	else if(status == 2){
		//player->send(PacketCreator().loginProcess(0x04));
		//player->setStatus(3);
		player->send(PacketCreator().loginProcess(0x00));
		player->setStatus(4);
	}
	//else if(status == 3)
		//checkPin(player, packet);
	else if(status == 4)
		player->send(PacketCreator().loginProcess(0x00));
}
void PlayerLoginHandler::showWorldRequestHandle(PacketReader& packet){
	printf("PlayerLoginHandler::showWorldRequestHandle\n");
	
   player->send(PacketCreator().showWorld(Worlds::getInstance()->getWorld(0)));
   //player->send(PacketCreator().FromFile("D:\\Mgame\\Packets\\LoginServer\\nPacketSERVERLIST_ACK.txt","nPacketSERVERLIST_ACK - 0x33",true));
	/*
	for(int i=0; i<Worlds::getInstance()->getWorldsCount(); i++){
		player->send(PacketCreator().showWorld(Worlds::getInstance()->getWorld(i)));
	}
	player->send(PacketCreator().endWorlds());
*/
}
void PlayerLoginHandler::characterSelectHandle(PacketReader& packet){
	printf("PlayerLoginHandler::characterSelectHandle\n");
	int id = packet.readInt();
	if(player->getCharacter(id) == NULL) return;
	MySQL::getInstance()->setString("characters", "ip", id, (char*)player->getIP().c_str());
	Channel* channel = Worlds::getInstance()->getWorld(player->getServer())->getChannels()->getChannel(player->getChannel());
	if(channel == NULL) return;
	player->send(PacketCreator().connectChannel(id, channel->getPort()));
}
void PlayerLoginHandler::nameSelectHandle(PacketReader& packet){
	printf("PlayerLoginHandler::nameSelectHandle\n");
	int Ppos = packet.readInt();
	player->setLastPos(Ppos);
	string name = packet.readStringNoSise2();
	player->send(PacketCreator().checkName(name, MySQL::getInstance()->isString("characters", "name", name)));
}
void PlayerLoginHandler::createCharacterHandle(PacketReader& packet){
	printf("PlayerLoginHandler::createCharacterHandle(\n");
	packet.readInt();
	string name = packet.readString(20);
	unsigned char gender = packet.read();
	packet.read();
	packet.read();
	packet.read();

	if(MySQL::getInstance()->isString("characters", "name", name))
		return;
	//int face = packet.readInt();
	//int hair = packet.readInt();
	//int haircolor = packet.readInt();
	//int skin = packet.readInt();
	int eEye = packet.readInt();
	int eHair = packet.readInt();
	int eWep = packet.readInt();
	int eArmor = packet.readInt();
	//int gender = packet.read();
	//int str=packet.read(),dex=packet.read(),intt=packet.read(), luk=packet.read();
	//if(str + dex + intt + luk != 25 || str < 4 || dex < 4 || intt < 4 || luk < 4)
	//	return;

	//hair += haircolor;
	//NAME ,Gender
	int Pos = 0;
	int poss[4] = {0};
	vector <Character*>* chars = player->getCharacters();
	for(int i = 0 ;i < (*chars).size();i++)
			poss[(*chars)[i]->getPos()-1] = 1;
	for(int i = 0; i < 4;i++)
		if(poss[i] == 0)
		{
			Pos = i+1;
			break;
		}
	
	int id = MySQL::getInstance()->setChar(player->getUserid(), name, Pos, 0, 0, gender, 3, 3, 3, 3, 0);
	MySQL::getInstance()->createEquip(eEye, 8, id);
	MySQL::getInstance()->createEquip(eHair, 7, id);
	MySQL::getInstance()->createEquip(eWep, 0, id);
	MySQL::getInstance()->createEquip(eArmor, 1, id);
	player->addID(id);
	player->loadCharacters();
	Character* character = player->getCharacter(id);
	if(character == NULL) return;
	//character->setPos(player->getLastPos());
	player->send(PacketCreator().addCharacter(character));

	LogInfo tmpinfo;
	player->SetPlayerIdForLogin(tmpinfo.UserPIds);
	tmpinfo.UserId = player->getUserid();
	PgoLogin::getInstance()->setPlayerLoginId(player->getIP(),tmpinfo);
}
void PlayerLoginHandler::deleteCharacterHandle(PacketReader& packet){
	printf("PlayerLoginHandler::deleteCharacterHandle\n");
	packet.readInt();//counter
	//int date = packet.readInt();
	int playerpos = packet.readInt();
	playerpos++;
	int id = 0;

	Character* character =  player->getCharacterByPos(playerpos);
	if(character == NULL)
		return;
	id = character->getID();
	MySQL::getInstance()->deletePlayer(id);
	player->removeID(id);
	player->removeCharacter(id);
	player->send(PacketCreator().removeCharacter(playerpos));
}
void PlayerLoginHandler::loginBackHandle(PacketReader& packet){
	printf("PlayerLoginHandler::loginBackHandle\n");
	player->send(PacketCreator().logBack());
}