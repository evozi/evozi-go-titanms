 /*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/


#include "PacketCreator.h"
#include "PacketWriter.h"
#include "Drop.h"
#include "Tools.h"
#include "PlayerInventories.h"
#include "Item.h"

PacketWriter* PacketCreator::dropObjects(vector<Drop*> drops, MapObject *dropper){
	pw.writeBytes("0501");//start
	pw.writeBytes("4A00");//Head
	pw.writeBytes("9A00");//Size
	pw.writeBytes("E901");//Crc
	pw.writeBytes("00000000");//PacketCounter
	//DropsIds
	int count = drops.size();
	for(int i = 0 ;i < 7 ;i++)
	{
		if(count <= i)
			pw.writeInt(0);
		else
			pw.writeInt(drops[i]->getID());
	}
	for(int i = 0 ;i < 7 ;i++)
	{
		if(count <= i)
			pw.writeInt(0);
		else
			pw.writeInt(drops[i]->getItem()->getID());
	}
	for(int i = 0 ;i < 7 ;i++)
	{
		if(count <= i)
			pw.writeShort(0);
		else
			pw.writeShort(drops[i]->getPosition().x);
	}
	for(int i = 0 ;i < 7 ;i++)
	{
		if(count <= i)
			pw.writeShort(0);
		else
			pw.writeShort(drops[i]->getPosition().y);
	}

	if(drops[0] != NULL)
		pw.writeInt(drops[0]->getOwner());

	for(int i = 0 ;i < 7 ;i++)
	{
		if(count <= i)
			pw.writeInt(0);
		else
			pw.writeInt(drops[i]->getAmount());
	}

	pw.writeBytes("0060A40D");//Junk
	pw.writeBytes("01000000");//
	pw.writeBytes("0060A40D");//
	pw.writeBytes("01000000");//
	pw.writeBytes("0060A40D");//
	pw.writeBytes("9F9700009ED8");//
return &pw;
}
PacketWriter* PacketCreator::dropObject(Drop* drop, char mode, MapObject* dropper){
	pw.writeBytes("0501");//start
	pw.writeBytes("4E00");//Head
	pw.writeBytes("3200");//Size
	pw.writeBytes("8501");//Crc
	pw.writeBytes("00000000");//PacketCounter


	pw.writeInt(drop->getID());
	pw.writeInt(drop->getItem()->getID());
	pw.writeShort(dropper->getPosition().x);
	pw.writeShort(dropper->getPosition().y);
	pw.writeInt(drop->getItem()->getAmount());

	pw.writeShort(dropper->getPosition().x);
	pw.writeShort(dropper->getPosition().y);
	pw.writeInt(drop->getItem()->getAmount());

	pw.writeBytes("00704000E8031426C904000068A2");//Junk
	return &pw;
}
PacketWriter* PacketCreator::showDrop(Drop* drop){
	return dropObject(drop, 2, NULL);
}
PacketWriter* PacketCreator::gainDrop(int itemid, int amount, bool mesos){

	pw.writeShort(SHOW_GAIN);
	pw.write(0);
	if(itemid == 0){
		pw.write(-1);
	}
	else{
		pw.write(mesos);
		pw.writeInt(itemid);
		if(mesos)
			pw.writeShort(0); // Internet Cafe Bonus
		else if(INVENTORY(itemid) != EQUIP)
			pw.writeShort(amount);
	}
	if(!mesos){
		pw.writeLong(0);
	}
	return &pw;

}
PacketWriter* PacketCreator::gainMesos(int mesos){
	return gainDrop(mesos, 0, true); 
}

PacketWriter* PacketCreator::lootError(){
	return gainDrop(0, 0, 0); 
}

PacketWriter* PacketCreator::removeDrop(int dropid, int PlayerID, int ItemID, char type){
	pw.writeBytes("0501");//start
	pw.writeBytes("4D00");//Head
	pw.writeBytes("3200");//Size
	pw.writeBytes("8401");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(PlayerID);
	pw.writeInt(dropid);
	pw.writeInt(ItemID);
	pw.writeBytes("0001");
	pw.writeBytes("FFFF");
	pw.writeInt(ItemID);
	pw.writeBytes("0001");
	pw.writeBytes("FFFF");

	pw.writeBytes("00304000E8034E919F970000412C");
	return &pw;
}

PacketWriter* PacketCreator::explodeDrop(int dropid){
	//return removeDrop(dropid, 4);
	return 0;
}
PacketWriter* PacketCreator::lootDrop(int dropid, int playerid){
	return removeDrop(dropid, 2, playerid);
}