/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef PETCHATCOMMANDDATA_H
#define PETCHATCOMMANDDATA_H

#include "DataStruct.h"

class PetCommandData : public DataStruct {
private:
	int Exp;
	int HP;
	int LIFE;
	short PATTACK;
	short DEF;
	short SPEED;
	short AttackRange;
	short AttackDelay;
	short GainDestance;
	short GainRange;
	short GainDelay;
	short MoveType;
	int DefReduce;
public:
	PetCommandData(int id){
		this->id = id;
		Exp = 0;
		HP = 0;
		LIFE = 0;
		PATTACK = 0;
		DEF =0;
		SPEED =0;
		DefReduce = 0;
	}
	void setExp(int exp ){
		Exp = exp;
	}
	void setHP(int hp ){
		HP = hp;
	}
	void setLIFE(int  life){
		LIFE = life;
	}
	void setPATTACK(short pattack ){
		PATTACK = pattack;
	}
	void setDEF(short def){
		DEF = def;
	}
	void setSPEED(short speed){
		SPEED = speed;
	}
	void setAttackRange(short  attrange){
		AttackRange = attrange;
	}
	void setAttackDelay(short attdelay){
		AttackDelay = attdelay;
	}
	void setGainDestance(short gdestance ){
		GainDestance = gdestance;
	}
	void setGainRange (short grange ){
		GainRange = grange;
	}
	void setGainDelay (short gdelay ){
		GainDelay = gdelay;
	}
	void setMoveType (short  movet){
		MoveType = movet;
	}
	void setDefReduce (int defredus ){
		DefReduce = defredus;
	}

	int getExp (){
		return Exp;
	}
	int getHP (){
		return HP;
	}
	int getLIFE (){
		return LIFE;
	}
	short getPATTACK (){
		return PATTACK;
	}
	short getDEF (){
		return DEF;
	}
	short getSPEED (){
		return SPEED;
	}
	short getAttackRange (){
		return AttackRange;
	}
	short getAttackDelay (){
		return AttackDelay;
	}
	short getGainDestance (){
		return GainDestance;
	}
	short getGainRange (){
		return GainRange;
	}
	short getGainDelay (){
		return GainDelay;
	}
	short getMoveType (){
		return MoveType;
	}
	int getDefReduce (){
		return DefReduce;
	}
	

};

#endif