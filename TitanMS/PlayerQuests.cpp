/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "DataProvider.h"
#include "PlayerQuests.h"
#include "PacketCreator.h"
#include "Player.h"
#include "Quest.h"
#include "QuestsData.h"
#include "QuestData.h"
#include "QuestRewardsData.h"
#include "QuestRewardData.h"
#include "QuestRequestsData.h"
#include "QuestRequestData.h"
#include "PlayerSkills.h"
#include "Skill.h"
#include "Item.h"
#include "Worlds.h"
void PlayerQuests::giveQuest(int questid, int npcid){
	QuestData* questd = DataProvider::getInstance()->getQuestData(questid);
	if(questd == NULL)
		return;
	Quest* quest = new Quest(questid);
	quest->getKilledMobs();
	quest->setState(0x31);
	pquests[questid] = quest;
	cquests[questid] =quest;
	player->send(PacketCreator().QuestAll(getQuestsInProgress(),getCompletedQuests()));
	//rewardsData(questid,1);
	//addQuestReqs(questid);
}
void PlayerQuests::removeQuest(short questid){
	pquests.erase(questid);
	cquests.erase(questid);
	player->send(PacketCreator().QuestAll(getQuestsInProgress(),getCompletedQuests()));
}
void PlayerQuests::UpdateQuest(short questid, int mobid){

	Quest* quest =  player->getQuests()->getQuestByID(questid);
	quest->addKmob(mobid);
	
	int NeedKill = 3;
	int Killed = quest->GetKillMobs(mobid);

	if(Killed == NeedKill)
		player->send(PacketCreator().QuestUpdate(questid,1,0,Killed));
	else
		player->send(PacketCreator().QuestUpdate(questid,1,1,Killed));

}
Quest* PlayerQuests::getQuestByID(int questid){
	return pquests[questid];
}
void PlayerQuests::completeQuest(int questid){
	Quest* quest = cquests[questid];
	quest->setState(0x32);

	pquests.erase(questid);
	rewardsData(questid);
	//player->send(PacketCreator().forfeitQuest(questid));
}
void PlayerQuests::completeQuest(Quest* quest){
	
}
void PlayerQuests::deleteAll(){

}
void PlayerQuests::rewardsData(int questid){
	QuestData* questd = DataProvider::getInstance()->getQuestData(questid);
	vector<QuestRewardData*> *rewards = questd->getRewardsData()->getData();
	for(int i = 0; i < (int)rewards->size(); i++) {
		if(rewards[i].at(i)->getJob() > 0)
		{
			player->setJob(rewards[i].at(i)->getJob());			//CangeJobHAdler
			player->setStr(3);
			player->setDex(3);
			player->setVit(3);
			player->setInt(3);
			player->setAbiltyPoints(40);
			player->send(PacketCreator().Player_CHAR_ALL(player));
			player->send(PacketCreator().QuestAll(getQuestsInProgress(),getCompletedQuests()));
			player->send(PacketCreator().LVLMSG());
		}

	}
	
}
