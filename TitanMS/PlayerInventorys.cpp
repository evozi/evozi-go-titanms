/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "DataProvider.h"
#include "PlayerInventories.h"
#include "Inventory.h"
#include "Item.h"
#include "Equip.h"
#include "ItemsData.h"
#include "Pet.h"
#include "EquipsData.h"
#include "PetsData.h"
#include "DataProvider.h"

bool CompareItems::operator()(Item* x, Item* y) const
{
	return x->getID() < y->getID(); // what is the order in global?
	//return DataProvider::getInstance()->getItemName(x->getID()) < DataProvider::getInstance()->getItemName(y->getID());
}
PlayerInventories::PlayerInventories(Player* player){
	this->player = player;
	invs[0] = new Inventory(0, 19, player);
	for(int i=1; i<6; i++){
		invs[i] = new Inventory(i, 48, player);
	}
	empty = new Inventory(-1, 0, player);
}
bool PlayerInventories::giveItem(int itemid, int amount,bool lock ,bool rands){
	//if(!validID(itemid))
	//	return false;
	int inv = getINVbyITEM(itemid);
	if (inv == -1)
		return false;
	Item* item;
	if(inv == 1 || inv == 2)
		for(int i=0; i<amount; i++){
			item = new Equip(itemid, rands);
			item->setLocked(lock);
			bool check = getInventory(inv)-> addItem(item);
			if(!check)
				return false;
		}	
	else if(IS_PET(itemid) || IS_TOY(itemid))
		for(int i=0; i<amount; i++){
			item = new Pet(itemid);
			item->setLocked(lock);
			bool check = getInventory(inv)-> addItem(item);
			if(!check)
				return false;
		}	
	else{ 
		int max = DataProvider::getInstance()->getItemMaxPerSlot(itemid);
		while(amount > 0){
			if(amount%max == 0){
				item = new Item(itemid, max);
				amount -= max;
			}else{
				item = new Item(itemid, amount%max);
				amount -= amount%max;				
			}
			bool check = getInventory(inv)-> addItem(item);
			if(!check)
				return false;
		}
	}
	return true;
}
bool PlayerInventories::giveItem(Item* item, bool drop){
	return getInventory(getINVbyITEM(item->getID()))->addItem(item, true, drop,false);
}
Item* PlayerInventories::getItemBySlot(int inv, int slot){
	return getInventory(inv)->getItemBySlot(slot);
}
Item* PlayerInventories::getItemByID(int id){
	if(VALID_INVENTORY(INVENTORY(id)))
		return getInventory(INVENTORY(id))->getItemByID(id);
	return NULL;
}/*
void PlayerInventories::removeItem(int inv, int slot){
	getInventory(inv)->removeItem(slot);
	
}*/
void PlayerInventories::removeItem(int itemid){
	Item* item;
	while((item = getInventory(getINVbyITEM(itemid))->getItemByID(itemid)) != NULL)
		getInventory(INVENTORY(itemid))->removeItem(item, false, true);
	
}/*
void PlayerInventories::removeItem(Item* item){
	getInventory(INVENTORY(item->getID()))->removeItem(item);	
}*/

bool PlayerInventories::validID(int id){
	int inv = INVENTORY(id);
	if(IS_EQUIP(id))
		return (EquipsData::getInstance()->getDataByID(id) != NULL);
	else if(IS_PET(id))
		return (PetsData::getInstance()->getDataByID(id) != NULL);
	else
		return (ItemsData::getInstance()->getDataByID(id) != NULL);
	return false;
}

int PlayerInventories::getINVbyITEM(int id){
	
	if(IS_EVENT_WEP(id))  return 1;
	if(IS_WEP_BOX(id))    return 1;
	if(IS_ARCH_WEP(id))   return 1;
	if(IS_SHOOTER_WEP(id))return 1;
	if(IS_GSP_WEP(id))    return 1;
	if(IS_WEP(id))        return 1;

	if(IS_MARK(id))    return 2;//??      (id >= 8100000 && id < 8109999) //Done
	if(IS_CUPLE(id))      return 3;//   (id >= 8892000 && id < 8899999) //Done
	if(IS_EARNING(id))    return 2;//   (id >= 7510000 && id < 7609999) //DONE
	if(IS_SEANCE(id))     return 1;//   (id >= 6800000 && id < 6999999) //Done
	if(IS_DRESS(id))      return 1;//   (id >= 8110000 && id < 8209999) //Done
	if(IS_DRESS2(id))     return 1;//   (id >= 9510000 && id < 9609999) //DONE
	if(IS_RING(id))       return 2;
	if(IS_NECKLACE(id))   return 2;
	if(IS_MANTLE(id))     return 2;//   (id >= 8410000 && id < 8509999) //DONE
	if(IS_SEAL(id))       return 2;
	if(IS_SPEND(id))      return 3;   //(id >= 8810000 && id < 8909999) //DONE
	if(IS_Jebok(id))      return 3;//??   (id >= 1200000 && id < 1299999) //DONE
	if(IS_Pazel(id))      return 3;
	if(IS_HAT(id))        return 2;//   (id >= 8610000 && id < 8689999) //DONE
	if(IS_SHOPIT(id))     return 3;//??   (id >= 8699001 && id < 8699999) //DONE
	if(IS_OTHER(id))      return 4;   //(id >= 8910000 && id < 9009999) //DONE
	if(IS_FACE(id))       return 2;//   (id >= 8710000 && id < 8809999) //DONE
	if(IS_FACE2(id))      return 2;//   (id >= 9410000 && id < 9509999) //DONE
	if(IS_EYE(id))        return 2;//??   (id >= 9110000 && id < 9209999) //DONE
	if(IS_HAIR(id))       return 2;//??   (id >= 9010000 && id < 9109999) //DONE
	if(IS_PET(id))        return 5;
	if(IS_TOY(id))        return 5;//   (id >= 9231031 && id < 9232105) //DONE
	if(IS_PET_DECO(id))   return 5;   //(id >= 9220011 && id < 9229999) //DONE
	if(IS_SPEND_PET(id))  return 5;
	if(IS_HAIRACC(id))    return 2;//   (id >= 7300001 && id < 7399999) //DONE
	return -1;
}
int PlayerInventories::getItemAmount(int id){
	return getInventory(INVENTORY(id))->getItemAmount(id);
}
void PlayerInventories::removeItemBySlot(int inv, int slot, int amount, bool send){
	getInventory(inv)->removeItemBySlot(slot, amount, send);
}
void PlayerInventories::deleteAll(){
	for(int i=0; i<6; i++){
		invs[i]->deleteAll();
	}
	delete empty;
}