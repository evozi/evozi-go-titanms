/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "DataProvider.h"
#include "PlayerHandler.h"
#include "Player.h"
#include "Mob.h"
#include "Map.h"
#include "PacketCreator.h"
#include "PacketReader.h"
#include "ObjectMoving.h"
#include "PlayerInventories.h"
#include "Inventory.h"
#include "Item.h"
#include "Damage.h"
#include "Map.h"
#include "MapMobs.h"
#include <vector>
using namespace std;

void PlayerHandler::damageMobMagicHandle(PacketReader& packet){
	Damage damage = Damage(packet);
	player->getMap()->send(PacketCreator().damageMobMagic(player->getID(), damage), player);
	player->getMap()->getMobs()->damage(player, damage);
}
void PlayerHandler::damageMobHandle(PacketReader& packet){
/*
	//Analyse
	short PlayerID=0;
	short MobID=0;
	short SamthingWithMob=0;
	short Val1=0;
	int AtkDmg=0;
	short MobX=0; //-100
	short Val2=0;
	int SkillUse=0;
	unsigned char Val3=0;
	unsigned char Val4=0;
	short Val5=0;
	int PlayerHitC=0;
	unsigned char Val7=0;
	unsigned char Val8=0;
	short HitPosX=0;
	short HitPosY=0;
	short Val9=0;
	packet.readInt(); //PoacketCounter
	
	PlayerID =			packet.readShort();
	MobID =				packet.readShort();
	SamthingWithMob =	packet.readShort();
	Val1 =				packet.readShort();
	AtkDmg =		packet.readInt();
	MobX =				packet.readShort();
	Val2 =				packet.readShort();
	SkillUse =		packet.readInt();
	Val3 =            packet.read();
	Val4 =	          packet.read();
	Val5 =				packet.readShort();
	PlayerHitC =	packet.readInt();
	Val7 =		      packet.read();
	Val8 =            packet.read();
	HitPosX =			packet.readShort();
	HitPosY =			 packet.readShort();
	Val9 =				packet.readShort();
	printf("PlayerID : %d\n",PlayerID);
	printf("MobID    : %d\n",MobID);
	printf("Val1     : %d\n",Val1);
	printf("AtkDmg   : %d\n",AtkDmg);
	printf("MobX     : %d\n",MobX);
	printf("Val2     : %d\n",Val2);
	printf("SkillUse : %d\n",SkillUse);
	printf("Val3     : %X\n",Val3);
	printf("Val4     : %X\n",Val4);
	printf("Val5     : %d\n",Val5);
	printf("PHitC    : %d\n",PlayerHitC);
	printf("Val7     : %X\n",Val7);
	printf("Val8     : %X\n",Val8);
	printf("HitPosX  : %d\n",HitPosX);
	printf("HitPosY  : %d\n",HitPosY);
	printf("Val9     : %d\n",Val9);
	
	Mob* mob = player->getMap()->getMob(MobID);
	if(mob == NULL){
		printf("Mob Not Found In Map\n");
		return;
	}
	else
	{
		printf("Mob HP : %d\n",mob->getHP());
		printf("X,Y : %d,%d\n",mob->getPosition().x,mob->getPosition().y);
	}
/*
	mob->SetMobState(0x7);
	
printf("MobX     : %d =%d\n",MobX,abs(HitPosX - MobX));
printf("MobX+100 : %d =%d\n",MobX+100,abs(HitPosX -(MobX+100)));
printf("MobX-100 : %d =%d\n",MobX-100,abs(HitPosX - (MobX-100)));
printf("MobHitX  : %d \n",HitPosX);
int min =abs(HitPosX - MobX);
int val = MobX;

if(abs(HitPosX -(MobX+100) < min))
{
	min = abs(HitPosX -(MobX+100));
	val = MobX+100;
}
if(abs(HitPosX -(MobX-100) < min))
{
	min = abs(HitPosX -(MobX-100));
	val = MobX-100;
}
Position pos = mob->getPosition();
	if(HitPosX > val) //Attack From Left
		pos.Facing  = 0x1;
	else
		pos.Facing = 0xFF;

	mob->setPosition(pos);
		mob->setHP(mob->getHP() - 13);
		if(mob->getHP() <= 0)
			mob->SetMobState(0x9);
		
		player->getMap()->send(PacketCreator().AttackMob(mob,5,13,1,HitPosX,HitPosY));
	
		if(mob->getHP() <= 0)
			player->getMap()->getMobs()->kill(mob,player);
		else
			mob->SetMobState(0x1);

*/
	
	Damage damage = Damage(packet);
	/*
	player->getMap()->send(PacketCreator().damageMob(player->getID(), damage), player);
	*/
	player->getMap()->getMobs()->damage(player, damage);
	

}
void PlayerHandler::damageMobRangedHandle(PacketReader& packet){
	Damage damage = Damage(packet, true);
	short slot = damage.getItemSlot();
	Item* item = player->getInventories()->getItemBySlot(USE, slot);
	Item* touse = item;
	if(item == NULL || item->getAmount() <= 0)
		return;
	int itemid = item->getID();
	// TODO: 
	Item* weapon = player->getInventories()->getInventory(EQUIPPED)->getItemByType(147);
	if(weapon != NULL){
		Item* titem = player->getInventories()->getInventory(CASH)->getItemByType(502);
		if(titem != NULL)
			itemid = titem->getID();
	}
	if(!item->isBullet()){
		return;
	}
	player->getMap()->send(PacketCreator().damageMobRanged(player->getID(), damage, itemid), player);
	player->getMap()->getMobs()->damage(player, damage, item);
}
void PlayerHandler::controlMobHandle(PacketReader& packet){
	int mobid = packet.readInt();
	Mob* mob = player->getMap()->getMob(mobid);
	if(mob == NULL)
		return;
	short type =  packet.readShort();
	char skillType = packet.read();
	int skill = packet.readInt();
	packet.read(6);
	Position pos;
	pos.x = packet.readShort();
	pos.y = packet.readShort();
	ObjectMoving moving = ObjectMoving(packet, mob);
	if(player != mob->getControl())
		mob->setControl(player, false, true);
	else if(skill == 255 && mob->getAggressive() == 2)
		mob->setAggressive(0);
	player->send(PacketCreator().moveMobResponse(mob, type, (mob->getAggressive() != 0)));
	//Position newpos = moving.getPosition();
	//newpos.y-=2;
	//newpos = mob->getMap()->findFoothold(newpos);
	player->getMap()->send(PacketCreator().moveMob(mob->getID(), pos, moving, skillType, skill), player, mob->getPosition());


}
void PlayerHandler::controlMobSkillHandle(PacketReader& packet){
	// Not sure..
}
void PlayerHandler::hitByMobHandle(PacketReader& packet){
	// TODO get packets (henesysPQ)
	int byid = packet.readInt();
	int mobid = packet.readInt();
	Mob* by = player->getMap()->getMob(byid);
	Mob* mob = player->getMap()->getMob(mobid);
	if(by == NULL || mob == NULL)
		return;
	//if(mob->getID()//if mob can be hitted by a mob TODO
}