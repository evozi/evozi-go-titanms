/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef MAPDATA_H
#define MAPDATA_H

#include "TopDataStruct.h"
#include "MapMobsData.h"
#include "MapNPCsData.h"
#include "MapReactorsData.h"
#include "MapPortalsData.h"
#include "MapFootholdsData.h"

class MapData : public DataStruct {
private:
	MapMobsData* mobs;
	MapNPCsData* NPCs;
	MapReactorsData* reactors;
	MapPortalsData* portals;
	MapFootholdsData* footholds;
	int returnMap;
	vector<vector<char>> MapPex;
	int MapH;
	int MapW;
	short pRegionid;
	short pMapid;
public:
	MapData(int id){
		this->id = id;
		mobs = new MapMobsData();
		NPCs = new MapNPCsData();
		reactors = new MapReactorsData();
		portals = new MapPortalsData();
		footholds = new MapFootholdsData();
	}

	MapMobsData* getMobsData(){
		return mobs;
	}
	MapNPCsData* getNPCsData(){
		return NPCs;
	}
	MapReactorsData* getReactorsData(){
		return reactors;
	}
	MapPortalsData* getPortalsData(){
		return portals;
	}
	MapFootholdsData* getFootholdsData(){
		return footholds;
	}
	vector<vector<char>> GetMapPex(){
		return MapPex;
	}
	void SetMapH_W(int H,int W){
		this->MapH = H*32;
		this->MapW = W*32;
		MapPex.resize(H);
		for (int i = 0; i < H; ++i)
			 MapPex[i].resize(W*33);
	}
	void SetMapPexel(int X,int Y,char Val)
	{
		this->MapPex[Y][X]  = Val;
	}
	char GetMapPexel(int X,int Y)
	{
		Y = Y/32;
		X = X+(X/32)+1;
		return this->MapPex[Y][X];
	}
	int GetMapW()
	{
		return this->MapW;
	}
	char GetPexInfo(int X,int Y)
	{
		Y = Y/32;
		X = 32 * (X / 32) + (X / 32);
		return this->MapPex[Y][X];
	}
	void setReturnMap(int returnMap){
		this->returnMap = returnMap;
	}
	int getReturnMap(){
		return returnMap;
	}
	void setRegionid(short Rid){
		this->pRegionid = Rid;
	}
	short getRegionid(){
		return pRegionid;
	}
	void setMapid(short Mid){
		this->pMapid = Mid;
	}
	short getMapid(){
		return pMapid;
	}
};


#endif