/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "DataProvider.h"
#include "PlayerHandler.h"
#include "PacketReader.h"
#include "PacketWriter.h"
#include "PacketCreator.h"
#include "Player.h"
#include "Map.h"
#include "MapDrops.h"
#include "DataProvider.h"
#include "PlayerInventories.h"
#include "Channel.h"
#include "Effect.h"
#include "Maps.h"
#include "Pet.h"
#include "Inventory.h"
#include "Item.h"
#include "ObjectMoving.h"
#include "PlayerSkills.h"
#include "Skill.h"
#include "Equip.h"
#include "ItemData.h"
#include <algorithm>
#include "ItemEffectData.h"
#include "PlayerBuffs.h"
#include "Tools.h"
using namespace Tools;

void PlayerHandler::cancelChairHandle(PacketReader& packet){
	player->setChair(0);
	player->send(PacketCreator().cancelChair());
	player->getMap()->send(PacketCreator().useChair(player->getID(), 0), player); 
}
void PlayerHandler::useChairHandle(PacketReader& packet){
	int chairid = packet.readInt();
	Item* item = player->getInventories()->getItemByID(chairid);
	if(item == NULL || MAJOR_TYPE(chairid) != 301) // not a chair
		return;
	player->setChair(chairid);
	player->getMap()->send(PacketCreator().useChair(player->getID(), chairid), player); 
}
void PlayerHandler::useItemEffectHandle(PacketReader& packet){
	int effect = packet.readInt();
	Item* item = player->getInventories()->getItemByID(effect);
	if((item == NULL || MAJOR_TYPE(effect) != 501) && effect != 0) 
		return;
	player->setItemEffect(effect);
	player->getMap()->send(PacketCreator().useItemEffect(player->getID(), effect), player); 

}
void PlayerHandler::unlockItemHandle(PacketReader& packet){
	//Chk To see If Its Eye Or Haier Then Equip It Dont Replace IT
	packet.readInt();//Counter

	int Inv = packet.readInt();
	int Pos = packet.readInt();

	Equip* itm = (Equip*)player->getInventories()->getItemBySlot(Inv, Pos);
	if(itm == NULL)
		return;
	if(IS_HAIR(itm->getID()))
	{
		
		player->getInventories()->getInventory(0)->removeItem(7, false, true, false);//Remove Player EYE
		player->getInventories()->getInventory(Inv)->removeItem(Pos,false,false,false);//Remov From INV
		itm->setSlot(7);
		itm->setLocked(false);
		player->getInventories()->getInventory(0)->addItem(itm,false,false,false); //Set New Haier
	
	}
	if(IS_EYE(itm->getID()))
	{
		player->getInventories()->getInventory(0)->removeItem(8, false, true, false);//Remove Player EYE
		player->getInventories()->getInventory(Inv)->removeItem(Pos,false,false,false);//Remov From INV
		itm->setSlot(8);
		itm->setLocked(false);
		player->getInventories()->getInventory(0)->addItem(itm,false,false,false); //Set New EYE
	}

	player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(0),0));//UpdateEquip
	player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(Inv),Inv));//UpdateInvFrom
	player->getMap()->send(PacketCreator().Player_SET_AVATAR(player));//UpdatePlayer
}
void PlayerHandler::useCashItemHandle(PacketReader& packet){
	short slot = packet.readShort();
	int itemid = packet.readInt();
	Item* item = player->getInventories()->getItemBySlot(CASH, slot);
	if(item == NULL || item->getID() != itemid){
		return;
	}
	switch(MAJOR_TYPE(itemid)){
		//case 504: break; //TODO: VIP Rock
		case 505:
		//case 503: break; //TODO: Shops
			{
				int to = packet.readInt();
				int from = packet.readInt();
				if(itemid%10 == 0){
					Values values;
					Value v = player->removeStat(from);
					if(v.getType() == 0)
						return;
					values.add(v);
					v = player->addStat(to);
					if(v.getType() == 0)
						return;
					values.add(v);
					player->send(PacketCreator().updateStats(values, true));
				}
				else{
					int job = itemid%10;
					if(SKILL_JOB_N(to) != job || SKILL_JOB_N(from) > job)
						return;
					Skill* froms = player->getSkills()->getSkill(from);
					Skill* tos =  player->getSkills()->getSkill(to);
					if(froms->getLevel() == 0 || tos->getLevel () == tos->getMasterLevel())
						return;
					froms->setLevel(froms->getLevel()-1);
					tos->setLevel(tos->getLevel()+1);
					vector <Skill*> skills;
					skills.push_back(froms);
					skills.push_back(tos);
					player->send(PacketCreator().updateSkill(skills));

				}
				break;
			}
		case 506:
			{
				switch(itemid%10){
					case 0:
						{
							short pos = packet.readShort();
							Equip* equip = (Equip*)player->getInventories()->getItemBySlot(EQUIPPED, pos);
							if(equip == NULL || equip->getOwner() != "")
								return;
							equip->setOwner(player->getName());
							player->send(PacketCreator().newItem(equip, true));
							break;
						}
					/*
					case 1:
						{	 // How to unlock item??
							packet.show();
							int inv = packet.readInt();
							int pos = packet.readInt();
							Item* on = player->getInventories()->getItemBySlot(inv, pos);
							if(on == NULL)
								return;
							on->setLocked(!on->getLocked());
							player->send(PacketCreator().newItem(on, true));
							break;
						}
					case 2: break;// egg?				
					*/
					default: player->send(PacketCreator().enableAction());
				}
				break;
			} 
		//case 507: break;// TODO: megaphone
		//case 508: break; // Old thing 0.o
			/*
		case 509:
			{	
				string receiver = packet.readString();
				string note = packet.readString();
				// TODO: Get note packet 0.o
				break;
			}
		case 510: 
			{
				player->getMap()->send(PacketCreator().changeSound("Jukebox/Congratulation")); // need to add timer
			}*/
		case 530:
			{
				Effect* effect = Effect::getEffect(itemid);
				if(effect != NULL)
					effect->use(player);
				else
					player->send(PacketCreator().enableAction());
			}
		default: player->send(PacketCreator().enableAction());
	}
	player->getInventories()->removeItemBySlot(CASH, slot, 1);
}
void PlayerHandler::SellItemHandle(PacketReader& packet){
	packet.readInt();//Read Counter

	int itemid = packet.readInt();
	char InvFrom = packet.read();
	char SlotFrom = packet.read();
	int  amount= packet.readShort();


		if(amount < 1) amount = 1;
		Item* item = player->getInventories()->getItemBySlot(InvFrom, SlotFrom);
		
		if(item == NULL)
			return;
		if(item->getID() != itemid || item->getAmount() < amount)
			return;
		if(item->getAmount() == amount) //Sell All
			player->getInventories()->getInventory(InvFrom)->removeItem(item, false, true);
		else{							//Sell just Part
			item->setAmount(item->getAmount() - amount);
			player->send( PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvFrom),InvFrom));
		}

		if(InvFrom != 4)
			player->addMesos((amount*DataProvider::getInstance()->getItemPrice(itemid))*0.2);
		else
			player->addMesos((amount*DataProvider::getInstance()->getItemPrice(itemid)));

}
void PlayerHandler::moveItemHandle(PacketReader& packet){
	packet.readInt();//Read Counter
	//
	char InvFrom = packet.read();
	char SlotFrom = packet.read();
	char InvTo = packet.read();
	char SlotTo = packet.read();
	int  amount= packet.readShort();
	//
	Item* itemFrom = player->getInventories()->getItemBySlot(InvFrom, SlotFrom);
	Item* itemTO = player->getInventories()->getItemBySlot(InvTo, SlotTo);
	printf("Item Wan Move %d\n",itemFrom->getID());
	if(InvTo == 0x63 && SlotTo == 0x63){ // Drop
		if(InvFrom == 0) //Player Need UnEwuip The ITEM
			return;
		
		if(amount < itemFrom->getAmount() && amount != 0){
			Item* item = new Item(itemFrom->getID(), amount);
			itemFrom->setAmount(itemFrom->getAmount()-amount);
			player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvFrom),InvFrom));
			player->getMap()->getDrops()->dropFromObject(item, player->getPosition(), player,0, player);
			//player->send(PacketCreator().updateSlot(itemFrom, true)); //NEED CHANGE
		}
		else{ //Drop All
			player->getMap()->getDrops()->dropFromObject(itemFrom, player->getPosition(), player,0,player);
			player->getInventories()->getInventory(InvFrom)->removeItem(SlotFrom, true, false, false);
			player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvFrom),InvFrom));
			//player->send(PacketCreator().moveItem((inv1 == 0 || inv2 == 0) ? 1 : inv1, slot1, slot2, (inv1 == 0 || inv2 == 0)));
		}
	}
	if(InvFrom == InvTo) //Player Wan Swap Or Move Item
	{
		if(itemTO == NULL) //Need Move
		{
			itemFrom->setSlot(SlotTo);
			player->getInventories()->getInventory(InvFrom)->removeItem(SlotFrom, false, false, false);
			player->getInventories()->getInventory(InvFrom)->addItem(itemFrom, false, false, false);
			player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvFrom),InvFrom));
		}
	}
	if(InvFrom == 0 && (InvTo == 1 || InvTo == 2)) //Dealing Remving Equip
	{
		Equip* equipFrom  = (Equip*)player->getInventories()->getItemBySlot(InvFrom, SlotFrom);
		Equip* equipTO  = (Equip*)player->getInventories()->getItemBySlot(InvTo, SlotTo);
		if(equipTO == NULL){
			equipFrom->setSlot(SlotTo);
			player->getInventories()->getInventory(InvFrom)->removeItem(SlotFrom, false, false, false);
			player->getInventories()->getInventory(InvTo)->addItem(equipFrom, false, false, false);
			player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvFrom),InvFrom));
			player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvTo),InvTo));
			player->getMap()->send(PacketCreator().Player_SET_AVATAR(player));
		}
		player->FixStats();
		player->send(PacketCreator().Player_CHAR_ALL(player));

	}
	if((InvFrom == 1 || InvFrom == 2) && InvTo == 0) //Dealing Set Equip
	{
		Equip* equipFrom  = (Equip*)player->getInventories()->getItemBySlot(InvFrom, SlotFrom);
		Equip* equipTO  = (Equip*)player->getInventories()->getItemBySlot(InvTo, SlotTo);
		if(equipTO == NULL){
			equipFrom->setSlot(SlotTo);
			player->getInventories()->getInventory(InvFrom)->removeItem(SlotFrom, false, false, false);
			player->getInventories()->getInventory(InvTo)->addItem(equipFrom, false, false, false);
			player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvFrom),InvFrom));
			player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvTo),InvTo));
			player->getMap()->send(PacketCreator().Player_SET_AVATAR(player));
		}else{//Need Sowp
			int sl1 = equipFrom->getSlot();
			int sl2 = equipTO->getSlot();

			equipFrom->setSlot(sl2);
			equipTO->setSlot(sl1);
			player->getInventories()->getInventory(InvFrom)->removeItem(sl1, false, false, false);
			player->getInventories()->getInventory(InvTo)->removeItem(sl2, false, false, false);
			player->getInventories()->getInventory(InvTo)->addItem(equipFrom, false, false, false);
			player->getInventories()->getInventory(InvFrom)->addItem(equipTO, false, false, false);
			player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvFrom),InvFrom));
			player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvTo),InvTo));
			player->getMap()->send(PacketCreator().Player_SET_AVATAR(player));

		}
		player->FixStats();
		player->send(PacketCreator().Player_CHAR_ALL(player));

	}
	if(InvFrom == 5 && InvTo == 0) //Dealing Set Equip From Pet Slot
	{
		if(IS_PET(itemFrom->getID()) || IS_TOY(itemFrom->getID()))//Its Pet 
		{
			Pet* equipFrom = (Pet*)player->getInventories()->getItemBySlot(InvFrom, SlotFrom);
			Pet* equipTO  = (Pet*)player->getInventories()->getItemBySlot(InvTo, SlotTo);
			if(equipTO == NULL){//EquipIT
				equipFrom->setSlot(SlotTo);
				equipFrom->setPetSlot(SlotFrom);
				player->getInventories()->getInventory(InvFrom)->removeItem(SlotFrom, false, false, false);
				player->getInventories()->getInventory(InvTo)->addItem(equipFrom, false, false, false);
				player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvFrom),InvFrom));
				player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvTo),InvTo));
				player->getMap()->send(PacketCreator().Player_SET_AVATAR(player));
			}
			else{//Swap It

			}

		}
	}
	if(InvFrom == 5 && IS_PET_DECO(itemFrom->getID())){
		Pet* p =  (Pet*)player->getInventories()->getItemBySlot(0,10);
		p->setPetDeco(itemFrom->getID());
		player->getMap()->send(PacketCreator().Player_SET_AVATAR(player));
	}
	if(InvFrom == 0 && (InvTo == 5)) //Dealing Remving Pet Or Toy
	{
		Pet* equipFrom = (Pet*)player->getInventories()->getItemBySlot(InvFrom, SlotFrom);
		Pet* equipTO  = (Pet*)player->getInventories()->getItemBySlot(InvTo, SlotTo);
		if(equipTO == NULL){//Wean Its the SlotTo is -1
			equipFrom->setSlot(equipFrom->getPetSlot());
			player->getInventories()->getInventory(InvFrom)->removeItem(SlotFrom, false, false, false);
			player->getInventories()->getInventory(InvTo)->addItem(equipFrom, false, false, false);
			player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvFrom),InvFrom));
			player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(InvTo),InvTo));
			player->getMap()->send(PacketCreator().Player_SET_AVATAR(player));
		}
	}
	/*
	else{
		if(InvTo == NULL){ // move to empty pos
			item1->setSlot(slot2);
			player->getInventories()->getInventory(inv1)->removeItem(slot1, false, false, false);
			player->getInventories()->getInventory(inv2)->addItem(item1, false, false, false);
			player->send(PacketCreator().moveItem((inv1 == 0 || inv2 == 0) ? 1 : inv1, slot1, slot2, (inv1 == 0 || inv2 == 0)));
		}
		else if(item2->getID() != item1->getID() || inv1 <= 1 || inv2 <= 1 || IS_STAR(item1->getID()) || IS_STAR(item2->getID())){ // switch positions
			item1->setSlot(slot2);
			item2->setSlot(slot1);
			player->getInventories()->getInventory(inv1)->removeItem(slot1, false, false, false);
			player->getInventories()->getInventory(inv2)->removeItem(slot2, false, false, false);
			player->getInventories()->getInventory(inv2)->addItem(item1, false, false, false);
			player->getInventories()->getInventory(inv1)->addItem(item2, false, false, false);
			player->send(PacketCreator().moveItem((inv1 == 0 || inv2 == 0) ? 1 : inv1, slot1, slot2, (inv1 == 0 || inv2 == 0)));
		}
		else{ // merge
			if(item1->getAmount() + item2->getAmount() <= DataProvider::getInstance()->getItemMaxPerSlot(item1->getID())){ 
				item2->setAmount(item1->getAmount()+item2->getAmount());
				player->send(PacketCreator().moveItemMerge(inv2, slot1, slot2, item2->getAmount()));
				player->getInventories()->getInventory(inv1)->removeItem(slot1, true, true, false);
			}
			else{
				int change = DataProvider::getInstance()->getItemMaxPerSlot(item1->getID()) - item2->getAmount();
				item2->setAmount(DataProvider::getInstance()->getItemMaxPerSlot(item1->getID()));
				item1->setAmount(item1->getAmount()-change);
				player->send(PacketCreator().moveItemMergeTwo(inv1, slot1, item1->getAmount(), slot2, item2->getAmount()));
			}
		}
	}
	if(inv1 == 0 || inv2 == 0)
		player->getMap()->send(PacketCreator().updatePlayer(player), player);
		*/
}
void PlayerHandler::useItemHandle(PacketReader& packet){
	packet.readInt();
	char Inv = packet.read();
	short slot = packet.read();
	
	Item* item = player->getInventories()->getItemBySlot(Inv, slot);
	if(item == NULL ){
		//player->send(PacketCreator().enableAction());
		return;
	}
	player->getInventories()->removeItemBySlot(Inv, slot, 1,false);
	player->send(PacketCreator().INVEN_UPDATE(player->getInventories()->getInventory(Inv),Inv));
	Effect* effect = Effect::getEffect(item->getID());
	if(effect != NULL)
		effect->use(player);	
}
void PlayerHandler::useReturnScrollHandle(PacketReader& packet){
	packet.readInt();
	short slot = packet.readShort();
	int itemid = packet.readInt();
	Item* item = player->getInventories()->getItemBySlot(USE, slot);
	if(item == NULL || item->getID() != itemid || MAJOR_TYPE(itemid) != 203){
		player->send(PacketCreator().enableAction());
		return;
	}
	player->getInventories()->removeItemBySlot(USE, slot, 1);
	int mapid = DataProvider::getInstance()->getItemMoveTo(itemid);
	if(mapid == 999999999){
		mapid = DataProvider::getInstance()->getReturnMap(player->getMap()->getID());
	}
	Map* map = player->getChannel()->getMaps()->getMap(mapid);
	if(map != NULL)
		player->changeMap(map);
}
int getRandomStat(){
	return random(1,5)*((random(2) == 1) ? 1 : -1);
}
void PlayerHandler::useScrollHandle(PacketReader& packet){
	packet.readInt();
	short slot = packet.readShort();
	short eslot = packet.readShort();
	bool wscroll = packet.readShort() == 2;
	Item* scroll = player->getInventories()->getItemBySlot(USE, slot);
	Equip* equip = (Equip*)player->getInventories()->getItemBySlot(EQUIPPED, eslot);
	if(scroll == NULL || equip == NULL || MAJOR_TYPE(scroll->getID()) != 204 || equip->getSlots() == 0 || (wscroll && player->getInventories()->getItemByID(2340000) == NULL)){
		player->send(PacketCreator().enableAction());
		return;
	}
	if(wscroll){
		player->getInventories()->getInventory(USE)->removeItem(2340000, (int)1);
	}
	ItemData* item = DataProvider::getInstance()->getItemData(scroll->getID());
	if(item == NULL)
		return;
	bool success = false, cursed = false;
	ItemEffectData* effect = item->getEffectData();
	if(random(100) < effect->getSuccess()){
		success = true;
		equip->setStr(equip->getStr()+effect->getStr());
		equip->setDex(equip->getDex()+effect->getDex());
		equip->setInt(equip->getInt()+effect->getInt());
		equip->setLuk(equip->getLuk()+effect->getLuk());
		equip->setHP(equip->getHP()+effect->getHP());
		equip->setMP(equip->getMP()+effect->getMP());
		equip->setWAtk(equip->getWAtk()+effect->getWAtk());
		equip->setMAtk(equip->getMAtk()+effect->getMAtk());
		equip->setWDef(equip->getWDef()+effect->getWDef());
		equip->setMDef(equip->getMDef()+effect->getMDef());
		equip->setAcc(equip->getAcc()+effect->getAcc());
		equip->setAvo(equip->getAvo()+effect->getAvo());
		equip->setHand(equip->getHand()+effect->getHand());
		equip->setJump(equip->getJump()+effect->getJump());
		equip->setSpeed(equip->getSpeed()+effect->getSpeed());
		if(effect->getRecover()){
			equip->setSlots(equip->getSlots()+1);
		}
		else{
			equip->setScrolls(equip->getScrolls()+1);
		}
		if(effect->getRandStat()){
			if(equip->getStr() > 0)
				equip->setStr(equip->getStr()+getRandomStat());
			if(equip->getDex() > 0)
				equip->setDex(equip->getDex()+getRandomStat());
			if(equip->getInt() > 0)
				equip->setInt(equip->getInt()+getRandomStat());
			if(equip->getLuk() > 0)
				equip->setLuk(equip->getLuk()+getRandomStat());
			if(equip->getHP() > 0)
				equip->setHP(equip->getHP()+getRandomStat());
			if(equip->getMP() > 0)
				equip->setMP(equip->getMP()+getRandomStat());
			if(equip->getWAtk() > 0)
				equip->setWAtk(equip->getWAtk()+getRandomStat());
			if(equip->getStr() > 0)
				equip->setMAtk(equip->getMAtk()+getRandomStat());
			if(equip->getStr() > 0)
				equip->setWDef(equip->getWDef()+getRandomStat());
			if(equip->getMDef() > 0)
				equip->setMDef(equip->getMDef()+getRandomStat());
			if(equip->getAcc() > 0)
				equip->setAcc(equip->getAcc()+getRandomStat());
			if(equip->getAvo() > 0)
				equip->setAvo(equip->getAvo()+getRandomStat());
			if(equip->getHand() > 0)
				equip->setHand(equip->getHand()+getRandomStat());
			if(equip->getJump() > 0)
				equip->setJump(equip->getJump()+getRandomStat());
			if(equip->getSpeed() > 0)
				equip->setSpeed(equip->getSpeed()+getRandomStat());
		}
	}
	else{
		if(random(100) < effect->getCursed() || effect->getRandStat()){
			cursed = true;
		}
	}
	if(!(wscroll || effect->getRecover()))
		equip->setSlots(equip->getSlots()-1);
	player->getMap()->send(PacketCreator().useScroll(player->getID(), success, cursed));
	player->send(PacketCreator().scrollItem(scroll, equip, cursed));
	player->getInventories()->removeItemBySlot(USE, slot, 1, false);
	if(cursed){
		player->getMap()->send(PacketCreator().updatePlayer(player), player);
		player->getInventories()->removeItemBySlot(EQUIPPED, eslot, 1, false);
	}
}
void PlayerHandler::useSummonBugHandle(PacketReader& packet){
}

void PlayerHandler::cancelItemBuffHandle(PacketReader& packet){
	int effect = packet.readInt();
	player->getBuffs()->cancelBuff(effect);	
}

void PlayerHandler::autoArrangementHandle(PacketReader& packet){
	packet.show();
	packet.readInt();
	int inv = packet.read();
	hash_map <int, Item*>* hitems = player->getInventories()->getInventory(inv)->getItems();
	vector <Item*> items;
	for(hash_map<int, Item*>::iterator iter = hitems->begin(); iter != hitems->end(); iter++){
		items.push_back(iter->second);	
	}
	sort<vector <Item*>::iterator, CompareItems>(items.begin(),items.end(), CompareItems());
	//TODO: find the packets
	bool check = true;
	for(int i=0; i<(int)items.size(); i++)
		if(items[i]->getSlot() != i+1) check = false;
	if(check){ // TODO: stacking
	}
	else{
		for(int i=0; i<(int)items.size(); i++){	
			Item* item1 = items[i];
			Item* item2 = player->getInventories()->getItemBySlot(inv, i+1);
			if(item1 == item2) continue;
			short slot1 = items[i]->getSlot();
			short slot2 = i+1;
			if(item2 == NULL){
				item1->setSlot(slot2);
				player->getInventories()->getInventory(inv)->removeItem(slot1, false, false, false);
				player->getInventories()->getInventory(inv)->addItem(item1, false, false, false);
			}
			else {
				item1->setSlot(slot2);
				item2->setSlot(slot1);
				player->getInventories()->getInventory(inv)->removeItem(slot1, false, false, false);
				player->getInventories()->getInventory(inv)->removeItem(slot2, false, false, false);
				player->getInventories()->getInventory(inv)->addItem(item1, false, false, false);
				player->getInventories()->getInventory(inv)->addItem(item2, false, false, false);
			}
			player->send(PacketCreator().moveItem(inv, slot1, slot2, inv));
		}
	}
}