#pragma once
#include <vector>
#include "Player.h"
#include "PvP.h"
using namespace std;
class PvP;
class Map;
class Player;
class MapPVP
{
	private:
	Map* map;
	hash_map <int, PvP*> pvps;
public:
	MapPVP(Map* map){
		this->map = map;
	}
	void addPvP(PvP* pvp);
	void removePvP(PvP* pvp);
	PvP* getPvPByID(int id);
};
