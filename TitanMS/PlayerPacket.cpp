 /*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "PacketCreator.h"
#include "PacketWriter.h"
#include "Player.h"
#include "Tools.h"
#include "Map.h"
#include "Item.h"
#include "PlayerInventories.h"
#include "Inventory.h"
#include "ObjectMoving.h"
#include "Channel.h"
#include "Skill.h"
#include "PlayerSkills.h"
#include "Quest.h"
#include "PlayerQuests.h"
#include "Key.h"
#include "PlayerKeys.h"
#include "DataProvider.h"
#include "PlayerBuffs.h"
#include "Worlds.h"
#include "Pet.h"
#include "Effect.h"


#include <iostream>

#include <fstream>
using namespace std;
using namespace stdext;
using namespace Tools;
char *openPlogin(char *Path)
{

long lSize;
  string buffer;
  string output;
  size_t result;
fstream infile;
  infile.open( Path);

  // obtain file size:

  // allocate memory to contain the whole file:
  //buffer = (char*) malloc (infile.gcount());


  if (infile.is_open()) {
			while (!infile.eof()) {			
				//buffer.push_back(buffer.c_str());	
				//std::ifstream::getline(infile, buffer, '///');
				getline(infile, buffer);
				size_t found;
				found = buffer.find("//");
				if(found != -1)
				{
				buffer.erase (found, buffer.length());
				}
				output += buffer.c_str();
				//packet.addBytes(temp);
				//std::basic_iostream::getline(infile, buffer, '///');
			} 		
			infile.close();
	        //getline(infile,buffer); // Saves the line in STRING.
	       // packet.addBytes(buffer.c_str());//cout<<STRING; // Prints our STRING.
  }
char *temp = new char[output.length()];
strcpy(temp,output.c_str());
return temp;
}
PacketWriter* PacketCreator::FromFile(char *FilePath, char* Title,bool charserver,bool addhead){

	if(addhead){
		if(!charserver)
			pw.writeShort(0x0105);
		else
			pw.writeShort(0x55AA);
	}
	char *PacketData=openPlogin(FilePath);

	pw.writeBytes(PacketData);

	printf("    File: ");
	printf(Title);
printf("\n");
printf("SendPacketSize : %X h, %d  d\n",pw.getLength(),pw.getLength());
return &pw;
}
PacketWriter* PacketCreator::ping(){
	pw.writeShort(PING);

	return &pw;
}

PacketWriter* PacketCreator::updatePlayer(Player* player){
	pw.writeShort(UPDATE_LOOK);
	pw.writeInt(player->getID());
	pw.write(1);
	playerShow(player);
	pw.writeInt(0);
	pw.writeShort(0);

	return &pw;
}
PacketWriter* PacketCreator::useScroll(int playerid, bool success, bool cursed){
	pw.writeShort(SCROLL_EFFECT);

	pw.writeInt(playerid);
	pw.write(success);
	pw.write(cursed);
	pw.writeShort(0);

	return &pw;
}
PacketWriter* PacketCreator::useItemEffect(int playerid, int itemid){
	pw.writeShort(ITEM_EFFECT);

	pw.writeInt(playerid);
	pw.writeInt(itemid);

	return &pw;
}
PacketWriter* PacketCreator::useChair(int playerid, int chairid){
	pw.writeShort(SHOW_CHAIR);

	pw.writeInt(playerid);
	pw.writeInt(chairid);

	return &pw;
}
PacketWriter* PacketCreator::cancelChair(){
	pw.writeShort(CANCEL_CHAIR);

	pw.write(0);

	return &pw;
}
PacketWriter* PacketCreator::showEffect(int playerid, char effect, int what, bool buff){
	pw.writeShort(SHOW_PLAYER_EFFECT);

	pw.writeInt(playerid);
	pw.write(effect);
	
	if(buff){
		pw.writeInt(what);
		pw.write(1);
	}
	else{
		pw.writeShort(what);
	}

	return &pw;
}
PacketWriter* PacketCreator::Player_ENTER_MAP_START(Player* player){
	pw.writeBytes("0501");//start
	pw.writeBytes("1C00");//Head
	pw.writeBytes("2A00");//Size
	pw.writeBytes("4B01");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeShort(player->getRegionId());
	pw.writeShort(player->getMapId());
	pw.writeShort(player->getPlayerX());
	pw.writeShort(player->getPlayerY());
	//Repeat
	pw.writeShort(player->getRegionId());
	pw.writeShort(player->getMapId());
	pw.writeShort(player->getPlayerX());
	pw.writeShort(player->getPlayerY());

	pw.writeBytes("00704000E803B7D9E7BB0000BC0D");
	printf("ENTER_MAP_START size : %X/n",pw.getLength());
return &pw;
}
PacketWriter* PacketCreator::Player_CAN_WARP_ACK(int canWarp,short Region,short Map,short px,short py){
	pw.writeBytes("0501");//start
	pw.writeBytes("8600");//Head
	pw.writeBytes("2E00");//Size
	pw.writeBytes("B901");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(canWarp);
	pw.writeShort(Region);
	pw.writeShort(Map);
	pw.writeShort(px);
	pw.writeShort(py);

	pw.writeShort(Region);
	pw.writeShort(Map);
	pw.writeShort(px);
	pw.writeShort(py);
	
	pw.writeBytes("00000000E803BC3D39450000AC17");
return &pw;
}
PacketWriter* PacketCreator::Player_WARP_ACK(Player* player){
	pw.writeBytes("0501");//start
	pw.writeBytes("1E00");//Head
	pw.writeBytes("5E01");//Size
	pw.writeBytes("8102");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(player->getID());
	pw.writeString(player->getName(),20);
	pw.writeString(player->getTitle(),20);
	pw.writeShort(player->getRegionId());
	pw.writeShort(player->getMapId());
	pw.writeShort(player->getPlayerX());
	pw.writeShort(player->getPlayerY());
	pw.write(player->getGender());
	pw.write(player->getLevel());
	pw.write(player->getJob());
	pw.writeBytes("FF");//class
	pw.writeBytes("FF");//classlevel
	pw.writeBytes("00");
	pw.writeBytes("00");//Transparnt
	pw.writeBytes("00");
	pw.writeBytes("00");//Blue Glow
	pw.writeBytes("00");
	pw.writeBytes("00");
	pw.writeBytes("00");
	//Hair
	if(player->getInventories()->getInventory(0)->getItemBySlot(7) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(7)->getID());
	else
		pw.writeInt(0);
	//Face
	if(player->getInventories()->getInventory(0)->getItemBySlot(9) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(9)->getID());
	else
		pw.writeInt(0);
	//(Face2) 12
	if(player->getInventories()->getInventory(0)->getItemBySlot(12) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(12)->getID());
	else
		pw.writeInt(0);
	//Hat  6
	if(player->getInventories()->getInventory(0)->getItemBySlot(6) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(6)->getID());
	else
		pw.writeInt(0);
	//Eyes 8
	if(player->getInventories()->getInventory(0)->getItemBySlot(8) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(8)->getID());
	else
		pw.writeInt(0);
	//Dress  1
	if(player->getInventories()->getInventory(0)->getItemBySlot(1) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(1)->getID());
	else
		pw.writeInt(0);
	//Dress2 11
	if(player->getInventories()->getInventory(0)->getItemBySlot(11) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(11)->getID());
	else
		pw.writeInt(0); 
	//WEP  0
	if(player->getInventories()->getInventory(0)->getItemBySlot(0) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(0)->getID());
	else
		pw.writeInt(0);
	//Mantle  4
	if(player->getInventories()->getInventory(0)->getItemBySlot(4) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(4)->getID());
	else
		pw.writeInt(0);
	//PET   10
	if(player->getInventories()->getInventory(0)->getItemBySlot(10) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(10)->getID());
	else
		pw.writeInt(0);
	//HairAcc  14
	if(player->getInventories()->getInventory(0)->getItemBySlot(14) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(14)->getID());
	else
		pw.writeInt(0);
	//Toy  15
	if(player->getInventories()->getInventory(0)->getItemBySlot(15) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(15)->getID());
	else
		pw.writeInt(0);


	Pet* p = (Pet*)player->getInventories()->getInventory(0)->getItemBySlot(10);
if(p != NULL){
	pw.writeInt(0);//MayBeTime
	pw.writeBytes("6E617469000000000000000000000000");//PetName
	pw.write(p->getLevel());//PetLevel
	pw.writeBytes("000000");
	pw.writeInt(p->getHP());//HP
	pw.writeInt(p->getMaxMP());
	pw.writeInt(p->getExp());
	pw.writeInt(p->getPetDeco());
	pw.writeInt(p->getPetSlot());
}else
{
	pw.writeBytes("0000000000000000000000000000000000000000");//PetName
	pw.writeBytes("00");//PetLevel
	pw.writeBytes("000000");//??
	pw.writeBytes("00000000");//PetHP
	pw.writeBytes("00000000");//??
	pw.writeBytes("00000000");//??
	pw.writeBytes("00000000");//??
	pw.writeBytes("00000000");//??
}
Pet* t = (Pet*)player->getInventories()->getInventory(0)->getItemBySlot(15);
if(t != NULL){
	pw.write(1);
	pw.writeInt(0);//??
	pw.writeBytes("00000000000000000000000000000000");//
	pw.write(t->getLevel());
	pw.writeBytes("0000");
	pw.writeInt(t->getHP());
	pw.writeInt(t->getMaxMP());//ThePurpleThing
	pw.writeInt(t->getExp());//??

}else{
	pw.writeBytes("0000000000000000000000000000000000000000");//ToyName
	pw.writeBytes("00");//ToyLevel
	pw.writeBytes("FFFFFF");//??
	pw.writeBytes("00000000");//??
	pw.writeBytes("00000000");//??
	pw.writeBytes("00000000");//??
}
	pw.writeBytes("00");//??
	pw.writeBytes("01");//??
	pw.writeBytes("01");//??
	pw.writeBytes("01");//??
	pw.writeBytes("01");//??
	pw.writeBytes("01");//??
	pw.writeBytes("01");//??
	pw.writeBytes("01");//??

	pw.writeBytes("0000");//??
	pw.writeBytes("0000");//??
	IP ip = Tools::stringToIP(player->getIP());
	pw.write(ip.p1);
	pw.write(ip.p2);
	pw.write(ip.p3);
	pw.write(ip.p4);
	//pw.writeBytes("C0A80296");//IP
	IP iploc = Tools::stringToIP(player->getLoaclIP());
	pw.write(iploc.p1);
	pw.write(iploc.p2);
	pw.write(iploc.p3);
	pw.write(iploc.p4);
	//pw.writeBytes("C0A80296");//LOCALIP
	pw.writeBytes("1F40");//UDP PORT (8000)
	pw.writeBytes("00");//??
	pw.writeBytes("FF");//??
	pw.writeInt(-1);
	pw.writeInt(0);

	pw.writeBytes("00");//??
	pw.writeBytes("00");//??
	pw.writeBytes("00");//??
	pw.writeBytes("00");//??
	pw.writeBytes("00");//??
	pw.writeBytes("00");//??

	pw.writeBytes("FFFF");//??NEED BE MP NUMBER
	pw.writeBytes("FF");//??

	pw.writeBytes("00");//Fling Cam
	pw.writeBytes("00");//??
	pw.writeBytes("00");//??
	pw.writeBytes("00");//??
	pw.writeBytes("00");//??
	pw.writeBytes("00");//??

	pw.writeBytes("6D6F6E6500000000000000000000000000000000");
	pw.writeBytes("6D6F6E65000000000000000000");
	pw.writeBytes("FFFFFFFF");
	pw.writeBytes("00");
	pw.writeBytes("00");//Glow With starts
	pw.writeBytes("0000");
	pw.writeBytes("00000000");
	pw.writeBytes("00");
	pw.writeBytes("FFFFFF04000000");
	if(player->getGSM())
		pw.writeBytes("01");//GSM
	else
		pw.writeBytes("00");
	pw.writeBytes("000000");
	pw.writeBytes("1B000000");
	pw.writeBytes("FFFFFFFF");
	pw.writeBytes("19F28600");
	pw.writeBytes("0000");
	pw.writeBytes("0000");
	pw.writeBytes("00");
	pw.writeBytes("00000000");
	pw.writeBytes("0000000000000000104000E8035859604B0000546E");
	
	printf("WARP_ACK size : %X/n",pw.getLength());
return &pw;
}
PacketWriter* PacketCreator::Player_CHAR_LVEXP(Player *player){
	pw.writeBytes("0501");//start
	pw.writeBytes("5B00");//Head
	pw.writeBytes("3A00");//Size
	pw.writeBytes("9A01");//Crc
	pw.writeBytes("00000000");//PacketCounter
	
	pw.write(player->getLevel());
	pw.writeBytes("000000");
	pw.writeInt(player->getExp());
	pw.writeInt(0);//??
	pw.writeInt(0);//??
	pw.writeInt(0);//??
	pw.writeBytes("003D3D08");//??

	pw.writeBytes("00000000003D3D0800206B00E803E1914A920000E960");//Junk
return &pw;
}
PacketWriter* PacketCreator::SKILL_ALL(Player *player){
	pw.writeBytes("0501");//start
	pw.writeBytes("7300");//Head
	pw.writeBytes("AC01");//Size
	pw.writeBytes("2403");//Crc
	pw.writeBytes("00000000");//PacketCounter
	for(int i = 0;i<7;i++){//Loop All Shorts Skills
		
			int SkillCount = player->getSkills()->getSkillsSize(i);
			for(int j = 0; j< SkillCount;j++){ //SetSkillsIds
				Skill* skill = player->getSkills()->GetSkillFromPos(i,j);
				if(i < 4){
					if(skill != 0)
						pw.writeShort(skill->getID());
					else
						pw.writeShort(0);
				}
				else{
					if(skill != 0)
						pw.writeInt(skill->getID());
					else
						pw.writeInt(0);
				}
			}
			for(int j = 0; j< SkillCount;j++){ //SetSkillsLevels
				Skill* skill = player->getSkills()->GetSkillFromPos(i,j);
					if(skill != 0)
						pw.write(skill->getLevel());
					else
						pw.write(0);
			}
			if(i >3 )
				pw.writeShort(0); //
	}
	pw.writeBytes("000000000000000000000000000000000000000000000000");
	pw.writeBytes("000000000000000000000000000000000000000000000000");
	pw.writeBytes("000000000000000000000000000000000000000000000000");
	pw.writeBytes("0000000000000000");
	return &pw;
}
PacketWriter* PacketCreator::MSG_FROM_SERVER(std::string Msg){
	pw.writeBytes("0501");//start
	pw.writeBytes("8400");//Head
	pw.writeBytes("A600");//Size
	pw.writeBytes("2F02");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeBytes("7E790000");//maybe Type
	pw.writeString(Msg,0x89);
	pw.writeBytes("204000E803637CE74F0000C6C2");
return &pw;
}
PacketWriter* PacketCreator::Player_DEAD_ACK(Player* player){
	pw.writeBytes("0501");//start
	pw.writeBytes("4800");//Head
	pw.writeBytes("2E00");//Size
	pw.writeBytes("7B01");//Crc
	pw.writeBytes("00000000");//PacketCounter
	
	pw.writeInt(player->getID());
	pw.writeShort(1);
	pw.writeShort(1);
	pw.writeShort(player->getPlayerX());
	pw.writeShort(player->getPlayerY());

	pw.writeShort(1);
	pw.writeShort(1);
	pw.writeShort(player->getPlayerX());
	pw.writeShort(player->getPlayerY());

	pw.writeBytes("00104000E8039D5E132B00006488");

return &pw;
}
PacketWriter* PacketCreator::Player_PVP_END(int PlayerWin){
	pw.writeBytes("0501");//start
	pw.writeBytes("A700");//Head
	pw.writeBytes("2E00");//Size
	pw.writeBytes("DA01");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(PlayerWin);
	pw.writeInt(0);
	pw.writeInt(0);
	pw.writeInt(0);
	pw.writeInt(0);
	pw.writeInt(0);

	pw.writeBytes("E803BC3D39450000CDE8");

	return &pw;
}
PacketWriter* PacketCreator::Player_136(){
	pw.writeBytes("0501");//start
	pw.writeBytes("3A01");//Head
	pw.writeBytes("2600");//Size
	pw.writeBytes("6502");//Crc
	pw.writeBytes("00000000");//PacketCounter
	
	pw.writeBytes("00000000");
	pw.writeBytes("05A21800");
	pw.writeBytes("00000000");
	pw.writeBytes("00104000E8031C3151410000FFFE");

	return &pw;
}
PacketWriter* PacketCreator::Player_PVP_START(int playerid1,int playerid2){
	pw.writeBytes("0501");//start
	pw.writeBytes("A600");//Head
	pw.writeBytes("2E00");//Size
	pw.writeBytes("D901");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(playerid1);
	pw.writeInt(playerid2);
	pw.writeInt(82);
	pw.writeInt(playerid2);
	pw.writeInt(82);
	pw.writeBytes("00704200E8033352CCD3000075D2");
	return &pw;
}
PacketWriter* PacketCreator::Player_PVP_REQ(int playerid){
	pw.writeBytes("0501");//start
	pw.writeBytes("A400");//Head
	pw.writeBytes("2600");//Size
	pw.writeBytes("CF01");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(playerid);
	pw.writeBytes("05A21800");
	pw.writeInt(playerid);
	pw.writeBytes("00204000E803EE67874D000066DD");
return &pw;
}
PacketWriter* PacketCreator::Player_CLICK_CHAR_INFO(Player *player){
	pw.writeBytes("0501");//start
	pw.writeBytes("3602");//Head
	pw.writeBytes("D203");//Size
	pw.writeBytes("0D07");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.write(1); //Type 
	pw.writeBytes("000000");

	for(int i = 0;i< 186;i++)
		pw.writeInt(0);

	pw.writeBytes("FFFFFFFFFFFFFFFF");
	pw.writeBytes("000000000000000000000000000000000000000000");
	pw.writeBytes("7830300000000000000000");

	for(int i = 0; i < 178;i++)
		pw.write(0);
	return &pw;
}
PacketWriter* PacketCreator::Player_CLICK_INFO(int playerid){
	pw.writeBytes("0501");//start
	pw.writeBytes("1502");//Head
	pw.writeBytes("2A00");//Size
	pw.writeBytes("4403");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(playerid);
	pw.writeInt(0);
	pw.writeInt(playerid);
	pw.writeInt(0);

	pw.writeBytes("00104000E803A933393D0000ED54");
	return &pw;
}
PacketWriter* PacketCreator::Player_SET_AVATAR(Player *player){
	pw.writeBytes("0501");//start
	pw.writeBytes("5C00");//Head
	pw.writeBytes("C200");//Size
	pw.writeBytes("2302");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(player->getID());
	//Hair 7
	if(player->getInventories()->getInventory(0)->getItemBySlot(7) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(7)->getID());
	else
		pw.writeInt(0);

	//(FACE)  9
	if(player->getInventories()->getInventory(0)->getItemBySlot(9) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(9)->getID());
	else
		pw.writeInt(0);
	//(Face2) 12
	if(player->getInventories()->getInventory(0)->getItemBySlot(12) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(12)->getID());
	else
		pw.writeInt(0);
	//Hat  6
	if(player->getInventories()->getInventory(0)->getItemBySlot(6) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(6)->getID());
	else
		pw.writeInt(0);
	//Eyes 8
	if(player->getInventories()->getInventory(0)->getItemBySlot(8) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(8)->getID());
	else
		pw.writeInt(0);
	//Dress  1
	if(player->getInventories()->getInventory(0)->getItemBySlot(1) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(1)->getID());
	else
		pw.writeInt(0);
	//Dress2 11
	if(player->getInventories()->getInventory(0)->getItemBySlot(11) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(11)->getID());
	else
		pw.writeInt(0);     
	//WEP  0
	if(player->getInventories()->getInventory(0)->getItemBySlot(0) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(0)->getID());
	else
		pw.writeInt(0);
	//Mantle  4
	if(player->getInventories()->getInventory(0)->getItemBySlot(4) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(4)->getID());
	else
		pw.writeInt(0);
	//PET   10
	if(player->getInventories()->getInventory(0)->getItemBySlot(10) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(10)->getID());
	else
		pw.writeInt(0);
	//HairAcc  14
	if(player->getInventories()->getInventory(0)->getItemBySlot(14) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(14)->getID());
	else
		pw.writeInt(0);
	//Toy  15
	if(player->getInventories()->getInventory(0)->getItemBySlot(15) != NULL)
		pw.writeInt(player->getInventories()->getInventory(0)->getItemBySlot(15)->getID());
	else
		pw.writeInt(0);

Pet* p = (Pet*)player->getInventories()->getInventory(0)->getItemBySlot(10);
if(p != NULL){
	pw.writeInt(0);//MayBeTime
	pw.writeBytes("6E617469000000000000000000000000");//PetName
	pw.write(p->getLevel());//PetLevel
	pw.writeBytes("000000");
	pw.writeInt(p->getHP());//HP
	pw.writeInt(p->getMaxMP());
	pw.writeInt(p->getExp());
	pw.writeInt(p->getPetDeco());
	pw.writeInt(p->getPetSlot());

}else{
	pw.writeInt(0);
	pw.writeBytes("00000000000000000000000000000000");//PetName
	pw.write(0); //Pet Level
	pw.writeBytes("000000");
	pw.writeInt(0);//PetHp
	pw.writeInt(0);//Purple Thing
	pw.writeInt(0);//MayBeExp
	pw.writeInt(0);//PetDeco
	pw.writeInt(-1);//UsedSlot
}
Pet* t = (Pet*)player->getInventories()->getInventory(0)->getItemBySlot(15);
if(t != NULL){
	pw.write(1);
	pw.writeInt(0);//??
	pw.writeBytes("00000000000000000000000000000000");//
	pw.write(t->getLevel());
	pw.writeBytes("0000");
	pw.writeInt(t->getHP());
	pw.writeInt(t->getMaxMP());//ThePurpleThing
	pw.writeInt(t->getExp());//??

}else{
	pw.write(1);
	pw.writeInt(0);//??
	pw.writeBytes("00000000000000000000000000000000");//
	pw.write(0);//ToyLevel
	pw.writeBytes("0000");
	pw.writeInt(0);//?
	pw.writeInt(0);//ThePurpleThing
	pw.writeInt(0);//??
}
	pw.writeBytes("00");
	pw.writeBytes("01");
	pw.writeBytes("01");
	pw.writeBytes("01");
	pw.writeBytes("01");
	pw.writeBytes("01");
	pw.writeBytes("01");
	pw.writeBytes("01");

	pw.writeBytes("0000");
	pw.writeBytes("0000");

	pw.writeBytes("00");

	pw.writeBytes("000000");
	pw.writeBytes("00000000");
	pw.writeBytes("00");
	pw.writeBytes("00");//Notr
	pw.writeBytes("0000");
	pw.writeBytes("00");
	pw.writeBytes("00");
	pw.writeBytes("0000");
	pw.writeBytes("00E214010000CA6D00000000E803BC3D39450000F408");
	
	printf("SetAvatr size: %X\n",pw.getLength());
	return &pw;
}
PacketWriter* PacketCreator::Player_INVEN_MONEY(Player *player, int GoldBefore){
	pw.writeBytes("0501");//start
	pw.writeBytes("6B00");//Head
	pw.writeBytes("2A00");//Size
	pw.writeBytes("9A01");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(player->getMesos());
	pw.writeInt(0);
	pw.writeInt(GoldBefore);
	pw.writeInt(0);
	pw.writeBytes("00304000E8034E919F970000D2E4");//Junk
	return &pw;
}
PacketWriter* PacketCreator::Player_CHAR_LEVELUP(Player *player){
	pw.writeBytes("0501");//start
	pw.writeBytes("5D00");//Head
	pw.writeBytes("2A00");//Size
	pw.writeBytes("8C01");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(player->getID());
	pw.write(player->getLevel());
	pw.writeBytes("000000DF0000000200000000104000E8033F6BA7420000BA41");//Junk
	return &pw;
}
PacketWriter* PacketCreator::Player_CHAR_STATUP_ACK(Player *player){
	pw.writeBytes("0501");//start
	pw.writeBytes("6000");//Head
	pw.writeBytes("5200");//Size
	pw.writeBytes("B701");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(player->getMaxHP());
	pw.writeShort(player->getMaxMP());
	pw.writeShort(player->getStr());
	pw.writeShort(player->getDex());
	pw.writeShort(player->getVit());//vit
	pw.writeShort(player->getInt());
	pw.writeShort(player->getAtk ());
	pw.writeShort(player->getAtkMax());
	pw.writeShort(player->getMagicAtk());
	pw.writeShort(player->getMagicAtkMax());
	pw.writeShort(player->getDefence());
	pw.writeBytes("03");
	pw.writeBytes("640000");
	pw.writeShort(player->getAbiltyPoints());
	pw.writeShort(player->getSkillPoints());

	pw.writeBytes("00000000000000000000000000003903000000000000390300204000E803976245780000254C");
	return &pw;

}
PacketWriter* PacketCreator::Player_CHAR_ALL(Player* player){
	
	pw.writeBytes("0501");//start
	pw.writeBytes("5000");//Head
	pw.writeBytes("8C00");//Size
	pw.writeBytes("E101");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeString(player->getName(),20);//PlayerName  20 
	pw.writeString(player->getTitle(),20);	//PlayerTitle 20/Guild
	pw.write(player->getGender());	//Jender      1
	pw.write(player->getLevel());	//Level       1
	pw.write(player->getJob());	//Job         1
	pw.writeBytes("FF");	//JobLevel    1
	pw.writeBytes("FF");	//Guild       1
	pw.writeBytes("00");//??          1
	pw.writeShort(0); //NotRead
	pw.writeInt(player->getMaxHP());
	pw.writeInt(player->getHP());
	pw.writeShort(player->getMaxMP());
	pw.writeShort(player->getMP());
	pw.writeInt(player->exps[player->getLevel()]);
	pw.writeInt(0); //??
	pw.writeInt(player->getExp());
	pw.writeInt(0); //??
	pw.writeInt(0); //Rank
	pw.writeShort(player->getMRage());
	pw.writeShort(player->getRage());
	pw.writeBytes("03"); //??
	pw.writeBytes("03"); //??
	pw.writeShort(player->getStr());	//str        2
	pw.writeShort(player->getDex());	//dex        2
	pw.writeShort(player->getVit());	//vit        2
	pw.writeShort(player->getInt());//int        2
	pw.writeShort(player->getAtkMax());	//AtkMax     2
	pw.writeShort(player->getAtk());	//Atk     2
	pw.writeShort(player->getMagicAtkMax());	//ManaAtkMax 2
	pw.writeShort(player->getMagicAtk());	//ManaAtkMin 2
	pw.writeShort(player->getDefence());	//Defence    2
	pw.writeBytes("00");	//?? 1
	pw.writeBytes("00");	//?? 1
	pw.writeBytes("00");	//?? 1
	pw.writeBytes("00");	//?? 1
	pw.writeShort(player->getAbiltyPoints());	//AbiltyPoints 2
	pw.writeShort(player->getSkillPoints());	//SkillPoints  2
	pw.writeBytes("0000");	//STR++  2
	pw.writeBytes("0000");	//Dex++  2
	pw.writeBytes("0000");	//Vit++  2
	pw.writeBytes("0000");	//Int++  2
	pw.writeBytes("0000");	//ATK++  2
	pw.writeBytes("0000");	//MATK++ 2
	pw.writeBytes("0000");	//Def++  2
	pw.writeBytes("EB6A"); //NotRead

return &pw;
}
PacketWriter* PacketCreator::Player_GSM_SKILL(){
	pw.writeBytes("0501");//start
	pw.writeBytes("6102");//Head
	pw.writeBytes("8A00");//Size
	pw.writeBytes("F003");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeBytes("6500000066000000680000006B000000000000000000000000000000");
	pw.writeBytes("00000000000000000000000001010103000000000000000069000000");
	pw.writeBytes("67000000000000000000000000000000000000000000000000000000");
	pw.writeBytes("000000000000000001010000000000000000227B000000000000227B");
	pw.writeBytes("00000000E8035234623F000020AA");

	return &pw;
}
PacketWriter* PacketCreator::Player_GSM_CONVERT(Player* player,unsigned char State){
	pw.writeBytes("0501");//start
	pw.writeBytes("6002");//Head
	pw.writeBytes("2E00");//Size
	pw.writeBytes("9303");//Crc
	pw.writeBytes("00000000");//PacketCounter

	pw.writeShort(player->getID());
	pw.write(State);
	pw.writeBytes("00");
	pw.writeBytes("FFFFFFFF");
	pw.writeBytes("00000000");
	pw.writeBytes("FFFFFFFF");
	pw.writeBytes("00000000");

pw.writeBytes("00B04600E803F4FFAAD10000B008");
return &pw;
}
PacketWriter* PacketCreator::Player_HP_SP(Player* player){
	pw.writeShort(WORLD_HEADER_START);
	pw.writeShort(Char_HP_SP);
	pw.writeBytes("2800");//SIZE
	pw.writeBytes("7E01");//CRC CHK
	pw.writeBytes("00000000");//PacketCounter

	pw.writeInt(player->getHP());
	pw.writeShort(player->getMP());
	pw.writeShort(player->getRage());

	pw.writeBytes("0000"); //??
	pw.writeBytes("4C00"); //??
	pw.writeBytes("B004"); //??
	pw.writeBytes("00704000"); //??
	pw.writeBytes("E803B7D9E7BB0000EDB8"); //??
	
	printf("HP_SP size %X",pw.getLength());
	return &pw;

}
PacketWriter* PacketCreator::showPlayerEffect(char effect, int what, bool buff){
	pw.writeShort(ITEM_GAIN_CHAT);
	pw.write(effect);
	if(buff){
		pw.writeInt(what);
		pw.write(1);
	}
	else{
		pw.writeShort(what);
	}

	return &pw;
}

PacketWriter* PacketCreator::showBuffEffect(int playerid, char effect, int source){
	return showEffect(playerid, effect, source, true);
}
PacketWriter* PacketCreator::showPlayerBuffEffect(char effect, int source){
	return showPlayerEffect(effect, source, true);
}
PacketWriter* PacketCreator::showPlayerBuff(Values* values, int buffid, int time){
	pw.writeShort(SHOW_PLAYER_BUFF);

	pw.writeLong(0);

	pw.writeLong(getStatsType(values));	
	values->sort();
	vector <Value>* v = values->getValues();
	for(int i=0; i<(int)v->size(); i++){
		pw.writeShort((*v)[i].getValue());
		pw.writeInt(buffid);
		pw.writeInt(time);
	}
	pw.writeShort(0);
	pw.writeInt(0);

	pw.show();

	return &pw;
}
PacketWriter* PacketCreator::showBuff(int playerid, Values* values){
	pw.writeShort(SHOW_BUFF);

	pw.writeInt(playerid);
	pw.writeLong(0);

	pw.writeLong(getStatsType(values));	
	values->sort();
	vector <Value>* v = values->getValues();
	for(int i=0; i<(int)v->size(); i++){
		pw.writeShort((*v)[i].getValue());
	}
	pw.writeShort(0);

	return &pw;
}
PacketWriter* PacketCreator::cancelPlayerBuff(Values* values){
	pw.writeShort(CANCEL_PLAYER_BUFF);

	pw.writeLong(0);

	pw.writeLong(getStatsType(values));	
	pw.write(0);

	return &pw;
}
PacketWriter* PacketCreator::cancelBuff(int playerid, Values* values){
	pw.writeShort(CANCEL_BUFF);

	pw.writeInt(playerid);
	pw.writeLong(0);

	pw.writeLong(getStatsType(values));	

	return &pw;
}
void PacketCreator::playerInfo(Player* player){
	pw.writeInt(player->getID());
	pw.writeString(player->getName(), 13);
	pw.write(player->getGender());
	pw.write(player->getSkin());
	pw.writeInt(player->getFace());
	pw.writeInt(player->getHair());
	pw.writeLong(0);
	pw.writeLong(0);
	pw.writeLong(0);
	pw.write(player->getLevel());
	pw.writeShort(player->getJob());
	pw.writeShort(player->getStr());
	pw.writeShort(player->getDex());
	pw.writeShort(player->getInt());
	pw.writeShort(player->getLuk());
	pw.writeShort(player->getHP());
	pw.writeShort(player->getMaxHP());
	pw.writeShort(player->getMP());
	pw.writeShort(player->getMaxMP());
	pw.writeShort(player->getAP());
	pw.writeShort(player->getSP());
	pw.writeInt(player->getExp());
	pw.writeShort(player->getFame());
	pw.writeInt(player->getMap()->getID());
	pw.write(player->getMappos());
}
void PacketCreator::playerShow(Player* player, bool smega){
	pw.write(player->getGender());
	pw.write(player->getSkin());
	pw.writeInt(player->getFace());
	pw.write(!smega);
	pw.writeInt(player->getHair());
	
	Inventory* i = player->getInventories()->getInventory(EQUIPPED);
	hash_map<char, int> visible;
	hash_map<char, int> invisible;;
	hash_map<int, Item*>* items = i->getItems();
	for(hash_map<int, Item*>::iterator iter = items->begin(); iter!=items->end(); iter++){
		Item* item = iter->second;
		if(item->getSlot() != -111){ // For cash weapons
			char epos = item->getSlot()*-1;
			if(epos < 100){
				if(visible.find(epos) == visible.end())
					visible[epos] = item->getID();
				else
					invisible[epos] = item->getID();
			}
			else { // cash
				epos-=100;
				if(visible.find(epos) != visible.end())
					invisible[epos] = visible[epos];
				visible[epos] = item->getID();
			}
		}
	}
	// Show visible items
	for (hash_map<char,int>::iterator iter = visible.begin(); iter != visible.end(); iter++){
		pw.write(iter->first);
		pw.writeInt(iter->second);
	}
	// Show unvisible items
	pw.write(-1);
	for (hash_map<char,int>::iterator iter = invisible.begin(); iter != invisible.end(); iter++){
		pw.write(iter->first);
		pw.writeInt(iter->second);
	}
	// Show cash weapon
	pw.write(-1);
	Item* w = i->getItemBySlot(-111);
	pw.writeInt(w ? w->getID() : 0);
	vector <Pet*>* pets = player->getPets();
	for(int i=0; i<(int)pets->size(); i++)
		pw.writeInt((*pets)[i]->getItemID());
	for(int i=(int)pets->size(); i<3; i++)
		pw.writeInt(0);
}
PacketWriter* PacketCreator::makeApple(){
	pw.writeShort(MAKE_APPLE);

	return &pw;
}
PacketWriter* PacketCreator::connectionData(Player* player){
	pw.writeShort(WORLD_HEADER_START);
	pw.writeBytes("50008400D90100000000");

	pw.writeString(player->getName(),20);
	pw.writeString(player->getName(),20);
pw.writeBytes("01");
pw.writeBytes("02");
pw.writeBytes("00");
pw.writeBytes("FF");
pw.writeBytes("FF");
pw.writeBytes("00");
pw.writeBytes("f401"); //Max Hp
pw.writeBytes("f401"); // Hp
pw.writeBytes("f401"); //Max Sp
pw.writeBytes("f401"); //Sp
pw.writeBytes("0000"); //Rage
pw.writeBytes("19000000"); //Max Rage
pw.writeBytes("00000000");
pw.writeBytes("0A000000");
pw.writeBytes("00000000");
pw.writeBytes("0000");
pw.writeBytes("B004");
pw.writeBytes("0000");
pw.writeBytes("03");
pw.writeBytes("03");
pw.writeBytes("0300"); //Str
pw.writeBytes("0400"); //Int
pw.writeBytes("0500"); //Dex
pw.writeBytes("0600"); //Vit
pw.writeBytes("2200");
pw.writeBytes("2200");
pw.writeBytes("0200");
pw.writeBytes("0200");
pw.writeBytes("0600");
pw.writeBytes("03");
pw.writeBytes("01");
pw.writeBytes("00");
pw.writeBytes("00");
pw.writeBytes("0700"); //Stat Points
pw.writeBytes("0200");
pw.writeBytes("0000");
pw.writeBytes("0000");
pw.writeBytes("0000");
pw.writeBytes("0000");
pw.writeBytes("0000");
pw.writeBytes("0000");
pw.writeBytes("0000");  
/*
	for(int i=1; i<=5; i++)
		pw.write(player->getInventories()->getInventory(i)->getSlots());
	for(int i=0; i<=5; i++){
		Inventory* in = player->getInventories()->getInventory(i);
		hash_map<int, Item*>* items = in->getItems();
		if(i == EQUIPPED){
			for(hash_map<int, Item*>::iterator iter = items->begin(); iter!=items->end(); iter++){
				Item* item = iter->second;
				if(!DataProvider::getInstance()->isItemCash(item->getID()))
					itemInfo(item);
			}
			pw.write(0);
			for(hash_map<int, Item*>::iterator iter = items->begin(); iter!=items->end(); iter++){
				Item* item = iter->second;
				if(DataProvider::getInstance()->isItemCash(item->getID()))
					itemInfo(item);
			}
		}
		else{
			for(hash_map<int, Item*>::iterator iter = items->begin(); iter!=items->end(); iter++){
				itemInfo(iter->second);
			}
		}
		pw.write(0);
	}
	//skill
	hash_map <int, Skill*>* skills = player->getSkills()->getSkillsInfo();
	pw.writeShort(skills->size());
	for(hash_map<int, Skill*>::iterator iter = skills->begin(); iter!=skills->end(); iter++){
		Skill* skill = iter->second;
		pw.writeInt(skill->getID());
		pw.writeInt(skill->getLevel());
		if(FOURTH_JOB(skill->getID())){
			pw.writeInt(skill->getMasterLevel());
		}
	}
	//Quests
	pw.writeShort(0);
	vector <Quest*> p = player->getQuests()->getQuestsInProgress();
	vector <Quest*> c = player->getQuests()->getCompletedQuests();
	pw.writeShort(p.size());
	Quest* quest=NULL;
	for(int i=0; i<(int)p.size() && (quest = p[i]) != NULL; i++){
		pw.writeInt(quest->getID());
	}
	pw.writeShort(c.size());
	for(int i=0; i<(int)c.size() && (quest = c[i]) != NULL; i++){
		pw.writeShort(quest->getID());
		pw.writeLong(quest->getCompleteTime());
	}
	pw.writeLong(0);
	for(int i=0; i<15; i++)
		pw.writeBytes("FFC99A3B");
	pw.writeInt(0);
	pw.writeLong(getLongTime());
*/
	return &pw;
}
PacketWriter* PacketCreator::showKeys(PlayerKeys* keys){
	pw.writeShort(SHOW_KEYS);

	pw.write(0);
	Key* key;
	for(int i=0; i<90; i++){
		key = keys->getKey(i);
		if(key != NULL){
			pw.write(key->getType());
			pw.writeInt(key->getAction());
		}
		else{
			pw.write(0);
			pw.writeInt(0);
		}
	}

	return &pw;
}
PacketWriter* PacketCreator::showMoving(int playerid, ObjectMoving& moving){
	pw.writeShort(PLAYER_MOVE);
	
	pw.writeInt(playerid);
	pw.writeInt(0);
	pw.write(moving.getPacket()->getBytes(), moving.getPacket()->getLength());

	return &pw;
}
PacketWriter* PacketCreator::faceExpression(int playerid, int face){
	pw.writeShort(FACE_EEXPRESSION);

	pw.writeInt(playerid);
	pw.writeInt(face);

	return &pw;
}
PacketWriter* PacketCreator::showChatMassage(int playerid, string msg,string PlayerName, bool macro, bool gm){
	pw.writeBytes("0501");
	pw.writeBytes("1700");
	pw.writeBytes("8E00");
	pw.writeBytes("AA01");
	pw.writeBytes("00000000");
	char OutPut[100];
	pw.writeInt(playerid);
	pw.writeInt(0);
	if(gm)
		sprintf(OutPut,"%s (GM): %s",PlayerName.c_str(),msg.c_str());
	else
		sprintf(OutPut,"%s : %s",PlayerName.c_str(),msg.c_str());

	pw.writeString(OutPut,100);

	pw.writeBytes("000000000000000000704000E80373DA17BB000014B5");

	return &pw;
}
PacketWriter* PacketCreator::damagePlayer(int playerid, int skill, int dmg, int obj){
	pw.writeShort(DAMAGE_PLAYER);

	pw.writeInt(playerid);
	pw.write(skill);
	pw.writeInt(0);
	if(skill == -2)
		pw.writeInt(dmg);
	else
		pw.writeInt(obj);
	pw.writeShort(1);
	pw.write(0);
	pw.writeInt(dmg);

	return &pw;
}
PacketWriter* PacketCreator::showMassage(string msg, char type, int channel, bool server){
	pw.writeBytes("0501");
	pw.writeBytes("1200");
	pw.writeBytes("6600");
	pw.writeBytes("7D01");
	pw.writeBytes("00000000");
	pw.write(type);
	pw.writeString(msg,0x4c);
	pw.writeBytes("104000E8033D8FC9A80000A511");

	return &pw;
}
PacketWriter* PacketCreator::showInfo(Player* player){
	pw.writeShort(PLAYER_INFO);
	
	pw.writeInt(player->getID());
	pw.write(player->getLevel());
	pw.writeShort(player->getJob());
	pw.writeShort(player->getFame());
	pw.write(0); // Married
	pw.writeString("-"); // Guild Name
	pw.writeString("");
	pw.write(0);
	vector <Pet*>* pets = player->getPets();
	for(int i=0; i<(int)pets->size(); i++){
		pw.write(1);
		Pet* pet = (*pets)[i];
		pw.writeInt(pet->getItemID());
		pw.writeString(pet->getName());
		pw.write(pet->getLevel());
		pw.writeShort(pet->getCloseness());
		pw.write(pet->getFullness());
		pw.writeInt(0);
		pw.writeShort(0);
	}
	pw.write(0); // end of pets
	bool taming = player->getBuffs()->isBuffActive(Effect::MONSTER_RIDING);
	pw.write(taming); 
	if(taming){
		pw.writeInt(0); // Level
		pw.writeInt(0); // EXP
		pw.writeInt(0); // Tiredness

	}
	pw.write(0);
	pw.writeLong(1);
	pw.writeLong(0);
	pw.writeLong(0);

	return &pw;
}
PacketWriter* PacketCreator::findPlayerReplay(string name, bool success){
	pw.writeShort(FIND_PLAYER);

	pw.write(0xA);
	pw.writeString(name);
	pw.write(success);

	return &pw;
}
PacketWriter* PacketCreator::findPlayerMap(string name, int map, int channel){
	pw.writeShort(FIND_PLAYER);

	pw.write(0x9);
	pw.writeString(name);
	pw.write(1);
	pw.writeInt(map);

	return &pw;
}
PacketWriter* PacketCreator::findPlayerChannel(string name, int map, int channel){
	pw.writeShort(FIND_PLAYER);

	pw.write(0x9);
	pw.writeString(name);
	pw.write(3);
	pw.writeInt(channel);

	return &pw;
}
PacketWriter* PacketCreator::whisperPlayer(string name, string msg, int channel){
	pw.writeShort(FIND_PLAYER);

	pw.write(0x12);
	pw.writeString(name);
	pw.writeShort(channel);
	pw.writeString(msg);

	return &pw;
}
PacketWriter* PacketCreator::changeChannel(char channelid, short port){
	pw.writeShort(CHANGE_CHANNEL);
		
	pw.write(channelid);

	IP* ip = Worlds::getInstance()->getIP();

	pw.write(ip->p1);
	pw.write(ip->p2);
	pw.write(ip->p3);
	pw.write(ip->p4);

	pw.writeShort(port);
	return &pw;
}
//My Canges
PacketWriter* PacketCreator::CharHPSPINIT(Player *player){
pw.writeShort(WORLD_HEADER_START);
pw.writeBytes("510014006A0100000000000000000000604C");
return &pw;
}
PacketWriter* PacketCreator::FWDiscauntFaction(Player *player){
pw.writeShort(WORLD_HEADER_START);
pw.writeBytes("5D012A008C02000000000000000064000000000000006400000000704000");
pw.writeBytes("E803D2A874A9000084D1");
return &pw;
}
PacketWriter* PacketCreator::QUEST_ALL(Player *player){
pw.writeShort(WORLD_HEADER_START);
pw.writeBytes("7900E4046206000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000030303030");
pw.writeBytes("3030303030303030303030303030303030303030303030303030303030303030");
pw.writeBytes("3030303030303030303030303030303030303030303030303030303030303030");
pw.writeBytes("3030303030303030303030303030303030303030303030303030303030303030");
pw.writeBytes("3030303030303030303030303030303030303030303030303030303030303030");
pw.writeBytes("3030303030303030303030303030303030303030303030303030303030303030");
pw.writeBytes("3030303030303030303030303030303030303030303030303030303030303030");
pw.writeBytes("3030303020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("2020202020202020202020202020202020202020202020202020202020202020");
pw.writeBytes("20202000");
return &pw;
}
PacketWriter* PacketCreator::INVEN_ALL(Player *player){
pw.writeShort(WORLD_HEADER_START);
pw.writeBytes("64005011B9120000000043397A00CBE67B00000000000000000000000000");
pw.writeBytes("3BDA810000000000657B8900FB018B0000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000010000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000");
pw.writeBytes("000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFF6BFC7A00000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFF000000001B6E86000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000800000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("00000000000000000000000000000000000000000000000000000000FFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFF00000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("000000000000000000000000FF000301");
return &pw;
}

PacketWriter* PacketCreator::QUICKSLOTALL(Player *player){
pw.writeShort(WORLD_HEADER_START);
pw.writeBytes("A900E2009002000000000100000000000000040000000000030003000000");
pw.writeBytes("00000200FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
pw.writeBytes("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00704000E803D2A874A90000");
pw.writeBytes("1AA8");
return &pw;
}
PacketWriter* PacketCreator::STORE_INFO_1(Player *player){
pw.writeShort(WORLD_HEADER_START);
pw.writeBytes("AA00D00D7F0F000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000FF000000FF0000214E000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000FFFFFF000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("00000000000000FF0000FFFF0000000000000000000000000000000000000000");
pw.writeBytes("000000000000000000FFFFFF0000000000000000000000000000000000000000");
pw.writeBytes("000000000000000000000000000000000000000000000000000000FF000000FF");
pw.writeBytes("0000FFFF00000000000000000000000000000000000000000000000000000000");
pw.writeBytes("00FFFFFF000000000000000000000000");
return &pw;
}
PacketWriter* PacketCreator::STORE_INFO_2(Player *player){
pw.writeShort(WORLD_HEADER_START);
pw.writeBytes("AA00D00D7F0F000000000100000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000FF000000FF0000214E000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000FFFFFF000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("00000000000000FF0000FFFF0000000000000000000000000000000000000000");
pw.writeBytes("000000000000000000FFFFFF0000000000000000000000000000000000000000");
pw.writeBytes("000000000000000000000000000000000000000000000000000000FF000000FF");
pw.writeBytes("0000FFFF00000000000000000000000000000000000000000000000000000000");
pw.writeBytes("00FFFFFF000000000000000000000000");
return &pw;
}
PacketWriter* PacketCreator::STORE_INFO_3(Player *player){
pw.writeShort(WORLD_HEADER_START);
pw.writeBytes("AA00D00D7F0F000000000200000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000FF000000FF0000214E000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000FFFFFF000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("00000000000000FF0000FFFF0000000000000000000000000000000000000000");
pw.writeBytes("000000000000000000FFFFFF0000000000000000000000000000000000000000");
pw.writeBytes("000000000000000000000000000000000000000000000000000000FF000000FF");
pw.writeBytes("0000FFFF00000000000000000000000000000000000000000000000000000000");
pw.writeBytes("00FFFFFF000000000000000000000000");
return &pw;
}
PacketWriter* PacketCreator::STORE_INFO_4(Player *player){
pw.writeShort(WORLD_HEADER_START);
pw.writeBytes("AA00D00D7F0F000000000300000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000FF000000FF0000214E000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000FFFFFF000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("00000000000000FF0000FFFF0000000000000000000000000000000000000000");
pw.writeBytes("000000000000000000FFFFFF0000000000000000000000000000000000000000");
pw.writeBytes("000000000000000000000000000000000000000000000000000000FF000000FF");
pw.writeBytes("0000FFFF00000000000000000000000000000000000000000000000000000000");
pw.writeBytes("00FFFFFF000000000000000000000000");
return &pw;
}
PacketWriter* PacketCreator::STORE_MONEYINFO(Player *player){
pw.writeShort(WORLD_HEADER_START);
pw.writeBytes("AB002A00DA01000000000000000000000000000000000000000000704000");
pw.writeBytes("E803D2A874A9000027B3");
return &pw;
}
PacketWriter* PacketCreator::Enter_Map_Start(Player *player){
pw.writeShort(WORLD_HEADER_START);
pw.writeBytes("1C002A004B0100000000");
pw.writeShort(player->getRegionId()); //Region
pw.writeShort(player->getMapId()); //Map
pw.writeBytes("19009603010001001900960300704000");
pw.writeBytes("E803D2A874A900008AE8");
return &pw;
}
PacketWriter* PacketCreator::INVEN_CASH(Player *player){
pw.writeShort(WORLD_HEADER_START);
pw.writeBytes("E600DE06C908000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000000000000000000000000000000000000000000000000000000000");
pw.writeBytes("0000000000011808000000000001180800704000E803D2A874A900007019");
return &pw;
}