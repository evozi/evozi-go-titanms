/*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef PLAYERINVENOTRYs_H
#define PLAYERINVENOTRYs_H

#include <hash_map>
using namespace std;
using namespace stdext;

#define INVENTORY(x) (x/1000000)
#define IS_EQUIP(x) (INVENTORY(x) == 1)
#define VALID_INVENTORY(x) (x>=1 && x<=5)
#define TYPE(x) (x/1000000)
#define MAJOR_TYPE(x) (x/10000)
#define MINOR_TYPE(x) ((x/1000)%10)
#define IS_STAR(x) (MAJOR_TYPE(x) == 207 || MAJOR_TYPE(x) == 233) // TODO: or IS_THROWABLE?

#define IS_EVENT_WEP(id)   (id > 9300000 && id < 9399999) //Done
#define IS_WEP_BOX(id)     (id >= 7991011 && id <= 7992255) //Done
#define IS_ARCH_WEP(id)    (id >= 7930000 && id < 7939999) //Done
#define IS_SHOOTER_WEP(id) (id >= 7940000 && id < 7949999) //Done
#define IS_GSP_WEP(id)     (id >= 8000001 && id < 8009999) //Done
#define IS_WEP(id)         (id >= 8010000 && id < 8099999) //Done

#define IS_MARK(id)          (id >= 8100000 && id < 8109999) //Done
#define IS_CUPLE(id)         (id >= 8892000 && id < 8899999) //Done
#define IS_EARNING(id)       (id >= 7510000 && id < 7609999) //DONE
#define IS_SEANCE(id)        (id >= 6800000 && id < 6999999) //Done
#define IS_DRESS(id)         (id >= 8110000 && id < 8209999) //Done
#define IS_DRESS2(id)        (id >= 9510000 && id < 9609999) //DONE
#define IS_RING(id)          (id >= 8210000 && id < 8309999) //DONE
#define IS_NECKLACE(id)      (id >= 8310000 && id < 8409999) //DONE
#define IS_MANTLE(id)        (id >= 8410000 && id < 8509999) //DONE
#define IS_SEAL(id)          (id >= 8510000 && id < 8609999) //DONE  Equip5 INV2
#define IS_SPEND(id)         (id >= 8810000 && id < 8909999) //DONE
#define IS_Jebok(id)         (id >= 1200000 && id < 1299999) //DONE
#define IS_Pazel(id)         (id >= 1100000 && id < 1199999) //DONE
#define IS_HAT(id)           (id >= 8610000 && id < 8689999) //DONE
#define IS_SHOPIT(id)        (id >= 8699001 && id < 8699999) //DONE
#define IS_OTHER(id)         (id >= 8910000 && id < 9009999) //DONE
#define IS_FACE(id)          (id >= 8710000 && id < 8809999) //DONE
#define IS_FACE2(id)         (id >= 9410000 && id < 9509999) //DONE
#define IS_EYE(id)           (id >= 9110000 && id < 9209999) //DONE
#define IS_HAIR(id)          (id >= 9010000 && id < 9109999) //DONE
#define IS_PET(id)           (id >= 9210000 && id < 9220011) //DONE
#define IS_TOY(id)           (id >= 9231011 && id < 9232105) //DONE
#define IS_PET_DECO(id)      (id >= 9220011 && id < 9229999) //DONE
#define IS_SPEND_PET(id)     (id >= 7810000 && id < 7909999) //DONE
#define IS_HAIRACC(id)       (id >= 7300001 && id < 7399999) //DONE

#define EQUIPPED 0
#define EQUIP 1
#define USE 2
#define SETUP 3
#define ETC 4
#define CASH 5

class Inventory;
class Item;
class Player;

struct CompareItems : public std::binary_function<Item*, Item*, bool>
{
bool operator()(Item* x, Item* y) const;
};
class PlayerInventories {
private:
	hash_map <int, Inventory*> invs;
	Player* player;
	Inventory* empty;
public:
	PlayerInventories(Player* player);
	Inventory* getInventory(int n){
		if(invs.find(n) != invs.end())
			return invs[n];
		return empty;
	}
	bool giveItem(int itemid, int amount=1,bool lock = false, bool rands = false);
	bool giveItem(Item* item, bool drop = false);
	Item* getItemBySlot(int inv, int slot);
	Item* getItemByID(int id);
	//void removeItem(int inv, int slot);
	void removeItem(int itemid);
	//void removeItem(Item* item);
	static bool validID(int id);
	int getINVbyITEM(int id);
	int getItemAmount(int id);
	void removeItemBySlot(int inv, int slot, int amount, bool send = true);
	void deleteAll();
};

#endif