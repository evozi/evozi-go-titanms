 /*
	This file is part of TitanMS.
	Copyright (C) 2008 koolk

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "MasterDecoder.h"
string MasterDecoder::pass;
int MasterDecoder::version;
void MasterDecoder::Initializever(int version){
MasterDecoder::version = version;
}
void MasterDecoder::encrypt(unsigned char *buffer, int size){
	string tpass = string(pass);; 
	char* key = (char*)tpass.c_str();
	int len = strlen(key);
	char last = 'a';
	for(int j=0; j<3; j++){
		for(int i=size-1; i>=0; i--){
			key[len-i%len-1] += last;
			buffer[i] ^= key[len-i%len-1];
			last -= key[len-i%len-1];
		}
		for(int i=0; i<size; i++){
			key[i%len] ^= (last-4);
			buffer[i] ^= key[i%len];
			last ^= (key[i%len]+8);
		}
	}
} 

void MasterDecoder::decrypt(unsigned char *buffer, int size){
	encrypt(buffer, size);
}

void MasterDecoder::createHeader (unsigned char* header, short size) {
	(*(short*)header) = size;
}
unsigned char* MasterDecoder::getConnectPacket(int PlayerID,int TimeLogin,string ip) {

	IP p = Tools::stringToIP(ip);
	(*(short*)(connectBuffer))   = 0x0105;
	(*(short*)(connectBuffer+2)) = 0x0014;
	(*(short*)(connectBuffer+4)) = 0x0028;
	(*(short*)(connectBuffer+6)) = 0x0141;
	(*(int*)(connectBuffer+8))  = 0x0;
	(*(int*)(connectBuffer+12)) = PlayerID;  //Player ID
	(*(int*)(connectBuffer+16)) = version;  //Version
	(*(int*)(connectBuffer+20)) = 0x3B5F;   //UDP PORT
	(*(int*)(connectBuffer+24)) = 13187234;//A2 38 C9 00//TimeLogin;    //Well Be Send Counter
	connectBuffer[28]           = p.p1;//0xc0;     //192
	connectBuffer[29]           = p.p2 ;//0xa8;     //168
	connectBuffer[30]           = p.p3 ;//0x02;     //2
	connectBuffer[31]           = p.p4 ;//0x96;     //150
	(*(int*)(connectBuffer+32)) = 1784950331;    //3B 2E 64 6A dont kow
	(*(int*)(connectBuffer+36)) = 2411432947;     //F3 8B BB 8F dont know

	return connectBuffer;
}